//
//  CreateAutorizationViewController.swift
//  Salud-total
//
//  Created by iMac on 12/26/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class CreateAutorizationViewController: UIViewController {

    @IBOutlet weak var table: UITableView!
    
    let vm=CreateAutorizationViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        table.separatorStyle = .none
        vm.inflateTable()
        showLoading()
        vm.getServices(susses: {
            self.hiddenLoading()
        }) { (error) in
            self.alert( msj: error, callback: {
                print("")
            })
        }
        // Do any additional setup after loading the view.
    }
    
    @objc func tapSend(_ sender:UIButton){
        showLoading()
        self.vm.validateQuantity(susses: {
            self.hiddenLoading()
            self.autorizateService()
        }) { (error) in
            self.hiddenLoading()
            self.alert( msj: error, callback: {
                
            })
        }
    }
    
    @objc func tapSelect(_ sender:UIButton){
        
        openPicker(list: vm.retListFamiliars()) { (index) in
            self.vm.container[sender.tag].value=self.vm.familiars[index].nombre
            self.vm.selectedFamiliar=self.vm.familiars[index]
            self.table.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .none)
            self.showLoading()
            self.vm.getOrders(susses: {
                self.hiddenLoading()
            }, failed: { (error) in
                self.hiddenLoading()
                self.alert( msj: error, callback: {
                    
                })
            })
        }
    }
    
    @objc func tapSelectServices(_ sender:UIButton){
        
        openPicker(list: vm.retlistAutorizationServices()) { (index) in
            let item=self.vm.listAutorizationServices[index]
            self.vm.container[sender.tag].value=item.servicio
            self.vm.selectedAutorizationServiceModel=item
            self.table.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .none)
            self.showLoading()
            self.vm.getHeadquarters(susses: {
                self.hiddenLoading()
            }, failed: { (error) in
                self.hiddenLoading()
            })
        }
    }
    @objc func tapSelectOrder(_ sender:UIButton){
        
        openPicker(list: vm.retlistOrders()) { (index) in
            let item=self.vm.listOrders[index]
            self.vm.container[sender.tag].value=item.servicio
            self.vm.selectedOrder=item
            self.table.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .none)
        }
    }
    @objc func tapSelectHead(_ sender:UIButton){
        
        openPicker(list: vm.retlistHeadq()) { (index) in
            let item=self.vm.listHeadquarter[index]
            self.vm.container[sender.tag].value=item.nombreSede
            self.vm.selectedHeadquarter=item
            self.table.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .none)
        }
    }
    func autorizateService(){
        let validateObj=vm.validateForm()
        if validateObj.error{
            alert(msj: validateObj.alert) {
                
            }
        }else{
            goResume(vm: vm)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CreateAutorizationViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item=vm.container[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: item.cell, for: indexPath) as! CreateAutorizationTableViewCell
        switch item.cell {
       
        case vm.cells.select:
            
            cell.selectButton.tag=indexPath.row
            cell.lblPlaceHolder.text=item.placeholder
            cell.icon.image=UIImage(named:item.icon)
            let html="<p style='font-family:\"Futura\";font-size:14px'>\(item.label.uppercased()) <span style='color:rgb(45,79,140)'>*</span></p>".htmlToAttributedString
            cell.lblNaMeItem.attributedText=html
            cell.lblPlaceHolder.textColor=UIColor(red:186/255,green:186/255,blue:186/255,alpha:1)
            cell.selectButton.tag=indexPath.row
            cell.selectButton.removeTarget(nil, action: nil, for: .allEvents)
            
            if item.value != ""{
                cell.lblPlaceHolder.text=item.value
                cell.lblPlaceHolder.textColor=UIColor(red:0,green:0,blue:0,alpha:1)
            }
            
            switch item.id{
            case vm.ids.patient:
                cell.selectButton.addTarget(self, action: #selector(tapSelect(_:)), for: .touchDown)
            case vm.ids.autorizationService:
                cell.selectButton.addTarget(self, action: #selector(tapSelectServices(_:)), for: .touchDown)
            case vm.ids.order:
                cell.selectButton.addTarget(self, action: #selector(tapSelectOrder(_:)), for: .touchDown)
            case vm.ids.headquarters:
                cell.selectButton.addTarget(self, action: #selector(tapSelectHead(_:)), for: .touchDown)

            default:
                break
            }
            
            //cell.selectButton.addTarget(self, action: #selector(tapSelect(_:)), for: .touchDown)
            
            
        
        case vm.cells.textfield:
            cell.icon.image=UIImage(named:item.icon)
            cell.txtItem.text=""
            cell.txtItem.placeholder=item.placeholder
            cell.txtItem.tag=indexPath.row
            cell.txtItem.delegate=self
            cell.txtItem.keyboardType = item.type!
            let html="<p style='font-family:\"Futura\";font-size:14px'>\(item.label.uppercased()) <span style='color:rgb(45,79,140)'>*</span></p>".htmlToAttributedString
            
            cell.lblNaMeItem.attributedText=html
            if item.value != ""{
                cell.txtItem.text=item.value
            }
            if item.id == vm.ids.quantity{
                cell.txtItem.isEnabled=false
                cell.contentTxt.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1)
            }
            
        case vm.cells.button:
            cell.btnSend.addTarget(self, action: #selector(tapSend(_:)), for: .touchDown)
            break
        
        default:
            break
        }
        
        
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm.container.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(vm.container[indexPath.row].height)
    }
    
}
extension CreateAutorizationViewController: UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = UIColor.black
            
        }
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        vm.container[textView.tag].value=textView.text!
    }
}
extension CreateAutorizationViewController: UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        
            vm.container[textField.tag].value=textField.text!
        
        
        
    }
    
}
