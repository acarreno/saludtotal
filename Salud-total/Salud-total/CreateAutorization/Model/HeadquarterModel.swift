//
//  HeadquarterModel.swift
//  Salud-total
//
//  Created by Andrey Carreño on 12/29/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper
class HeadquarterModel: Mappable {
    
    var ciudad = "";
    var codigoConvenio = "";
    var direccion = "";
    var errordescripcion = "";
    var errornum = "";
    var esBolsa = 0;
    var esDireccionamientoNacional = 0;
    var esPropia = 0;
    var formaLiquidacion = "";
    var iD_TM_ConvenioPrincipal = 0;
    var id = 0;
    var idCiudad = 0;
    var idConvenio = 0;
    var idConvenioSede = 0;
    var idDireccionamiento = 0;
    var idEmpresa = 0;
    var idFormaLiquidacion = 0;
    var idIps = 0;
    var idSede = 0;
    var idSucursal = 0;
    var idZona = 0;
    var nombreEmpresa = "";
    var nombreIPs = "";
    var nombreSede = "T";
    var prioridad = 0;
    var prioridadFinal = "";
    var prioridadPrincipal = 0;
    var sucursal = "";
    var telefono = "";
    var valorServicio = 0;
    var zona = "";
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        ciudad <- map["ciudad"]
        codigoConvenio <- map["codigoConvenio"]
        direccion <- map["direccion"]
        errordescripcion <- map["errordescripcion"]
        errornum <- map["errornum"]
        esDireccionamientoNacional <- map["esDireccionamientoNacional"]
        esBolsa <- map["esBolsa"]
        esPropia <- map["esPropia"]
        formaLiquidacion <- map["formaLiquidacionj"]
        iD_TM_ConvenioPrincipal <- map["iD_TM_ConvenioPrincipal"]
        id <- map["id"]
        idCiudad <- map["idCiudad"]
        idConvenio <- map["idConvenio"]
        idConvenioSede <- map["idConvenioSede"]
        idDireccionamiento <- map["idDireccionamiento"]
        idEmpresa <- map["idEmpresa"]
        idFormaLiquidacion <- map["idFormaLiquidacion"]
        idIps <- map["idIps"]
        idSede <- map["idSede"]
        idSucursal <- map["idSucursal"]
        idZona <- map["idZona"]
        nombreEmpresa <- map["nombreEmpresa"]
        nombreIPs <- map["nombreIPs"]
        nombreSede <- map["nombreSede"]
        prioridad <- map["prioridad"]
        prioridadFinal <- map["prioridadFinal"]
        prioridadPrincipal <- map["prioridadPrincipal"]
        sucursal <- map["sucursal"]
        telefono <- map["telefono"]
        valorServicio <- map["valorServicio"]
        zona <- map["zona"]
       
        
        
        
        
       
        
        
    }
}
