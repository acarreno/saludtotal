//
//  MaxQuantityServices.swift
//  Salud-total
//
//  Created by Andrey Carreño on 12/29/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper
class MaxQuantityServices: Mappable {
    
    var cantidadMaximaServicio = 1;
    var cumple = 0;
    var iD_TP_Servicio = 156486;
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        cantidadMaximaServicio <- map["cantidadMaximaServicio"]
        cumple <- map["cumple"]
        iD_TP_Servicio <- map["iD_TP_Servicio"]
        
    }
}
