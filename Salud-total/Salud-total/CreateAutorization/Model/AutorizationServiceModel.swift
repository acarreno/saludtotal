//
//  AutorizationServiceModel.swift
//  Salud-total
//
//  Created by iMac on 12/27/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper

class AutorizationServiceModel : Mappable {
   
    var clasificacionServicio = ""
    var codigo = ""
    var errordescripcion = ""
    var errornum = 0
    var iD_TP_ClasificacionServicio = 0
    var iD_TP_Producto = 0
    var iD_TP_Servicio = 0
    var iD_TP_SubClasificacionServicio = 0
    var producto = ""
    var requiereNapa = ""
    var servicio = ""
    var subClasificacionServicio = ""
    var textoMostrar = ""
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        clasificacionServicio <- map["clasificacionServicio"]
        codigo <- map["codigo"]
        errordescripcion <- map["errordescripcion"]
        iD_TP_ClasificacionServicio <- map["iD_TP_ClasificacionServicio"]
        iD_TP_Producto <- map["iD_TP_Producto"]
        iD_TP_Servicio <- map["iD_TP_Servicio"]
        iD_TP_SubClasificacionServicio <- map["iD_TP_SubClasificacionServicio"]
        producto <- map["producto"]
        requiereNapa <- map["requiereNapa"]
        servicio <- map["servicio"]
        subClasificacionServicio <- map["subClasificacionServicio"]
        textoMostrar <- map["textoMostrar"]
        
    }
    
    
}
