//
//  OrderModel.swift
//  Salud-total
//
//  Created by Andrey Carreño on 12/29/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper
class OrderModel: Mappable {
   
    var errornum = ""
    var errordescripcion = ""
    var idResultadoTramiteSolicitud = 0
    var servicio = ""
    var napa = ""
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        errornum <- map["errornum"]
        errordescripcion <- map["errordescripcion"]
        idResultadoTramiteSolicitud <- map["idResultadoTramiteSolicitud"]
        servicio <- map["servicio"]
        napa <- map["napa"]
        
    }
}
