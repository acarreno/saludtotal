//
//  RequesCreteAuthorization.swift
//  Salud-total
//
//  Created by Andrey Carreño on 12/29/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper
class RequesCreteAuthorization: Mappable {
    
    var pIdAfiliado=""
    var pIdServicio=""
    var pIdProducto=""
    var pCantidad=""
    var pNapa=""
    var pIdConvenioSede=""
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        pIdAfiliado <- map["pIdAfiliado"]
        pIdServicio <- map["pIdServicio"]
        pIdProducto <- map["pIdProducto"]
        pCantidad <- map["pCantidad"]
        pNapa <- map["pNapa"]
        pIdConvenioSede <- map["pIdConvenioSede"]
        
    }
}
