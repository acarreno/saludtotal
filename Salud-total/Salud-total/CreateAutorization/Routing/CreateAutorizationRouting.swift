//
//  CreateAutorizationRouting.swift
//  Salud-total
//
//  Created by iMac on 12/26/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//


import Foundation
import UIKit

extension CreateAutorizationViewController{
    static func instantiate() -> CreateAutorizationViewController {
        let storyboard = UIStoryboard(name: "CreateAutorization", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! CreateAutorizationViewController
        return vc
    }
    func openPicker(list:[String],callback:@escaping (Int)->()){
        JACSSelectViewController.open(navController: self.navigationController!, list: list) { (index) in
            callback(index)
        }
    }
    func goResume(vm:CreateAutorizationViewModel) {
        let vc=ResumeAutorizationsViewController.instantiate()
        vc.vm=vm
        navigationController?.pushViewController(vc, animated: true)
    }
}
