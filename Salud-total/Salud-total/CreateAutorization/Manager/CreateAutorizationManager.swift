//
//  CreateAutorization.swift
//  Salud-total
//
//  Created by iMac on 12/27/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper
import UIKit
class CreateAutorizationManager:UIViewController {
    
    func getServices(susses:@escaping ([AutorizationServiceModel])->(),failed:@escaping (String)->()){
        servGet(url: URlGet().ConsultaServicioAutorizaApp, isHeaders: true) { (finish, res) in
            if finish{
                debugPrint(res)
                let response=res as![[String:Any]]
                susses(Mapper<AutorizationServiceModel>().mapArray(JSONObject: (response) )!)
            }else{
                failed(res as! String)
            }
        }
    }
    
    func getOrders(id:String,susses:@escaping ([OrderModel])->(),failed:@escaping (String)->()){
        let url="\(URlGet().ConsultaNapaAutorizaApp)?IDAfiliado=\(id)"
        servGet(url: url, isHeaders: true) { (finish, res) in
            if finish{
                debugPrint(res)
                
                let response=res as![[String:Any]]
                if  response[0]["errordescripcion"] != nil && (response[0]["errordescripcion"] as! String) != ""{
                    let error=response[0]["errordescripcion"]
                    failed(error as!String)
                }else{
                    susses(Mapper<OrderModel>().mapArray(JSONObject: (response) )!)
                }
                
            }else{
                failed(res as! String)
            }
        }
    }
    func getHeadquarters(idAf:String,idServ:String,product:String,susses:@escaping ([HeadquarterModel])->(),failed:@escaping (String)->()){
        let url="\(URlGet().ConsultaDireccionamientoAutorizaApp)?IDAfiliado=\(idAf)&IDServicio=\(idServ)&Producto=\(product)"
        servGet(url: url, isHeaders: true) { (finish, res) in
            if finish{
                debugPrint(res)
                
                let response=res as![[String:Any]]
                if  response[0]["errordescripcion"] != nil && (response[0]["errordescripcion"] as! String) != ""{
                    let error=response[0]["errordescripcion"]
                    failed(error as!String)
                }else{
                    susses(Mapper<HeadquarterModel>().mapArray(JSONObject: (response) )!)
                }
                
            }else{
                failed(res as! String)
            }
        }
    }
    func validateQuantity(idServ:String,quantity:String,susses:@escaping (MaxQuantityServices)->(),failed:@escaping (String)->()){
        let url="\(URlGet().ValidaCantidadMaxServicioAutorizaApp)?IDServicio=\(idServ)&Cantidad=\(quantity)"
        servGet(url: url, isHeaders: true) { (finish, res) in
            if finish{
                debugPrint(res)
                
                let response=res as![[String:Any]]
                let validationItem=MaxQuantityServices(JSON:response[0])!
                
                    susses(validationItem)
                
                
            }else{
                failed("Error interno")
            }
        }
    }
    func autoriZateService(params:RequesCreteAuthorization,susses:@escaping (String)->(),failed:@escaping (String)->()){
        let url=URlGet().AutorizaServicioApp
        
        servPost(url: url, params: params.toJSON(), isHeaders: true) { (finish, res) in
            if finish{
                debugPrint(res)
                let response = res as![[String:Any]]
                if (response[0]["origenError"] as! String) != "0"{
                    failed("Error interno")
                }else{
                    susses("")
                }
                
            }else{
                failed("Error interno")
            }
        }
    }
}
