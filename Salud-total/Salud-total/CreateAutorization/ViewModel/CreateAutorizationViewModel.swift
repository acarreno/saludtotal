//
//  CreateAutorizationViewModel.swift
//  Salud-total
//
//  Created by iMac on 12/26/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//


import Foundation
import UIKit
class CreateAutorizationViewModel  {
    
    struct Cells {
        let select="select"
        let textfield="textfield"
        let button="button"
        let text="text"
    }
    
    struct Ids {
        let patient="patient"
        let autorizationService="autorizationService"
        let quantity="quantity"
        let order="order"
        let headquarters="headquarters"
        let button="button"
    }
    struct Sizes {
        let select=98
        let textfield=108
        let separator=20
        let text=80
        let button=68
        
    }
    
    typealias CellModel =  (cell:String,id:String,value:String,alert:String,placeholder:String,type:UIKeyboardType?,icon:String,label:String,height:Int,obligatory:Bool)
    
    var container=[CellModel]()
    var ids=Ids()
    var sizes=Sizes()
    var cells=Cells()
    var manager=CreateAutorizationManager()
    var familiars=[AutorizationFamiliarModel]()
    var selectedFamiliar=AutorizationFamiliarModel(JSON: ["":""])
    var listAutorizationServices=[AutorizationServiceModel]()
    var selectedAutorizationServiceModel=AutorizationServiceModel(JSON: ["":""])
    var listHeadquarter=[HeadquarterModel]()
    var selectedHeadquarter=HeadquarterModel(JSON: ["":""])
    var listOrders=[OrderModel]()
    var selectedOrder=OrderModel(JSON: ["":""])
    var requesCreteAuthorization=RequesCreteAuthorization(JSON:["":""])!
    var quantity=""
    
    func inflateTable(){
        container.append(
            (
                cell:cells.select,
                id:ids.patient,
                value:"",
                alert:"Selecciona un paciente",
                placeholder:"Selecciona un paciente",
                type:nil,
                icon:"stGeneralUsuario",
                label:"Paciente",
                height:sizes.select,
                obligatory:true
            )
        )
        container.append(
            (
                cell:cells.select,
                id:ids.autorizationService,
                value:"",
                alert:"Selecciona el servicio a autorizar",
                placeholder:"Selecciona el servicio a autorizar",
                type:nil,
                icon:"stRadicacionesObservacionesSolicitar",
                label:"Servicio a autorizar",
                height:sizes.select,
                obligatory:true
            )
        )
        container.append(
            (
                cell:cells.text,
                id:"",
                value:"",
                alert:"",
                placeholder:"",
                type:nil,
                icon:"",
                label:"",
                height:sizes.text,
                obligatory:false
            )
        )
        container.append(
            (
                cell:cells.textfield,
                id:ids.quantity,
                value:"1",
                alert:"Ingresa la cantidad a autorizar",
                placeholder:"Ingresa la cantidad a autorizar",
                type:.numberPad,
                icon:"stRadicacionesObservacionesSolicitar",
                label:"Cantidad a autorizar",
                height:sizes.textfield,
                obligatory:true
            )
        )
        container.append(
            (
                cell:cells.select,
                id:ids.order,
                value:"",
                alert:"Selecciona para autorizar",
                placeholder:"Selecciona para autorizar",
                type:nil,
                icon:"stRadicacionesObservacionesSolicitar",
                label:"Orden médica",
                height:sizes.select,
                obligatory:true
            )
        )
        container.append(
            (
                cell:cells.select,
                id:ids.headquarters,
                value:"",
                alert:"Selecciona la sede",
                placeholder:"Selecciona la sede",
                type:nil,
                icon:"stGeneralUbicacion",
                label:"Sede",
                height:sizes.select,
                obligatory:true
            )
        )
        container.append(
            (
                cell:cells.button,
                id:ids.button,
                value:"",
                alert:"",
                placeholder:"",
                type:nil,
                icon:"",
                label:"",
                height:sizes.button,
                obligatory:false
            )
        )
    }
    
    func retListFamiliars()->[String]{
        var list=[String]()
        for item in familiars {
            list.append(item.nombre)
        }
        return list
    }
    func retlistAutorizationServices()->[String]{
        var list=[String]()
        for item in listAutorizationServices {
            list.append(item.servicio)
        }
        return list
    }
    func retlistOrders()->[String]{
        var list=[String]()
        for item in listOrders {
            list.append(item.servicio)
        }
        return list
    }
    func retlistHeadq()->[String]{
        var list=[String]()
        for item in listHeadquarter {
            list.append(item.nombreSede)
        }
        return list
    }
    func getServices(susses:@escaping ()->(),failed:@escaping (String)->()){
        manager.getServices(susses: { res in
            self.listAutorizationServices=res
            susses()
        }) { (error) in
            failed(error)
        }
    }
    func getOrders(susses:@escaping ()->(),failed:@escaping (String)->()){
        
        manager.getOrders(id:"\(selectedFamiliar!.iD_Afiliado)", susses: { (res) in
            self.listOrders=res
            susses()
            debugPrint(res)
        }) { (error) in
            failed(error)
        }
        
        /*manager.getServices(susses: { res in
            self.listAutorizationServices=res
            susses()
        }) { (error) in
            failed(error)
        }*/
    }
    func getHeadquarters(susses:@escaping ()->(),failed:@escaping (String)->()){
        manager.getHeadquarters(idAf:"\(selectedFamiliar!.iD_Afiliado)" , idServ: "\(selectedAutorizationServiceModel!.iD_TP_Servicio)", product: "\(selectedAutorizationServiceModel!.iD_TP_Producto)", susses: { (res) in
            debugPrint(res)
            self.listHeadquarter=res
            susses()
        }) { (error) in
            failed(error)
        }
    }
    func validateQuantity(susses:@escaping ()->(),failed:@escaping (String)->()){
        let cell=container[3]
        manager.validateQuantity(idServ: "\(selectedAutorizationServiceModel!.iD_TP_Servicio)", quantity:cell.value , susses: { (res) in
            if res.cantidadMaximaServicio<Int(cell.value)!{
                failed("Cantidad a autorizar no permitida. <br> El número maximo permitido es de \(res.cantidadMaximaServicio)")
            }
            susses()
            debugPrint(res)
        }) { (error) in
            failed(error)
        }
    }
    func validateForm()->(error:Bool,alert:String){
        for item in container {
            if item.value=="" && item.obligatory{
               return (error:true,alert:item.alert)
            }
        }
        return (error:false,alert:"")
    }
    func sendForm(susses:@escaping ()->(),failed:@escaping (String)->()){
        requesCreteAuthorization.pCantidad=container[3].value
        requesCreteAuthorization.pIdServicio="\(selectedAutorizationServiceModel!.iD_TP_Servicio)"
        requesCreteAuthorization.pIdProducto="\(selectedAutorizationServiceModel!.iD_TP_Producto)"
        requesCreteAuthorization.pNapa="\(selectedOrder!.napa)"
        requesCreteAuthorization.pIdConvenioSede="\(selectedHeadquarter!.idConvenioSede)"
        requesCreteAuthorization.pIdAfiliado="\(selectedFamiliar!.iD_Afiliado)"
        manager.autoriZateService(params: requesCreteAuthorization, susses: { (res) in
            debugPrint(res)
            
            susses()
        }) { (error) in
            debugPrint(error)
            failed(error)
        }
    }
    
}
