//
//  MenuViewModel.swift
//  Salud-total
//
//  Created by iMac on 11/12/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

class MenuViewModel:UIViewController {
    
    struct Cells {
        let item = "item"
        let profile = "profile"
    }
    
    struct Ids {
        let profile = "profile"
        let home = "home"
        let data = "data"
        let notifications = "notifications"
        let news = "news"
        let directory = "directory"
        let aplications = "aplications"
        let closeSesion = "closeSesion"
    }
    
    
    
    typealias CellModel =  (cell:String,id:String,image:UIImage,text:String,obj:Any?,height:CGFloat)
    
    let cells=Cells()
    let ids=Ids()
    var container=[CellModel]()
    var beneficiary=BeneficiaryModel(JSON: ["":""])
    
    func initModel(){
        inflateTeble()
        beneficiary=retBeneficiary()
        
    }
    
    func inflateTeble(){
        container.append((cell:cells.profile,id:ids.profile,image:UIImage(named: "stGeneralUsuario")!,text:"",obj:nil,height:87))
        container.append((cell:cells.item,id:ids.home,image:UIImage(named: "stMenuHome")!,text:"HOME",obj:nil,height:56))
        container.append((cell:cells.item,id:ids.data,image:UIImage(named: "stMenuTusdatos")!,text:"TUS DATOS",obj:nil,height:56))
        container.append((cell:cells.item,id:ids.notifications,image:UIImage(named: "stMenuNotificaciones")!,text:"NOTIFICACIONES",obj:nil,height:56))
        container.append((cell:cells.item,id:ids.news,image:UIImage(named: "stMenuNoticias")!,text:"NOTICIAS",obj:nil,height:56))
        container.append((cell:cells.item,id:ids.directory,image:UIImage(named: "stMenuDirectorio")!,text:"DIRECTORIO",obj:nil,height:56))
        container.append((cell:cells.item,id:ids.aplications,image:UIImage(named: "stMenuAplicacionesrelacionadas")!,text:"APLICACIONES RELACIONADAS",obj:nil,height:56))
        container.append((cell:cells.item,id:ids.closeSesion,image:UIImage(named: "stMenuCerrarsesion")!,text:"CERRAR SESIÓN",obj:nil,height:56))
       
        
    }
    
    
}
