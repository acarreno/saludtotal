//
//  MenuViewController.swift
//  Salud-total
//
//  Created by iMac on 11/12/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    
    @IBOutlet weak var table: UITableView!
    
    var susses: ((_ id: String) -> Void)?
    let vm=MenuViewModel()
    var navigatioVC=UINavigationController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        vm.initModel()
        table.separatorStyle = .none
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MenuViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item=vm.container[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: item.cell, for: indexPath) as! MenuTableViewCell
        
        if item.cell == vm.cells.item{
            cell.icon.image=item.image
            cell.lblItemName.text=item.text
        }else{
            cell.lblProfileName.text=vm.beneficiary?.Nombres
            let html="<p style='font-family:\"Futura\";font-size:14px;color:rgb(128,128,128)'>\(vm.beneficiary!.Parentesco)/ <span style='color:rgb(0,159,227)'>\(vm.beneficiary!.EstadoGeneral)</span></p>"
            cell.lblUserState.attributedText=html.htmlToAttributedString
        }
        
        
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm.container.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = vm.container[indexPath.row]
        self.dismiss(animated: true) {
            self.susses?(item.id)
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return vm.container[indexPath.row].height
    }
    
}
