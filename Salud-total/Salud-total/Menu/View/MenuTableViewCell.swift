//
//  MenuTableViewCell.swift
//  Salud-total
//
//  Created by iMac on 11/12/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var icon: UIImageView!
    
     @IBOutlet weak var lblItemName: UILabel!
    
    @IBOutlet weak var lblProfileName: UILabel!
    
    @IBOutlet weak var lblUserState: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
