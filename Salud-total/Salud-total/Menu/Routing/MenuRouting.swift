//
//  MenuRouting.swift
//  Salud-total
//
//  Created by iMac on 11/12/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

extension MenuViewController{
    
    static func instantiate() -> HomeViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HomeViewController
        return vc
    }
    
    static func open(navController:UINavigationController, callback:@escaping(String)->()) {
        let storyboard = UIStoryboard(name: "Menu", bundle: Bundle.main)
        
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! MenuViewController
        vc.navigatioVC=navController
        vc.modalPresentationStyle = .overCurrentContext
        vc.susses={(id:String) -> Void in
            
           callback(id)
        }
        navController.present(vc, animated: true, completion: nil)
       
    }
    
}
