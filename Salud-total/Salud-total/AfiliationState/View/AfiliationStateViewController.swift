//
//  AfiliationStateViewController.swift
//  Salud-total
//
//  Created by iMac on 11/13/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class AfiliationStateViewController: UIViewController {

    @IBOutlet weak var table: UITableView!
    
    let vm=AfiliationStateViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vm.inflateTable()
        addHeader()
        table.separatorStyle = .none
        showLoading()
        vm.getBeneficiary { (finish, res) in
            self.hiddenLoading()
            if finish{
                self.table.reloadData()
            }else{
                self.alert( msj: res, callback: {
                    print("alert")
                })
            }
        }
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AfiliationStateViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item=vm.container[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: item.cell, for: indexPath) as! AfiliationStateTableViewCell
        if item.cell==vm.cells.item{
            cell.contentView.addLine(position: .LINE_POSITION_BOTTOM, color: UIColor(red: 248/255, green: 248/255, blue: 248/255, alpha: 1), width: cell.contentView.frame.width)
            cell.lblName.text=item.obj!.Nombres
            cell.lblID.text="\(item.obj!.TipoDocumento)\(item.obj!.IdBeneficiario)"
            
            cell.lblState.text=item.obj!.EstadoGeneral
            cell.stateCircle.backgroundColor=UIColor(red: 143/255, green: 255/255, blue: 130/255, alpha: 1)
            if item.obj!.Parentesco.trimmingCharacters(in: .whitespacesAndNewlines) != "COTIZANTE"{
                cell.lblAfiliationType.text="Beneficiario / \(item.obj!.Parentesco)"
            }else{
                cell.lblAfiliationType.text="\(item.obj!.Parentesco)"
            }
            if item.obj!.EstadoGeneral == "Inactivo"{
                cell.stateCircle.backgroundColor=UIColor(red: 255/255, green: 0/255, blue: 0/255, alpha: 1)
            }
            
            
        }
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm.container.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return vm.container[indexPath.row].height
    }
    
}
