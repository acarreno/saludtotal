//
//  AfiliationStateTableViewCell.swift
//  Salud-total
//
//  Created by Andrey Carreño on 11/19/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class AfiliationStateTableViewCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblID: UILabel!
    @IBOutlet weak var lblAfiliationType: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var stateCircle: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
