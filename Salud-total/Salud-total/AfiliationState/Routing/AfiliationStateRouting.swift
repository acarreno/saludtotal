//
//  AfiliationStateRouting.swift
//  Salud-total
//
//  Created by iMac on 11/14/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

extension AfiliationStateViewController{
        static func instantiate() -> AfiliationStateViewController {
           let storyboard = UIStoryboard(name: "AfiliationState", bundle: Bundle.main)
           let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! AfiliationStateViewController
           return vc
       }
       
}
