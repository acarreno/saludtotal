//
//  AfiliationStateViewModel.swift
//  Salud-total
//
//  Created by iMac on 11/13/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit
class AfiliationStateViewModel:UIViewController  {
    
    struct Cells {
        let introduction="introduction"
        let item="item"
    }
    
    
    typealias CellModel =  (cell:String,obj:BeneficiaryModel?,height:CGFloat)
    
    var container=[CellModel]()
    var cells=Cells()
    
    func inflateTable(){
        container.append((cell:cells.introduction,obj:nil,height:100))
        container.append((cell:cells.item,obj:BeneficiaryModel(JSON: ["":""]),height:100))
    }
    
    func getBeneficiary(callback:@escaping (Bool,String)->()){
        let user=retUser()
        let url="\(URlGet().ConsultaGrupoFamiliar)?BeneficiarioId=\(user.user.DocumentoId)&BeneficiarioTipoId=\(user.user.codeDocument)"
        servGet(url: url,isHeaders: true) { (finish, res) in
            if finish{
                self.container=[]
                self.container.append((
                    cell:self.cells.introduction,
                    obj:nil,
                    height:100
                ))
                let response = res as![[String:Any]]
                for item in response{
                     self.container.append((
                        cell:self.cells.item,
                        obj:BeneficiaryModel(JSON:item),
                        height:100
                     ))
                }
                callback(true,"")
                
            }else{
                callback(false,"error en el servicio")
            }
        }
    }
}
