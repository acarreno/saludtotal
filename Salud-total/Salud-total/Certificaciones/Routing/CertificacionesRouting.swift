//
//  CertificacionesRouting.swift
//  Salud-total
//
//  Created by csanchez on 11/20/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit
extension CertificacionesViewController {
    
    static func instantiate() -> CertificacionesViewController {
        
        let storyboard = UIStoryboard(name: "Certificaciones", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! CertificacionesViewController
        return vc
        
    }
    
}

