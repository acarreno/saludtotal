//
//  CertificacionesViewModel.swift
//  Salud-total
//
//  Created by csanchez on 11/20/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

struct Cells {
    static let descripcion = "CellCrfcnsDescripcion"
    static let infoPersona = "CellCrfcnsinfoPersona"
    static let Btn = "CellCrfcnsBtn"
}

protocol CertificacionesViewModelProtocol {
    func alert(_ text: String, popView: Bool)
    func reload()
    func configBtn()
    func subShowLoading()
    func subHiddenLoading()
}

class CertificacionesViewModel: MasterViewModel {
    
    var delegate : CertificacionesViewModelProtocol!
    var botonActivo = false
    let descripcionPantalla = "<i style='text-align:center;font-size:14px;color:rgb(128,128,128)'>Genera tus certificados de afiliación aquí. Este <span style='color:rgb( 0,159,227)'> será enviado al correo electrónico registrado.</span> </i>"
    var container = [ListAffiliatesModel]()
    
    func initContainer(){
        let itemDescripcion = ListAffiliatesModel(JSON: ["cell": Cells.descripcion, "description": descripcionPantalla, "alto": 135])
        container.append(itemDescripcion!)
    }
    
    func checkIn(_ index: Int){
        botonActivo = true
        var newContainer = [ListAffiliatesModel]()
        for item in container {
            let newItem = item
            if newItem.cell == Cells.infoPersona {
                newItem.active = false
            }
            newContainer.append(newItem)
        }
        
        newContainer[index].active = true
        container = newContainer
        
        delegate.configBtn()
        delegate.reload()
    }
    
    func getAfiliados(){
        delegate.subShowLoading()
        
        AfiliadosManager.ConsultaGrupoFamiliar(BeneficiarioId: active_user!.DocumentoId, BeneficiarioTipoId: active_user!.codeDocument) { (response, finish, error) in
            self.delegate.subHiddenLoading()
            if finish {
                
                self.container = [ListAffiliatesModel]()
                let itemDescripcion = ListAffiliatesModel(JSON: ["cell": Cells.descripcion, "description": self.descripcionPantalla, "alto": 115])
                
                self.container.append(itemDescripcion!)
                
                for item in response {
                    let userAffiliate = ListAffiliatesModel(JSON: [:])!
                    userAffiliate.dataUserAffiliate = item
                    self.container.append(userAffiliate)
                }
                
                self.delegate.reload()
            }else{
                self.delegate.alert(error, popView: false)
            }
        }
        
    }
    
    func accionGenerate(user:UserModel){
        delegate.subShowLoading()
        /*var user = ListAffiliatesModel(JSON: [:])!
        for item in container {
            if item.active {
                user = item
            }
        }*/
        
        //let codeDoc = retTypeDoc(searchBy: .code, search: user.codeDocument)!
        let codeDoc = user.codeDocument
        delegate.subShowLoading()
        
        AfiliadosManager.GeneracionCertificado(BeneficiarioId: user.DocumentoId, BeneficiarioTipoId: codeDoc) { (response, finish, error) in
            self.delegate.subHiddenLoading()
            if finish {
                //print(response.toJSON())
                self.delegate.alert(response.MensajeRespuesta, popView: false)
            }else{
                //print(error)
                self.delegate.alert( error, popView: false )
            }
        }
        
        
    }
    
}

