//
//  CertificacionesViewController.swift
//  Salud-total
//
//  Created by csanchez on 11/20/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class CertificacionesViewController: MasterView {
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var buttonGenerate: UIButton!
    
    let vm = CertificacionesViewModel()
    var widthTable = CGFloat()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        widthTable = table.frame.size.width
    }
    
    @IBAction func generate(_ sender: Any) {
        let user=retUser()
        vm.accionGenerate(user:user.user)
    }
    
    func initView(){
        addHeader()
        
        vm.delegate = self
        
        vm.initContainer()
        table.reloadData()
        
        showLoading()
        vm.getAfiliados()
        
        configButton()
    }
    
    func configButton(){
        
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CertificacionesViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm.container.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = vm.container[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: item.cell) as! CertificacionesTableViewCell
        
        if item.cell == Cells.descripcion {
            cell.textDescripcion.attributedText = item.description.htmlToAttributedString
            cell.textDescripcion.font=UIFont(name:"FuturaStd-BookOblique" , size:CGFloat(14))
        }
        
        if item.cell == Cells.infoPersona {
            //let isActivo = vm.container[indexPath.row].active
            //cell.imageIsActive.image = UIImage(named: isActivo ? "stGeneralCheckCheck": "elipse412")
            cell.personName.text = item.dataUserAffiliate!.Nombres
            cell.document.text = "\(item.dataUserAffiliate!.TipoDocumento) \(item.dataUserAffiliate!.BeneficiarioId)"
            if item.dataUserAffiliate!.Parentesco.trimmingCharacters(in: .whitespacesAndNewlines) == "COTIZANTE" {
                cell.relationship.text = item.dataUserAffiliate!.Parentesco
            }else{
                cell.relationship.text = "Beneficiario / \(item.dataUserAffiliate!.Parentesco)"
            }
            
        }
        
        // set cell's textLabel.text property
        // set cell's detailTextLabel.text property
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = vm.container[indexPath.row]
        if item.cell == Cells.infoPersona {
            vm.checkIn(indexPath.row)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: widthTable, height: 30))
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let item = vm.container[indexPath.row]
        return CGFloat(item.alto)
        
    }
    
}

extension CertificacionesViewController: CertificacionesViewModelProtocol {
    
    func subShowLoading() {
        showLoading()
    }
    
    func subHiddenLoading() {
        hiddenLoading()
    }
    
    func configBtn() {
        configButton()
    }
    
    func alert(_ text: String, popView: Bool) {
        
        alert( msj: text) {
            if popView {
                self.navigationController?.popViewController(animated: true)
            }
            
        }
        
    }

    func reload(){
        table.reloadData()
        
        if vm.container.count > 1 {
            buttonGenerate.isHidden = false
        }else{
            buttonGenerate.isHidden = true
        }
        
    }
    
}
