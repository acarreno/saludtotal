//
//  CertificacionesTableViewCell.swift
//  Salud-total
//
//  Created by csanchez on 11/20/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class CertificacionesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var textDescripcion: UITextView!
    @IBOutlet weak var imageIsActive: UIImageView!
    @IBOutlet weak var personName: UILabel!
    @IBOutlet weak var document: UILabel!
    @IBOutlet weak var relationship: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
