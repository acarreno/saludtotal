//
//  ListAffiliatesModel.swift
//  Salud-total
//
//  Created by csanchez on 11/26/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper

class ListAffiliatesModel: Mappable {
    
    var cell = Cells.infoPersona
    var description = ""
    var active = false
    var alto = 83
    
    var dataUserAffiliate = ConsultaGrupoFamiliarModel(JSON: [:])
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        cell <- map["cell"]
        description <- map["description"]
        active <- map["active"]
        alto <- map["alto"]
        
        dataUserAffiliate <- map["dataUserAffiliate"]
        
    }
    
}
