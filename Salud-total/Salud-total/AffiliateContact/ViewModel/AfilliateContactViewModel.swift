//
//  AfilliateContactViewModel.swift
//  Salud-total
//
//  Created by iMac on 12/10/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit
class AfilliateContactViewModel:UIViewController  {
    
    struct Cells {
        let normal="normal"
        let button="button"
        let desc="desc"
    }
    
    struct Ids {
        let desc="desc"
        let street="street"
        let department="department"
        let town="town"
        let telephone="telephone"
        let movile="movile"
        let mail="mail"
        let button="button"
    }
    
    typealias CellModel =  (cell:String,id:String,text:String,img:String,value:String)
    
    var container=[CellModel]()
    let ids=Ids()
    let cells=Cells()
    var beneficiary=BeneficiaryModel(JSON:["":""])!
    
    func inflateTable(){
         let city=findCityBy(by:.CiudadId, word:beneficiary.CiudadId )
        container.append((cell:cells.desc,id:ids.desc,text:"DATOS DE RESIDENCIA",img:"stGeneralUbicacion",value:""))
        container.append((cell:cells.normal,id:ids.street,text:"Dirección completa",img:"stGeneralUbicacion",value:beneficiary.Direccion))
        container.append((cell:cells.normal,id:ids.department,text:"Departamento",img:"stGeneralUbicacion",value:city.Departamento))
        container.append((cell:cells.normal,id:ids.town,text:"Municipio / Ciudad",img:"stGeneralUbicacion",value:city.NombreMostrar))
        
        container.append((cell:cells.normal,id:ids.telephone,text:"Teléfono fijo",img:"stGeneralTelefono",value:String(describing:beneficiary.Telefono)))
        
        container.append((cell:cells.normal,id:ids.movile,text:"Teléfono celular",img:"stGeneralCelular",value:beneficiary.Celular))
        
        container.append((cell:cells.normal,id:ids.mail,text:"Correo electrónico",img:"stGeneralCorreoelectronico",value:beneficiary.CorreoElectronico))
        container.append((cell:cells.button,id:ids.button,text:"",img:"",value:""))
    }
    
}
