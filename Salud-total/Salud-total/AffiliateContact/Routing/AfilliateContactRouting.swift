//
//  AfilliateContactRouting.swift
//  Salud-total
//
//  Created by iMac on 12/10/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

extension AfilliateContactViewController{
    static func instantiate() -> AfilliateContactViewController {
        let storyboard = UIStoryboard(name: "AffiliateContact", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! AfilliateContactViewController
        return vc
    }
    func goToEdit(){
        let vc = EditInformationAffiliateViewController.instantiate()
        vc.vm.beneficiary=vm.beneficiary
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
