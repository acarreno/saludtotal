//
//  DirectoryMapRouting.swift
//  Salud-total
//
//  Created by iMac on 12/17/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

extension DirectoryMapViewController{
    static func instantiate() -> DirectoryMapViewController {
        let storyboard = UIStoryboard(name: "DirectoryMap", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! DirectoryMapViewController
        return vc
    }
    
}
