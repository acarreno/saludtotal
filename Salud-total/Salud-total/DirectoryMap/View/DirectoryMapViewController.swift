//
//  DirectoryMapViewController.swift
//  Salud-total
//
//  Created by iMac on 12/17/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit
import GoogleMaps

class DirectoryMapViewController: UIViewController {

    let vm=DirectoryMapViewModel()
    let myWindow=UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initMap()
        
        // Do any additional setup after loading the view.
    }
    
    func initMap(){
        // Create a GMSCameraPosition that tells the map to display the
        // coordinate -33.86,151.20 at zoom level 6.
        let camera = GMSCameraPosition.camera(withLatitude: 4.655576, longitude: -74.060590, zoom: 16.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        view = mapView
        mapView.settings.myLocationButton = true
        mapView.isMyLocationEnabled = true
        mapView.delegate = self
        
        var bounds = GMSCoordinateBounds()
        
        for item in  vm.container{
            let marker = GMSMarker()
            let lgn=Double(item.longitud) ?? -74.083953
            let lat=Double(item.latitud) ?? 4.668011
            print(lgn)
            print(lat)
            marker.position = CLLocationCoordinate2D(latitude:lat, longitude:lgn)
            marker.title = item.nombre
            marker.snippet = item.direccion
            marker.map = mapView
            bounds = bounds.includingCoordinate(marker.position)
            //mapView.addSubview(retInfoWidow(directory:item))
        }
       
        mapView.animate(with: GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: 50.0 , left: 50.0 ,bottom: 50.0 ,right: 50.0)))
        
        // Creates a marker in the center of the map.
        
    }
    func retInfoWidow(directory:DirectoryModel)->UIView{
        
        let lblName=UILabel()
        let lblDir=UILabel()
        let lblHour=UILabel()
        myWindow.frame.size.height=117
        myWindow.frame.size.width=290
        myWindow.backgroundColor=#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        myWindow.roundCorners(corners: [.topLeft,.topRight,.bottomLeft,.bottomRight], radius: 10)
        
        lblName.frame.size.width=280
        lblName.frame.origin.x=10
        lblName.frame.size.height=30
        lblName.textAlignment = .left
        lblName.font.withSize(12)
        lblName.textColor = #colorLiteral(red: 0.2500286698, green: 0.3332767487, blue: 0.618616581, alpha: 1)
        lblName.text=directory.nombre
        lblName.frame.origin.y=10
        
        myWindow.addSubview(lblName)
        
        lblDir.frame.size.width=280
        lblDir.frame.origin.x=10
        lblDir.frame.origin.y=30
        lblDir.frame.size.height=30
        lblDir.textAlignment = .left
        lblDir.font.withSize(12)
        lblDir.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        lblDir.text=directory.direccion
        
        myWindow.addSubview(lblDir)
        
        lblHour.frame.size.width=280
        lblHour.frame.origin.x=10
        lblHour.frame.origin.y=40
        lblHour.frame.size.height=50
        lblHour.lineBreakMode=NSLineBreakMode(rawValue: 0)!
        lblHour.textAlignment = .left
        lblHour.font.withSize(12)
        lblHour.textColor = #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
        lblHour.text="Horarios: \n\(directory.horario)"
        
         myWindow.addSubview(lblHour)
        
        return myWindow
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DirectoryMapViewController:GMSMapViewDelegate{
   
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        NSLog("marker was tapped")
        
        let location = marker.position
        //get position of tapped marker
        let position = marker.position
        mapView.animate(toLocation: position)
        let point = mapView.projection.point(for: position)
        let newPoint = mapView.projection.coordinate(for: point)
        let camera = GMSCameraUpdate.setTarget(newPoint)
        mapView.animate(with: camera)
        let directory=marker.userData as! [String:Any]
        let newView=retInfoWidow(directory:DirectoryModel(JSON: directory)!)
        newView.center = mapView.projection.point(for: location)
        newView.center.y = myWindow.center.y - 103
        
        debugPrint(directory)
        
        let opaqueWhite = UIColor(white: 1, alpha: 0.85)
        mapView.addSubview(newView)
        
        return false
    }
}
