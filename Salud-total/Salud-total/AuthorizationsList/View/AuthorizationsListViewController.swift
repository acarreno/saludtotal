//
//  AuthorizationsListViewController.swift
//  Salud-total
//
//  Created by iMac on 12/26/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class AuthorizationsListViewController: UIViewController {

    @IBOutlet weak var table: UITableView!
    
    let vm=AuthorizationsListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vm.inflateTable()
        table.separatorStyle = .none
       
        
        // Do any additional setup after loading the view.
    }
    @objc func tapSelect(_ sender:UIButton){
        
        openPicker(list: vm.retListFamiliars()) { (index) in
            self.vm.container[sender.tag].value=self.vm.familiars[index].nombre
            self.vm.selectedFamiliar=self.vm.familiars[index]
            self.table.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .none)
            self.showLoading()
            self.vm.getAuthorizations(susses: {
                self.hiddenLoading()
                self.vm.inflateAuthorizations()
                self.table.reloadData()
            }) { (error) in
                self.hiddenLoading()
                self.alert( msj: error, callback: {
                    self.navigationController?.popViewController(animated: true)
                })
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AuthorizationsListViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item=vm.container[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: item.cell, for: indexPath) as! AuthorizationsListTableViewCell
        switch item.cell {
        case vm.cells.select:
            cell.selectButton.tag=indexPath.row
            cell.lblPlaceHolder.textColor=UIColor(red:186/255,green:186/255,blue:186/255,alpha:1)
            cell.selectButton.tag=indexPath.row
            cell.selectButton.removeTarget(nil, action: nil, for: .allEvents)
            
            if (item.value) != ""{
                cell.lblPlaceHolder.text=(item.value )
                cell.lblPlaceHolder.textColor=UIColor(red:0,green:0,blue:0,alpha:1)
            }
            cell.selectButton.addTarget(self, action: #selector(tapSelect(_:)), for: .touchDown)
        case vm.cells.detail:
            cell.lblDetail.text=item.obj!.nombre_Area
            cell.lblCreation.text=item.obj!.fecha_Uso
            cell.lblFinish.text=item.obj!.fecha_Vto
            cell.lblHeadquarters.text=item.obj!.nombre_IPS
            
        default:
            break
        }
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm.container.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item=vm.container[indexPath.row]
        if item.cell==vm.cells.detail{
            showLoading()
            vm.selectedAuthorizations=vm.container[indexPath.row].obj
            vm.getDetail(susses: {
                self.hiddenLoading()
                self.goToDetail(detail: self.vm.detailOrder!)
            }) { (error) in
                self.hiddenLoading()
                self.alert( msj: error, callback: {
                    debugPrint("error in detail autorization")
                })
            }
    
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(vm.container[indexPath.row].height)
    }
    
}
