//
//  AuthorizationsListTableViewCell.swift
//  Salud-total
//
//  Created by iMac on 12/26/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class AuthorizationsListTableViewCell: UITableViewCell {

    @IBOutlet weak var lblNaMeItem: UILabel!
    @IBOutlet weak var lblPlaceHolder: UILabel!
    @IBOutlet weak var txtItem: UITextField!
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var icon: UIImageView!
    
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var lblCreation: UILabel!
    @IBOutlet weak var lblFinish: UILabel!
    @IBOutlet weak var lblHeadquarters: UILabel!
    @IBOutlet weak var lblStreet: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
