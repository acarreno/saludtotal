//
//  AuthorizationsListViewModel.swift
//  Salud-total
//
//  Created by iMac on 12/26/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//


import Foundation
import UIKit
class AuthorizationsListViewModel  {
    
    struct Cells {
        let select="select"
        let detail="detail"
        
    }
    
    struct Sizes {
        let select=98
        let detail=182
        
        
    }
    
    typealias CellModel =  (cell:String,value:String,obj:AuthorizationModel?,height:Int)
    
    var container=[CellModel]()
    
    var sizes=Sizes()
    var cells=Cells()
    var manager=AuthorizationListManager()
    var familiars=[AutorizationFamiliarModel]()
    var authorizations=[AuthorizationModel]()
    var selectedAuthorizations=AuthorizationModel(JSON: ["":""])
    var selectedFamiliar=AutorizationFamiliarModel(JSON: ["":""])
    var detailOrder=DetailAuthorizationModel(JSON: ["":""])
   var mail=""
    
    func inflateTable(){
        container.append(
            (
                cell:cells.select,
                value:"",
                obj:nil,
                height:sizes.select
              )
        )
        
        
    }
    
    func retListFamiliars()->[String]{
        var list=[String]()
        for item in familiars {
            list.append(item.nombre)
        }
        return list
    }
    func getAuthorizations(susses:@escaping ()->(),failed:@escaping (String)->()){
        manager.getAuthorizations(idAfi:"\(selectedFamiliar!.iD_Afiliado)",susses: { (res) in
            self.authorizations=res
            susses()
        }) { (error) in
            failed(error)
        }
    }
    func getDetail(susses:@escaping ()->(),failed:@escaping (String)->()){
        manager.getDetail(id: "\(selectedAuthorizations!.iD_TM_ResultadoTramiteSolicitud)", susses: { (res) in
            self.detailOrder=res
            susses()
            debugPrint(res)
        }) { (error) in
            failed(error)
            debugPrint(error)
        }
    }
    func sendMail(susses:@escaping ()->(),failed:@escaping (String)->()){
        manager.sendMail(result: "\(selectedAuthorizations!.iD_TM_ResultadoTramiteSolicitud)", email: "jeison-c13@hotmail.com", susses: { () in
            debugPrint("...")
            susses()
        }) { (error) in
            failed(error)
        }
    }
    func renovateAuth(susses:@escaping ()->(),failed:@escaping (String)->()){
        manager.renovateAuth(result: "\(selectedAuthorizations!.iD_TM_ResultadoTramiteSolicitud)", susses: {
            susses()
        }) { (error) in
           failed(error)
        }
        
    }
    func inflateAuthorizations(){
        container=[]
        container.append(
            
                (
                    cell:cells.select,
                    value:selectedFamiliar!.nombre,
                    obj:nil,
                    height:sizes.select
                )
            
        )
        for item in authorizations {
            container.append(
                (
                    cell:cells.detail,
                    value:"",
                    obj:item,
                    height:sizes.detail
                )
            )
        }
    }
}
