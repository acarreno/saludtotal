//
//  AuthorizationsListRouting.swift
//  Salud-total
//
//  Created by iMac on 12/26/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//


import Foundation
import UIKit

extension AuthorizationsListViewController{
    static func instantiate() -> AuthorizationsListViewController {
        let storyboard = UIStoryboard(name: "AuthorizationsList", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! AuthorizationsListViewController
        return vc
    }
    func goToDetail(detail:DetailAuthorizationModel) {
        let vc=DetailAutorizationViewController.instantiate()
        vc.vm=vm
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func openPicker(list:[String],callback:@escaping (Int)->()){
        JACSSelectViewController.open(navController: self.navigationController!, list: list) { (index) in
            callback(index)
        }
    }
}
