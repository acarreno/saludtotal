//
//  AuthorizationModel.swift
//  Salud-total
//
//  Created by iMac on 12/30/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper

class AuthorizationModel : Mappable {
   
    var codigo_IPS = ""
    var errordescripcion = ""
    var errornum = 0
    var fecha_Uso = ""
    var fecha_Vto = ""
    var iD_TM_ResultadoTramiteSolicitud = 0
    var naP_Consulta = ""
    var naP_Usuario = ""
    var nombre_Area = ""
    var nombre_IPS = ""
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        codigo_IPS <- map["codigo_IPS"]
        errordescripcion <- map["errordescripcion"]
        errornum <- map["errornum"]
        fecha_Uso <- map["fecha_Uso"]
        fecha_Vto <- map["fecha_Vto"]
        iD_TM_ResultadoTramiteSolicitud <- map["iD_TM_ResultadoTramiteSolicitud"]
        naP_Consulta <- map["naP_Consulta"]
        naP_Usuario <- map["naP_Usuario"]
        nombre_Area <- map["nombre_Area"]
        nombre_IPS <- map["nombre_IPS"]
        
    }
    
    
}
