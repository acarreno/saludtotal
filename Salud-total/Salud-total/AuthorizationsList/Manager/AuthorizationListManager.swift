//
//  AuthorizationListManager.swift
//  Salud-total
//
//  Created by iMac on 12/30/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class AuthorizationListManager: UIViewController {
    func getServices(susses:@escaping ([AutorizationServiceModel])->(),failed:@escaping (String)->()){
        servGet(url: URlGet().ConsultaServicioAutorizaApp, isHeaders: true) { (finish, res) in
            if finish{
                debugPrint(res)
                let response=res as![[String:Any]]
                susses(Mapper<AutorizationServiceModel>().mapArray(JSONObject: (response) )!)
            }else{
                failed(res as! String)
            }
        }
    }
    func getAuthorizations(idAfi:String,susses:@escaping ([AuthorizationModel])->(),failed:@escaping (String)->()){
        let url="\(URlGet().AutorizacionPorAfiliado)?Id_Afiliado=\(idAfi)"
        
        servGet(url: url, isHeaders: true) { (finish, res) in
            if finish{
                debugPrint(res)
                let response=res as![[String:Any]]
                susses(Mapper<AuthorizationModel>().mapArray(JSONObject: (response) )!)
            }else{
                failed(res as! String)
            }
        }
    }
    func getDetail(id:String,susses:@escaping (DetailAuthorizationModel)->(),failed:@escaping (String)->()){
        let url="\(URlGet().AutorizacionDetallePorIDResultadoTramite)?ID_ResultadoTramite=\(id)"
        
        servGet(url: url, isHeaders: true) { (finish, res) in
            if finish{
                debugPrint(res)
                let response=res as![[String:Any]]
                susses(DetailAuthorizationModel(JSON:response[0])!)
            }else{
                failed(res as! String)
            }
        }
    }
    func sendMail(result:String,email:String,susses:@escaping ()->(),failed:@escaping (String)->()){
        let url="\(URlGet().ID_ResultadoTramite)?ID_ResultadoTramite=\(result)&emailDestino=\(email)"
        
        servGet(url: url, isHeaders: true) { (finish, res) in
            if finish{
                debugPrint(res)
                let response=res as![String:Any]
                if response["mensajError"] != nil || (response["mensajError"] as! String) != ""{
                    failed(response["mensajError"] as! String)
                }else{
                    susses()
                }
                //susses(DetailAuthorizationModel(JSON:response[0])!)
            }else{
                failed(res as! String)
            }
        }
    }
    func renovateAuth(result:String,susses:@escaping ()->(),failed:@escaping (String)->()){
        let url="\(URlGet().ProrrogarServicioApp)?pIdResultadoTramite=\(result)&pHacerProrrogar=1&pOpcion=1"
        
        servGet(url: url, isHeaders: true) { (finish, res) in
            if finish{
                debugPrint(res)
                let response=res as![[String:Any]]
                
                if response[0]["mensajeInformacionRetorno"] != nil || (response[0]["mensajeInformacionRetorno"] as! String) != ""{
                    failed(response[0]["mensajeInformacionRetorno"] as! String)
                }else{
                    susses()
                }
                //susses(DetailAuthorizationModel(JSON:response[0])!)
            }else{
                failed(res as! String)
            }
        }
    }
}
