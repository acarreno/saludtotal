//
//  RegisterViewModel.swift
//  Salud-total
//
//  Created by csanchez on 12/23/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

protocol RegisterViewModelProtocol {
    func alert(_ text: String, popView: Bool)
    func subShowLoading()
    func subHiddenLoading()
    func updateButtonEnable(_ enable: Bool)
    func viewValidatePass(_ getCapitalLeter: Bool, _ getNumberReg: Bool, _ getMaxLeght: Bool)
    func confirPasswordsMatch(_ show: Bool)
}

class RegisterViewModel: MasterViewModel {
    
    var delegate: RegisterViewModelProtocol!
    var listDocumentType = [String]()
    var dataSent = FrmRegisterModel(JSON: ["":""])!
    
    var documentType: String? {
        didSet {
            validateForm()
        }
    }
    
    var documentNumber: String? {
        didSet {
            validateForm()
        }
    }
    
    var email: String? {
        didSet {
            validateForm()
        }
    }
    
    var confirmEmail: String? {
        didSet {
            validateForm()
        }
    }
    
    var password: String? {
        didSet {
            validateForm()
            validatePass()
        }
    }
    
    var confirmPasword: String? {
        didSet {
            validateForm()
            validatePass()
            passwordsMatch()
        }
    }
    
    var enableTermsAndConditions: Bool = false {
        didSet {
            validateForm()
        }
    }
    
    private var enableButonSingUp: Bool = false {
        didSet {
            delegate.updateButtonEnable(enableButonSingUp)
        }
    }
    
    func initLoad() {
        delegate.subShowLoading()
        RegisterManager.preLogin { (isError, text) in
            self.delegate.subHiddenLoading()
            if isError {
                self.delegate.alert(text, popView: true)
            }else{
                self.validateForm()
                for item in self.pickerData {
                    self.listDocumentType.append(item.name)
                }
            }
        }
    }
    
    func validateForm(){
        var valid = true
        let validData = [documentNumber, documentType, email, confirmEmail, password, confirmPasword]
        var numErrors = 0
        
        for item in validData {
            let conuntItem = item?.count ?? 0
            if conuntItem == 0 {
                numErrors += 1
            }
        }
        
        let documentNumberValidate = documentNumber?.count ?? 0
        let isValitemail = validateEmail(email ?? "")
        var isConfirmEmail = false
        if email == confirmEmail {
            isConfirmEmail = true
        }
        
        if numErrors > 0 || !enableTermsAndConditions ||  documentNumberValidate <= 4 || !isValitemail || !isConfirmEmail {
            valid = false
        }
        
        enableButonSingUp = valid
    }
    
    func passwordsMatch(){
        var showText = false
        if confirmPasword != "" && password == confirmPasword {
            showText = true
        }
        
        delegate.confirPasswordsMatch(showText)
    }
    
    func validatePass(){
        var capitalLeter = false
        var numberReg = false
        var maxLeght = false
        
        let capitalLetterRegEx  = ".*[A-Z]+.*"
        let upperTest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
        capitalLeter = upperTest.evaluate(with: password)
        
        let numberRegEx  = ".*[0-9]+.*"
        let numberTest = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        numberReg = numberTest.evaluate(with: password)
        
        if let _ = password, password!.count >= 8 {
            maxLeght = true
        }
        
        delegate.viewValidatePass(capitalLeter, numberReg, maxLeght)
    }
    
    func register(_ callback: @escaping ()->()){
        delegate.subShowLoading()
        let document = retTypeDoc(searchBy: .name, search: documentType!)!
        
        dataSent = FrmRegisterModel(JSON: [
            "documentType": document.code,
            "documentNumber": documentNumber!,
            "email": confirmEmail!,
            "password": confirmPasword!
        ])!
        print(dataSent.toJSON())
        RegisterManager.generateTempPassword(
            tipoDocumento: String(dataSent.documentType),
            documento: dataSent.documentNumber,
            correo: dataSent.email,
        susses: {
            self.delegate.subHiddenLoading()
            //UserDefaults.standard.set(dataSent.toJSONString(), forKey: "data_frmRegister")
            callback()
        }) { (erorrText) in
            self.delegate.subHiddenLoading()
            self.delegate.alert(erorrText, popView: false)
        }
    }
    
}
