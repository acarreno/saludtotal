//
//  BerificationCodeModel.swift
//  Salud-total
//
//  Created by Cristian Sanchez on 28/12/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

protocol BerificationCodeModelProtocol {
    func alert(_ text: String, popView: Bool)
    func subShowLoading()
    func subHiddenLoading()
    func updateButtonEnable(_ enable: Bool)
}

class BerificationCodeModel: MasterViewModel {
    
    var lastTwoNumbers = "XXXXXXXX00"
    var dataFrmRegister = FrmRegisterModel(JSON: [:])!
    var code = ""
    let description = "<i style='text-align:center;font-family:\"Futura\";font-size:15px;color:rgb(128,128,128)'>Hemos enviado un mensaje de texto con un  código de verificación de <span style='color:rgb( 0,159,227)'>6 dígitos</span> al siguiente <span style='color:rgb( 0,159,227)'>número celular registrado en Salud Total:</span></i>"
    let timeCode = "<i style='text-align:center;font-family:\"Futura\";font-size:15px;color:rgb(128,128,128)'>Recuerda que el código es válido durante: <span style='color:rgb( 0,159,227)'>10 Minutos</span></i>"
    
    var delegate: BerificationCodeModelProtocol!
    
    func loadInitView(){
        if let codeNumber = UserDefaults.standard.string(forKey: "code_number"){
            print(codeNumber)
            lastTwoNumbers = "XXXXXXXX\(codeNumber)"
        }
        /*if let getDataFrmRegister = UserDefaults.standard.string(forKey: "data_frmRegister"){
            let string = getDataFrmRegister
            let data = string.data(using: .utf8)!
            do {
                if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String: Any]
                {
                    dataFrmRegister = FrmRegisterModel(JSON: jsonArray)!
                    print(dataFrmRegister.toJSON())
                } else {
                    print("bad json")
                }
            } catch let error as NSError {
                print(error)
            }
        }*/
    }
    
    func validate(callback:@escaping ()->()){
        let resposeRegiste = responseRegidterModel(JSON: [
            "tipoUsuario":Int(dataFrmRegister.documentType)!,
            "tipoIdUsuario":Int(dataFrmRegister.documentType)!,
            "IdUsuario": dataFrmRegister.documentNumber,
            "Correo": dataFrmRegister.email,
            "Clave": dataFrmRegister.password
        ])!
        RegisterManager.Registro(
            documentType: Int(dataFrmRegister.documentType)!,
            documentNumber: dataFrmRegister.documentNumber,
            code: code,
            data: resposeRegiste,
            susses: {
                callback()
        }) { (error) in
            self.delegate.alert(error, popView: false)
        }
    }
    
}
