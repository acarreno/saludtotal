//
//  BerificationCodeRouting.swift
//  Salud-total
//
//  Created by csanchez on 12/27/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

extension BerificationCodeViewController{
    
    static func instantiate() -> BerificationCodeViewController {
        
        let storyboard = UIStoryboard(name: "BerificationCode", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! BerificationCodeViewController
        return vc
        
    }
    
    func goToSusses(){
        let vc=RegisteredSuccessfulViewController.instantiate()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
