//
//  Register.swift
//  Salud-total
//
//  Created by iMac on 11/7/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

extension RegisterViewController{
    
    static func instantiate() -> RegisterViewController {
        
        let storyboard = UIStoryboard(name: "Register", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! RegisterViewController
        return vc
        
    }
    
    func openPicker(list:[String],callback:@escaping (_ index:Int)->()){
        JACSSelectViewController.open(navController: self.navigationController!, list: list) { (index) in
            callback(index)
        }
    }
    
    func openBerificationCode(){
        let vc=BerificationCodeViewController.instantiate()
        vc.vm.dataFrmRegister=vm.dataSent
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
