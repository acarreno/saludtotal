//
//  RegisterSussesRounting.swift
//  Salud-total
//
//  Created by iMac on 1/2/20.
//  Copyright © 2020 Wigilabs. All rights reserved.
//


import Foundation
import UIKit

extension RegisteredSuccessfulViewController{
    static func instantiate() -> RegisteredSuccessfulViewController {
        let storyboard = UIStoryboard(name: "RegisteredSuccessful", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! RegisteredSuccessfulViewController
        return vc
    }
    
}
