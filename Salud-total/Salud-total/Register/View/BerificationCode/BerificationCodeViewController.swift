//
//  BerificationCodeViewController.swift
//  Salud-total
//
//  Created by csanchez on 12/27/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class BerificationCodeViewController: UIViewController {
    
    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var descrip: UILabel!
    @IBOutlet weak var timeCode: UILabel!
    
    @IBOutlet weak var characterOne: UITextField! {
        didSet{
            characterOne.delegate = self
            characterOne.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        }
    }
    
    @IBOutlet weak var characterTwo: UITextField! {
        didSet{
            characterTwo.delegate = self
            characterTwo.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        }
    }
    
    @IBOutlet weak var characterThree: UITextField! {
        didSet{
            characterThree.delegate = self
            characterThree.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        }
    }
    @IBOutlet weak var characterFour: UITextField! {
        didSet{
            characterFour.delegate = self
            characterFour.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        }
    }
    @IBOutlet weak var characterFive: UITextField! {
        didSet{
            characterFive.delegate = self
            characterFive.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        }
    }
    @IBOutlet weak var characterSix: UITextField! {
        didSet{
            characterSix.delegate = self
            characterSix.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        }
    }
    
    @IBOutlet weak var btnValidate: UIButton!
    
    let vm = BerificationCodeModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        // Do any additional setup after loading the view.
    }
    
    func initView(){
        addHeader()
        hideBack()
        
        vm.delegate = self
        
        vm.loadInitView()
        
        characterOne.delegate = self
        characterTwo.delegate = self
        characterThree.delegate = self
        characterFour.delegate = self
        characterFive.delegate = self
        characterSix.delegate = self
        enableButton(false)
        
        descrip.attributedText = vm.description.htmlToAttributedString
        timeCode.attributedText = vm.timeCode.htmlToAttributedString
        number.text = vm.lastTwoNumbers
    }
    
    func enableButton(_ isEnable: Bool){
        btnValidate.isEnabled = isEnable
        btnValidate.backgroundColor = !isEnable ? #colorLiteral(red: 0.7254901961, green: 0.7254901961, blue: 0.7254901961, alpha: 1) : #colorLiteral(red: 0.1764705882, green: 0.3098039216, blue: 0.5490196078, alpha: 1)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        switch textField.tag {
        case 1:
            if textField.text != "" {
                characterTwo.becomeFirstResponder()
            }
        case 2:
            if textField.text != "" {
                characterThree.becomeFirstResponder()
            }else{
                characterOne.becomeFirstResponder()
            }
        case 3:
            if textField.text != "" {
                characterFour.becomeFirstResponder()
            }else{
                characterTwo.becomeFirstResponder()
            }
        case 4:
            if textField.text != "" {
                characterFive.becomeFirstResponder()
            }else{
                characterThree.becomeFirstResponder()
            }
        case 5:
            if textField.text != "" {
                characterSix.becomeFirstResponder()
            }else{
                characterFour.becomeFirstResponder()
            }
        case 6:
            if textField.text == "" {
                characterFive.becomeFirstResponder()
            }
        default:
            print("no existe")
        }
        
        if characterOne.text != "" &&
            characterTwo.text != "" &&
            characterThree.text != "" &&
            characterFour.text != "" &&
            characterFive.text != "" &&
            characterSix.text != ""
        {
            vm.code = "\(characterOne.text!)\(characterTwo.text!)\(characterThree.text!)\(characterFour.text!)\(characterFive.text!)\(characterSix.text!)"
           
            enableButton(true)
        }else{
            enableButton(false)
        }
    }
    
    @IBAction func validateCode(_ sender: Any) {
        vm.validate(){
            self.goToSusses()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BerificationCodeViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 1
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}

extension BerificationCodeViewController: BerificationCodeModelProtocol {
    
    func updateButtonEnable(_ enable: Bool) {
        enableButton(enable)
    }
    
    func subShowLoading() {
        showLoading()
    }
    
    func subHiddenLoading() {
        hiddenLoading()
    }
    
    func alert(_ text: String, popView: Bool) {
        
        alert(msj: text) {
            if popView {
                self.navigationController?.popViewController(animated: true)
            }
        }
        
    }
    
}
