//
//  RegisterViewController.swift
//  Salud-total
//
//  Created by iMac on 11/7/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var contentForm: UIView!
    @IBOutlet weak var btnSingUp: UIButton!
    
    @IBOutlet weak var capitalLeterActive: SuperView!
    @IBOutlet weak var capitalLeterText: UILabel!
    
    @IBOutlet weak var numberLeterActive: SuperView!
    @IBOutlet weak var numberLeterText: UILabel!
    
    @IBOutlet weak var maxLeghtLeterActive: SuperView!
    @IBOutlet weak var maxLeghtLeterText: UILabel!
    
    @IBOutlet weak var lblTextErrorPassword: UILabel!
    
    @IBOutlet weak var documentType: UILabel!
    @IBOutlet weak var documentNumber: UITextField! {
        didSet{
            documentNumber.delegate = self
            documentNumber.addTarget(self, action: #selector(documentNumberDidChange), for: .editingChanged)
        }
    }
    
    @IBOutlet weak var email: UITextField! {
        didSet{
            email.delegate = self
            email.addTarget(self, action: #selector(emailDidChange), for: .editingChanged)
        }
    }
    
    @IBOutlet weak var confirmEmail: UITextField! {
        didSet{
            confirmEmail.delegate = self
            confirmEmail.addTarget(self, action: #selector(confirmEmailDidChange), for: .editingChanged)
        }
    }
    
    @IBOutlet weak var password: UITextField! {
        didSet{
            password.delegate = self
            password.addTarget(self, action: #selector(passwordDidChange), for: .editingChanged)
            password.isSecureTextEntry = true
        }
    }
    
    @IBOutlet weak var confirmPasword: UITextField! {
        didSet{
            confirmPasword.delegate = self
            confirmPasword.addTarget(self, action: #selector(confirmPaswordDidChange), for: .editingChanged)
            confirmPasword.isSecureTextEntry = true
        }
    }
    
    let vm = RegisterViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        // Do any additional setup after loading the view.
    }
    
    func initView(){
        hideBack()
        scroll.contentSize=CGSize(width: view.frame.width, height: contentForm.frame.origin.y+contentForm.frame.height)
        lblTextErrorPassword.isHidden = true
        addHeader()
        vm.delegate = self
        vm.initLoad()
        
    }
    
    func enableButton(_ isEnable: Bool){
        btnSingUp.isEnabled = isEnable
        btnSingUp.backgroundColor = !isEnable ? #colorLiteral(red: 0.7254901961, green: 0.7254901961, blue: 0.7254901961, alpha: 1) : #colorLiteral(red: 0.1764705882, green: 0.3098039216, blue: 0.5490196078, alpha: 1)
    }
    
    func documentTypeDidChange(){
        if documentType.text != "Selecciona el tipo de identificación" {
            vm.documentType = documentType.text
        }
    }
    
    @objc func documentNumberDidChange(textField: UITextField){
        vm.documentNumber = documentNumber.text ?? ""
    }
    
    @objc func emailDidChange(textField: UITextField){
        vm.email = email.text ?? ""
    }
    
    @objc func confirmEmailDidChange(textField: UITextField){
        vm.confirmEmail = confirmEmail.text ?? ""
    }
    
    @objc func passwordDidChange(textField: UITextField){
        vm.password = password.text ?? ""
    }
    
    @objc func confirmPaswordDidChange(textField: UITextField){
        vm.confirmPasword = confirmPasword.text ?? ""
    }
    
    @IBAction func openLisDocumentType(_ sender: Any) {
        openPicker(list: vm.listDocumentType) { (index) in
            self.documentType.textColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2, alpha: 1)
            self.documentType.text = self.vm.listDocumentType[index]
            self.documentTypeDidChange()
        }
    }
    
    @IBAction func showPassword(_ sender: UIButton) {
        let index = sender.tag
        if index == 0 {
            let textImage = password.isSecureTextEntry ? "stGeneralVercontraseA": "stGeneralOcultarcontraseA"
            sender.setImage(UIImage(named: textImage), for: .normal)
            password.isSecureTextEntry = !password.isSecureTextEntry
        }else{
            let textImage = confirmPasword.isSecureTextEntry ? "stGeneralVercontraseA": "stGeneralOcultarcontraseA"
            sender.setImage(UIImage(named: textImage), for: .normal)
            confirmPasword.isSecureTextEntry = !confirmPasword.isSecureTextEntry
        }
    }
    
    func alidatePass(capitalLeter: Bool, numberReg: Bool, maxLeght: Bool){
        
        capitalLeterActive.backgroundColor = capitalLeter ? #colorLiteral(red: 0.1764705882, green: 0.3098039216, blue: 0.5490196078, alpha: 1):#colorLiteral(red: 0.7254901961, green: 0.7254901961, blue: 0.7254901961, alpha: 1)
        capitalLeterText.textColor = capitalLeter ? #colorLiteral(red: 0.1764705882, green: 0.3098039216, blue: 0.5490196078, alpha: 1):#colorLiteral(red: 0.7254901961, green: 0.7254901961, blue: 0.7254901961, alpha: 1)
        
        numberLeterActive.backgroundColor = numberReg ? #colorLiteral(red: 0.1764705882, green: 0.3098039216, blue: 0.5490196078, alpha: 1):#colorLiteral(red: 0.7254901961, green: 0.7254901961, blue: 0.7254901961, alpha: 1)
        numberLeterText.textColor = numberReg ? #colorLiteral(red: 0.1764705882, green: 0.3098039216, blue: 0.5490196078, alpha: 1):#colorLiteral(red: 0.7254901961, green: 0.7254901961, blue: 0.7254901961, alpha: 1)
        
        maxLeghtLeterActive.backgroundColor = maxLeght ? #colorLiteral(red: 0.1764705882, green: 0.3098039216, blue: 0.5490196078, alpha: 1):#colorLiteral(red: 0.7254901961, green: 0.7254901961, blue: 0.7254901961, alpha: 1)
        maxLeghtLeterText.textColor = maxLeght ? #colorLiteral(red: 0.1764705882, green: 0.3098039216, blue: 0.5490196078, alpha: 1):#colorLiteral(red: 0.7254901961, green: 0.7254901961, blue: 0.7254901961, alpha: 1)
        
    }
    
    func passwordsMatch(getShow: Bool){
        lblTextErrorPassword.isHidden = getShow
    }
    
    @IBAction func acceptTermsAndConditions(_ Button: UIButton) {
        vm.enableTermsAndConditions = !vm.enableTermsAndConditions
        let textImage = vm.enableTermsAndConditions ? "stGeneralCheckCheck" : "stGeneralCheckUncheck"
        Button.setImage(UIImage(named: textImage), for: .normal)
    }
    
    
    @IBAction func singUp(_ sender: Any) {
        //vm.register {
        vm.register {
            self.openBerificationCode()
        }
        
        //}
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension RegisterViewController: RegisterViewModelProtocol {
    
    func confirPasswordsMatch(_ show: Bool) {
        passwordsMatch(getShow: show)
    }
    
    func viewValidatePass(_ getCapitalLeter: Bool, _ getNumberReg: Bool, _ getMaxLeght: Bool) {
        alidatePass(capitalLeter: getCapitalLeter, numberReg: getNumberReg, maxLeght: getMaxLeght)
    }
    
    
    func updateButtonEnable(_ enable: Bool) {
        enableButton(enable)
    }
    
    func subShowLoading() {
        showLoading()
    }
    
    func subHiddenLoading() {
        hiddenLoading()
    }
    
    func alert(_ text: String, popView: Bool) {
        
        alert( msj: text) {
            if popView {
                self.navigationController?.popViewController(animated: true)
            }
        }
        
    }
    
}

