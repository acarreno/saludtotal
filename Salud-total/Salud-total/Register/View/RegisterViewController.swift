//
//  RegisterViewController.swift
//  Salud-total
//
//  Created by iMac on 11/7/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var contentForm: UIView!
    
    let vm = RegisterViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scroll.contentSize=CGSize(width: view.frame.width, height: contentForm.frame.origin.y+contentForm.frame.height)
        addHeader()
        
        initView()
        // Do any additional setup after loading the view.
    }
    
    func initView(){
        vm.delegate = self
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RegisterViewController: RegisterViewModelProtocol {
    
    func subShowLoading() {
        showLoading()
    }
    
    func subHiddenLoading() {
        hiddenLoading()
    }
    
    func alert(_ text: String, popView: Bool) {
        
        alert(navController: self.navigationController!, msj: text) {
            if popView {
                self.navigationController?.popViewController(animated: true)
            }
        }
        
    }
    
}
