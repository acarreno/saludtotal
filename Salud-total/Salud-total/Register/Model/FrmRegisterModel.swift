//
//  FrmRegisterModel.swift
//  Salud-total
//
//  Created by csanchez on 12/27/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import ObjectMapper

class FrmRegisterModel: Mappable {
    
    var documentType = ""
    var documentNumber = ""
    var email = ""
    var password = ""
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        documentType <- map["documentType"]
        documentNumber <- map["documentNumber"]
        email <- map["email"]
        password <- map["password"]
    }
}
