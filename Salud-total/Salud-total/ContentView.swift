//
//  ContentView.swift
//  Salud-total
//
//  Created by iMac on 11/5/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
