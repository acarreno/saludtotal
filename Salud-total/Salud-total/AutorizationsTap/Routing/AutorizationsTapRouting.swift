//
//  AutorizationsTapRouting.swift
//  Salud-total
//
//  Created by iMac on 12/26/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

extension AutorizationsTapViewController{
    static func instantiate() -> AutorizationsTapViewController {
        let storyboard = UIStoryboard(name: "AutorizationsTap", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! AutorizationsTapViewController
        return vc
    }
    
}
