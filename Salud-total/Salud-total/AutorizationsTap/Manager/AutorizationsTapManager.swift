//
//  AutorizationsTapManager.swift
//  Salud-total
//
//  Created by iMac on 12/27/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class AutorizationsTapManager:UIViewController {
    
    func getTokeAuthorization(susses:@escaping ()->(),failed:@escaping (String)->()){
        let token=retToken()
        let url = "\(URlGet().getTokenAutorizaciones)/?Token=\(token)&Origen=APP"
        self.servGet(url:url , callback: { (finish, res) in
            if (res is [String:Any]){
                let response=GetTokenModel(JSON: (res as! [String:Any]))!
                
                if finish{
                    UserDefaults.standard.set("bearer  \(response.token)", forKey: "tokenAutorization")
                    susses()
                    
                }else{
                    failed(response.Error)
                    
                }
            }
        })
        
        
    }
    
    func getFamiliarGroup(typeDoc:String,idDoc:String,susses:@escaping ([AutorizationFamiliarModel])->(),failed:@escaping (String)->()){
        print(typeDoc)
        let typeDoc = retTypeDoc(searchBy: .code, search: typeDoc)!
        let url="\(URlGet().AutorizacionGrupoFamiliar)?pTipoDoc=\(typeDoc.id)&pDocumento=\(idDoc)&pOrigen=3"
        servGet(url: url,isHeaders: true) { (finish, res) in
            if finish{
                let response=res as! [[String:Any]]
                let list=Mapper<AutorizationFamiliarModel>().mapArray(JSONObject: (response) )!
                susses(list)
            }else{
                failed(res as! String)
            }
        }
    }
    
}
