//
//  AutorizationsTapViewModel.swift
//  Salud-total
//
//  Created by iMac on 12/26/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation

class AutorizationsTapViewModel  {
    
    let manager=AutorizationsTapManager()
    var usr=UserModel(JSON: ["":""])!
    var familiars=[AutorizationFamiliarModel]()
    
    func initViewModel(susses:@escaping ()->(),failed:@escaping (String)->()){
        getToken(susses: {
            
            self.getFamiliarGroup(susses: {
                susses()
            }, failed: { (error) in
                failed(error)
            })
            
        }) { (error) in
            failed(error)
        }
    }
    
    func getToken(susses:@escaping ()->(),failed:@escaping (String)->()){
        manager.getTokeAuthorization(susses: {
            susses()
        }) { (error) in
            failed(error)
        }
    }
    
    func getFamiliarGroup(susses:@escaping ()->(),failed:@escaping (String)->()){
        manager.getFamiliarGroup(typeDoc: usr.codeDocument, idDoc: usr.DocumentoId, susses: { (res) in
            self.familiars=res
            susses()
        }) { (error) in
            failed(error)
        }
        
    }
}
