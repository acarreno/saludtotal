//
//  AutorizationFamiliarModel.swift
//  Salud-total
//
//  Created by iMac on 12/27/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper

class AutorizationFamiliarModel:Mappable {
    var ciudadres = "";
    var codestado = "";
    var codtipodoc = "";
    var contrato = "";
    var dociden = "";
    var estado = "";
    var fechaNac = "";
    var iD_Afiliado = 0;
    var id = ""
    var nombre = "";
    var parentesco = ""
    var telcontacto = "";
    var telres = "";
    var tipodoc = "";
    
    required init?(map: Map){
        
    }
    func mapping(map: Map) {
        
        ciudadres <- map["ciudadres"]
        codestado <- map["codestado"]
        codtipodoc <- map["codtipodoc"]
        contrato <- map["contrato"]
        dociden <- map["dociden"]
        estado <- map["estado"]
        iD_Afiliado <- map["iD_Afiliado"]
        id <- map["id"]
        nombre <- map["nombre"]
        parentesco <- map["parentesco"]
        telcontacto <- map["telcontacto"]
        telres <- map["telres"]
        tipodoc <- map["tipodoc"]
        
    }

}


