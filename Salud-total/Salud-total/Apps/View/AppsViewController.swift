//
//  AppsViewController.swift
//  Salud-total
//
//  Created by Andrey Carreño on 11/16/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class AppsViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    let vm=AppsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addHeader()
        vm.inflateTable()
       
        vm.login(){(finish,res)in
            if finish{
                self.vm.getApps(){(finish,res)in
                    if finish{
                        self.collectionView.reloadData()
                    }else{
                        self.alert( msj: res, callback: {
                            self.navigationController?.popViewController(animated: true)
                        })
                    }
                    
                }
            }else{
                self.alert( msj: res, callback: {
                    self.navigationController?.popViewController(animated: true)
                })
            }
        }
        // Do any additional setup after loading the view.
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        UserDefaults.standard.removeObject(forKey: "token")
        self.navigationController?.isNavigationBarHidden=true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AppsViewController: UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return vm.container.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item=vm.container[indexPath.row]
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: "item", for: indexPath) as! AppsCollectionViewCell

        cell.imageApp.loadImage(item.Url_Imagen)

        cell.nameApp.text=item.NombreAPP
        cell.sizeApp.text=item.PesoAPP_IOS
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = view.frame.width/3
        let height = CGFloat(139)
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item=vm.container[indexPath.row]
        guard let url = URL(string: item.Url_TiendaIOS) else { return }
        UIApplication.shared.open(url)
    }
    

    func convertBase64ToImage(_ imageString: String) -> UIImage {
        let imageData = Data(base64Encoded: imageString, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
        return UIImage(data: imageData)!
    }

}
