//
//  AppsCollectionViewCell.swift
//  Salud-total
//
//  Created by iMac on 11/25/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class AppsCollectionViewCell: UICollectionViewCell {
    
   
    @IBOutlet weak var imageApp: UIImageView!
    @IBOutlet weak var nameApp: UILabel!
    @IBOutlet weak var sizeApp: UILabel!
    
}
