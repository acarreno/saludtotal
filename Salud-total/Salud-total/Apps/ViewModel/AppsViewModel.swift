//
//  AppsViewModel.swift
//  Salud-total
//
//  Created by Andrey Carreño on 11/16/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class AppsViewModel:UIViewController{
    
    struct Cells {
        let item = "item"
        
    }
    
    
    typealias CellModel =  (cell:String,height:Int,obj:Any?)
    
    let cells=Cells()
    var container=[RelatedAppModel]()
    
    
    func getApps(callback:@escaping (Bool,String)->()){
        
        servGet(url: URlGet().AppRelacionadas, isHeaders: true) { (finish, res) in
            if finish{

                let list=res as! [[String:Any]]

               self.container = Mapper<RelatedAppModel>().mapArray(JSONObject: (list) )!
                callback(true,"")
            }else{
                callback(false,"Error interno")
                
            }
        }
    }
    
    func login(callback:@escaping (Bool,String)->()){
        let url="\(URlGet().login)?PerfilUsuario=AFI&TipoEmpleadorId=N&EmpleadorId=800149453&TipoDocumentoId=C&DocumentoId=999&Clave=saludtotal&aplicativo=APP"
        servGet(url: url) { (finish, res) in
            debugPrint(res)
            if finish{
                if let response=LoginModel(JSON: res as! [String:Any]){
                    if response.Valido{
                        UserDefaults.standard.set(response.Token, forKey: "token")
                        
                        self.getToken(token:response.Token){(finish,res)in
                            if finish{
                                callback(true,response.Token)
                            }else{
                                callback(false,response.Token)
                            }
                        }
                    }else{
                        callback(false,response.Error)
                    }
                    
                }
                
            }
            else{
                callback(false,res as!String)
            }
            
            
        }
    }
    
    func getToken(token:String,callback:@escaping (Bool,String)->()){
        let url = "\(URlGet().GetTokenApps)?Token=\(token)&Origen=APP"
        servGet(url: url) { (finish, res) in
            if finish{
                if let response=LoginModel(JSON: res as! [String:Any]){
                    if response.Valido{
                        UserDefaults.standard.set(response.Token, forKey: "token")
                        UserDefaults.standard.set(response.Token, forKey: "tokenAutorization")
                        callback(true,response.Token)
                        
                    }else{
                        callback(false,response.Error)
                    }
                    
                }
                
            }
            else{
                callback(false,res as!String)
            }
            debugPrint(res)
        }
    }
    func inflateTable(){
        
        
    }
}
