//
//  AppsRouting.swift
//  Salud-total
//
//  Created by Andrey Carreño on 11/16/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit
extension AppsViewController{
    
    static func instantiate() -> AppsViewController {
        
        let storyboard = UIStoryboard(name: "Apps", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! AppsViewController
        return vc
        
    }
    
}
