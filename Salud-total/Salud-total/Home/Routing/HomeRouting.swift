//
//  HomeRouting.swift
//  Salud-total
//
//  Created by iMac on 11/12/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

extension HomeViewController {
    
    static func instantiate() -> HomeViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HomeViewController
        return vc
    }
    
    func openMenu(){
        MenuViewController.open(navController: self.navigationController!){ res in
            print(res)
            
            switch res {
            case "aplications":
                self.goToApps()
            case "closeSesion":
                self.openCloseSession()
            case "notifications":
                self.goToNotifications()
            case "data":
                self.goToData()
            case "news":
                self.goToNews()
            case "directory":
                self.goToDirectory()
            default:
                break
            }
        }
    }
    func openCloseSession(){
        CloseSessionViewController.open(navController: self.navigationController!) {
            
            self.navigationController?.pushViewController(InitViewController.instantiate(), animated: true)
        }
    }
    
    func goToAfiliationState(){
        let vc=AfiliationStateViewController.instantiate()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoCertificados(){
        let vc=CertificacionesViewController.instantiate()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goToAffiliate(){
        let vc=AffiliateViewController.instantiate()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func goToNotifications(){
        let vc=NotificationsViewController.instantiate()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func goToData(){
        let vc=AffiliateDataTabViewController.instantiate()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func goToHistorial(){
        let vc=HistoryViewController.instantiate()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func goToApps(){
        let vc=AppsViewController.instantiate()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoLaboratorios(){
        let vc = LaboratoriosViewController.instantiate()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func goToNews() {
        let vc=WebViewViewController.instantiate()
        vc.vm.title="NOTICIAS"
        let url=UserDefaults.standard.string(forKey: "newsUrl")
        vc.vm.url=url!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goToAutorizations(){
        let vc = AutorizationsTapViewController.instantiate()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goToDirectory(){
        let vc=DirectoryFilterViewController.instantiate()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
