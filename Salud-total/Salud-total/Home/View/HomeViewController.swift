//
//  HomeViewController.swift
//  Salud-total
//
//  Created by iMac on 11/12/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var lblName: UILabel!
    
    let vm = HomeViewModel()
    
    override func viewWillAppear(_ animated: Bool){
        self.navigationController?.isNavigationBarHidden=true
         hideBack()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showLoading()
        sessionManager(loading: true) {
            self.vm.inflateTable()
            self.table.reloadData()
            //goToAffiliate()
            self.vm.getToken { (finish, res) in
                debugPrint(res)
                self.hiddenLoading()
                if finish{
                    self.getUserInfo()
                }else{
                    self.alert( msj:res , callback: {
                        debugPrint("close")
                    })
                }
            }
            self.table.separatorStyle = .none
            self.navigationController?.isNavigationBarHidden=true
        }
        table.separatorStyle = .none
        self.navigationController?.isNavigationBarHidden=true
        //getUserInfo()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func getUserInfo(){
        let retUs=retUser()
        if retUs.error{
            alert( msj: "No se encuentra información para este usuario, intenta ingresar de nuevo") {
                let defaults = UserDefaults.standard
                let dictionary = defaults.dictionaryRepresentation()
                dictionary.keys.forEach { key in
                    defaults.removeObject(forKey: key)
                    
                }
                self.navigationController?.popViewController(animated: true)
            }
        }else{
            
        let user = retUs.user
            
            let url="\(URlGet().ConsultaGrupoFamiliarDetalle)?BeneficiarioId=\(user.DocumentoId)&BeneficiarioTipoId=\(user.codeDocument)&BeneficiarioConsecutivo=0"
            showLoading()
            servGet(url:url,isHeaders:true) { (finish, res) in
                self.hiddenLoading()
                if finish {
                    let response=res as![[String:Any]]
                    if response.count>0{
                        let item=response[0]
                        let beneficiary = BeneficiaryModel(JSON: item)!
                        UserDefaults.standard.set(beneficiary.toJSON(), forKey: "beneficiary")
                       self.lblName.text=beneficiary.Nombres
                    }else{
                        self.closeSeSSion()
                        self.alert( msj: "Su sesión ha expirado.", callback: {
                            self.navigationController?.popViewController(animated: true)
                        })
                    }
                }else{
                    self.alert( msj: res as! String, callback: {
                        print("close")
                    })
                }
                debugPrint(res)
            }
        }
    }
    
    @IBAction func tapNotifications(_ sender: Any) {
        goToNotifications()
    }
    
    @IBAction func tapMenu(_ sender: Any) {
        openMenu()
    }
    
    
}

extension HomeViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item=vm.container[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: item.cell, for: indexPath) as! HomeTableViewCell
        if item.cell == vm.cells.item{
            cell.icon.image=item.image!
            cell.itemName.text=item.text!.uppercased()
        }
        
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm.container.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = vm.container[indexPath.row]
        switch item.id {
        case vm.ids.afliationState:
            goToAfiliationState()
        case vm.ids.clinicalHistory:
            goToHistorial()
        case vm.ids.certificate:
            gotoCertificados()
        case vm.ids.labs:
            gotoLaboratorios()
        case vm.ids.register:
            goToAutorizations()
        default:
            break
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return vm.container[indexPath.row].height
    }
    
}
