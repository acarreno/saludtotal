//
//  HomeViewModel.swift
//  Salud-total
//
//  Created by iMac on 11/12/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
class HomeViewModel: UIViewController{
    
    struct Cells {
        let item="item"
        let notification = "notification"
    }
    
    struct Ids {
        let notification = "notification"
        let date = "date"
        let register = "register"
        let filing = "filing"
        let certificate = "certificate"
        let afliationState = "afliationState"
        let labs = "labs"
        let ipsChange = "ipsChange"
        let clinicalHistory = "clinicalHistory"
        let chat = "chat"
        let medicalOrientation = "medicalOrientation"
    }
    
    typealias Data = (text:String,image:UIImage)
    
    typealias CellModel =  (
        cell:String,
        id:String,
        image:UIImage?,
        text:String?,
        height:CGFloat
    )

    var container=[CellModel]()
    
    let cells=Cells()
    let ids=Ids()
    
    func inflateTable(){
        
        
        container.append((
            cell:cells.notification,
            id:ids.notification,
            image:nil,
            text:nil,
            height:145
        ))
        container.append((
            cell:cells.item,
            id:ids.date,
            image:UIImage(named: "stHomePosCitasmedicas"),
            text:"CITAS MÉDICAS",
            height:60))
        container.append((
            cell:cells.item,
            id:ids.register,
            image:UIImage(named: "stHomePosAutorizaciones"),
            text:"Registrar autorización",
            height:60
        ))
        container.append((
            cell:cells.item,
            id:ids.filing,
            image:UIImage(named: "stHomePosRadicaciones"),
            text:"Radicación de solicitud",
            height:60
        ))
        container.append((
            cell:cells.item,
            id:ids.certificate,
            image:UIImage(named: "stHomePosCertificaciones"),
            text:"certificaciones",
            height:60
        ))
        
        container.append((
            cell:cells.item,
            id:ids.afliationState,
            image:UIImage(named: "stHomePosEstadodeafiliacion"),
            text:"ESTADO DE AFILIACIÓN",
            height:60
        ))
        container.append((
            cell:cells.item,
            id:ids.labs,
            image:UIImage(named: "stHomePosLaboratorios"),
            text:"laboratorios",
            height:60
        ))
        container.append((
            cell:cells.item,
            id:ids.ipsChange,
            image:UIImage(named: "stHomePosCambiodeips"),
            text:"CAMBIO DE IPS",
            height:60
        ))
        container.append((
            cell:cells.item,
            id:ids.clinicalHistory,
            image:UIImage(named: "stHomePosHistoriaclinica"),
            text:"Historia clínica",
            height:60
        ))
        container.append((
            cell:cells.item,
            id:ids.chat,
            image:UIImage(named: "stHomePosChateaconnosotros"),
            text:"Chatea con nosotros",
            height:60
        ))
        container.append((
            cell:cells.item,
            id:ids.medicalOrientation,
            image:UIImage(named: "stHomePosOrientacionmedica"),
            text:"ORIENTACIÓN MÉDICA",
            height:60))
    }
    
    func getToken(callback:@escaping (Bool,String)->()){
        let url="\(URlGet().getTokenAfiliados)?Token=\(retToken())&Origen=APP"
        servGet(url: url) { (finish, res) in
            debugPrint(res)
            
            if finish{
                if let response=LoginModel(JSON: res as! [String:Any]){
                    if response.Valido{
                        
                        UserDefaults.standard.set(response.Token, forKey: "tokenAutorization")
                         callback(true,"")
                        
                    }else{
                        callback(false,response.Error)
                        
                        
                    }
                    
                }
                
            }
            else{
                callback(false,res as!String)
            }
            debugPrint(res)
        }
    }
    
    /*func getToken(callback:@escaping (Bool,String)->()){
        
        let url = "\(URlGet().token)?Token=\(tokenLogin)&Origen=APP"
        servGet(url: url) { (finish, res) in
            debugPrint(res)
            
            let response=LoginModel(JSON: res as! [String:Any])!
            if finish{
                
                UserDefaults.standard.set(response.Token, forKey: "tokenLogin")
                
                callback(true,"")
                
            }else{
                callback(false,response.Error)
            }
            
            
        }
    }*/
}
