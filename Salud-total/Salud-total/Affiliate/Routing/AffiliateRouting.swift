//
//  AffiliateRouting.swift
//  Salud-total
//
//  Created by iMac on 11/14/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

extension AffiliateViewController{
    
    static func instantiate() -> AffiliateViewController {
        
        let storyboard = UIStoryboard(name: "Affiliate", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! AffiliateViewController
        return vc
        
    }
    
}
