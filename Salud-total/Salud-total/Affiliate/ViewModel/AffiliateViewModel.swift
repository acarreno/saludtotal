//
//  AffiliateViewModel.swift
//  Salud-total
//
//  Created by iMac on 11/14/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
class AffiliateViewModel:UIViewController {
    
    struct Cells {
        let select="select"
        let textfield="textfield"
        let textArea="textArea"
        let disclaimer="disclaimer"
        let instructions="instructions"
        let politics="politics"
        let terms="terms"
        let button="button"
        
        
    }
    
    struct Ids {
        let name="name"
        let lastName="lastName"
        let typeID="typeID"
        let identification="identification"
        let phone="phone"
        let mobile="mobile"
        let mail="mail"
        let confirmMail="confirmMail"
        let city="city"
        let callOur="callOur"
        let coments="coments"
        let autorization="autorization"
    }
    
    struct Sizes {
        let select=98
        let textfield=98
        let textArea=131
        let disclaimer=92
        let instructions=40
        let terms=59
        let button=68
        let politics=99
    }
    
    struct typeField {
        let text=UIKeyboardType.default
        let email=UIKeyboardType.emailAddress
        let pass="pass"
        let number=UIKeyboardType.numberPad
    }
    
    typealias CellModel =  (cell:String,id:String,value:String,alert:String,placeholder:String,type:UIKeyboardType?,label:String,height:Int)
    
    let cells=Cells()
    let sizes=Sizes()
    var container=[CellModel]()
    let ids=Ids()
    var pickerData=[TypeDocumentModel]()
    var cities=[City]()
    var typeSelected=""
    var typeCode=""
    var selectedDate:Date?
    var selectCity=""
    var selectCityID=""
    var isAccept=false
    var request="?Nombres={{names}}&Apellidos={{lastname}}&TipoDocumento={{type}}&DocumentoId={{id}}&Telefono={{phone}}&Celular={{mobile}}&Email={{mail}}&Hora={{hour}}&Comentarios={{comment}}&producto=2&ciudad={{city}}&edad=1"
    var typeF=typeField()
    
    func login(callback:@escaping (Bool,String)->()){
        let url="\(URlGet().login)?PerfilUsuario=AFI&TipoEmpleadorId=N&EmpleadorId=800149453&TipoDocumentoId=C&DocumentoId=999&Clave=saludtotal&aplicativo=APP"
        servGet(url: url) { (finish, res) in
            debugPrint(res)
            if finish{
                if let response=LoginModel(JSON: res as! [String:Any]){
                    if response.Valido{
                        UserDefaults.standard.set(response.Token, forKey: "token")
                        
                        self.getTokenAffi(token:response.Token){(finish,res)in
                            if finish{
                               callback(true,response.Token)
                            }else{
                                callback(false,response.Token)
                            }
                        }
                    }else{
                        callback(false,response.Error)
                    }
                    
                }
                
            }
            else{
                callback(false,res as!String)
            }
            
            
        }
    }
    func getTokenAffi(token:String,callback:@escaping (Bool,String)->()){
        let url = "\(URlGet().GetTokenAfiliarme)?Token=\(token)&Origen=APP"
        servGet(url: url) { (finish, res) in
            if finish{
                if let response=LoginModel(JSON: res as! [String:Any]){
                    if response.Valido{
                        UserDefaults.standard.set(response.Token, forKey: "token")
                        UserDefaults.standard.set(response.Token, forKey: "tokenAutorization")
                        callback(true,response.Token)
                        self.getCities(token:response.Token){(finish,res)in
                            if finish {
                                callback(true,res)
                            }else{
                                callback(false,res)
                            }
                        }
                    }else{
                        callback(false,response.Error)
                    }
                    
                }
                
            }
            else{
                callback(false,res as!String)
            }
            debugPrint(res)
        }
    }
    func getCities(token:String,callback:@escaping (Bool,String)->()) {
        let url=URlGet().TraerCiudadesPac
        servGet(url:url, isHeaders: true) { (finish, res) in
            if finish{
                let list=res as! [[String:Any]]
                self.cities=Mapper<City>().mapArray(JSONObject:(list))!
                debugPrint(res)
                callback(true,"")
            }else{
                callback(false,"Error interno")
            }
           
        }
    }
    
    func validateForm()->(isError:Bool,alert:String){
        let isError=false
        let alert=""
        for item in container {
            switch item.cell{
            case cells.textfield:
                if item.value==""{
                   return (true,item.alert)
                }
            case cells.select:
                if typeSelected == "" && item.id==ids.typeID{
                    return (true,item.alert)
                }
                if selectedDate == nil && item.id==ids.callOur{
                   return (true,item.alert)
                }
                if selectCity == "" && item.id==ids.city{
                    return (true,item.alert)
                }
            case cells.textArea:
                if !ids.mail.isValidEmail(){
                    return (true,"Ingresa un email valido")
                }else{
                    if item.value==""{
                        return (true,item.alert)
                    }
                }
                
            case cells.terms:
                if !isAccept{
                    return (true,item.alert)
                }
            default:
                break
            }
        }
        return (isError,alert)
    }
    func register(callback:@escaping (Bool,String)->()){
        
        for item in container {
            switch item.id{
            case ids.name:
                request=request.replacingOccurrences(of: "{{names}}", with:"\(item.value)", options: .literal, range: nil)
            case ids.lastName:
                request=request.replacingOccurrences(of: "{{lastname}}", with:"\(item.value)", options: .literal, range: nil)
                
            case ids.callOur:
                let dateStr=String(describing:selectedDate)
                let arrDate=dateStr.split(separator: " ")
                
                request=request.replacingOccurrences(of: "{{hour}}", with:"\(arrDate[1])", options: .literal, range: nil)
            case ids.mail:
                request=request.replacingOccurrences(of: "{{mail}}", with:"\(item.value)", options: .literal, range: nil)
            case ids.mobile:
                request=request.replacingOccurrences(of: "{{mobile}}", with:"\(item.value)", options: .literal, range: nil)
            case ids.phone:
                request=request.replacingOccurrences(of: "{{phone}}", with:"\(item.value)", options: .literal, range: nil)
            case ids.coments:
                request=request.replacingOccurrences(of: "{{comment}}", with:"\(item.value)", options: .literal, range: nil)
            case ids.typeID:
                request=request.replacingOccurrences(of: "{{type}}", with:"\(typeCode)", options: .literal, range: nil)
            case ids.identification:
                request=request.replacingOccurrences(of: "{{id}}", with:"\(item.value)", options: .literal, range: nil)
            case ids.city:
                request=request.replacingOccurrences(of: "{{city}}", with:"\(selectCityID)", options: .literal, range: nil)
            default:
                break
            }
        }
        
        let url="\(URlGet().RegistrarSolicitud)\(request)"
        servGet(url:url, isHeaders: true) { (finish, res) in
            if finish{
               
                debugPrint(res)
                callback(true,"")
            }else{
                callback(false,"Error interno")
            }
            
        }
    }
    func inflateTable(){
        container.append((
            cell:cells.textfield,
            id:ids.name,
            value:"",
            type:typeF.text,
            alert:"Ingresa tus nombres",
            placeholder:"Ingresa tus nombres",
            label:"Nombres",
            height:sizes.textfield
        ))
        container.append((
            cell:cells.textfield,
            id:ids.lastName,
            value:"",
            type:typeF.text,
            alert:"Ingresa tus apellidos",
            placeholder:"Ingresa tus apellidos",
            label:"Apellidos",
            height:sizes.textfield
        ))
        container.append((
            cell:cells.select,
            id:ids.typeID,
            value:"",
            type:typeF.text,
            alert:"Ingresa el tipo de identificación",
            placeholder:"Tipo de identificación",
            label:"Selecciona el tipo de identificación",
            height:sizes.select
        ))
        container.append((
            cell:cells.textfield,
            id:ids.identification,
            value:"",
            type:typeF.text,
            alert:"Número de identificación",
            placeholder:"Número de identificación",
            label:"Ingresa el número de identificación",
            height:sizes.textfield
        ))
        container.append((
            cell:cells.textfield,
            id:ids.phone,
            value:"",
            type:typeF.number,
            alert:"Ingresa tu número de teléfono fijo",
            placeholder:"Ingresa tu número de teléfono fijo",
            label:"Teléfono fijo",
            height:sizes.textfield
        ))
        container.append((
            cell:cells.textfield,
            id:ids.mobile,
            value:"",
            type:typeF.number,
            alert:"Ingresa tu número de celular",
            placeholder:"Ingresa tu número de celular",
            label:"Celular",
            height:sizes.textfield
        ))
        container.append((
            cell:cells.textfield,
            id:ids.mail,
            value:"",
             type:typeF.email,
            alert:"Ingresa tu correo",
            placeholder:"Ingresa tu correo",
            label:"correo ELECTRÓNICO",
            height:sizes.textfield
        ))
        container.append((
            cell:cells.textfield,
            id:ids.confirmMail,
            value:"",
             type:typeF.email,
            alert:"Confirma tu correo",
            placeholder:"Confirma tu correo",
            label:"confirma TU CORREO ELECTRÓNICO",
            height:sizes.textfield
        ))
        container.append((
            cell:cells.select,
            id:ids.city,
            value:"",
             type:nil,
            alert:"Selecciona tu ciudad",
            placeholder:"Selecciona tu ciudad",
            label:"Ciudad",
            height:sizes.select
        ))
        container.append((
            cell:cells.select,
            id:ids.callOur,
            value:"",
             type:nil,
            alert:"Selecciona la hora",
            placeholder:"Selecciona la hora",
            label:"¿A qué hora te podemos llamar?",
            height:sizes.select
        ))
        container.append((
            cell:cells.textArea,
            id:ids.coments,
            value:"",
             type:nil,
            alert:"Ingresa un comentario",
            placeholder:"Ingresa un comentario",
            label:"Comentarios",
            height:sizes.textArea
        ))
        container.append((
            cell:cells.politics,
            id:"",
            value:"",
             type:nil,
            alert:"",
            placeholder:"",
            label:"Los  campos marcados con * son obligatorios",
            height:sizes.politics
        ))
        container.append((
            cell:cells.instructions,
            id:"",
            value:"",
            type:nil,
            alert:"",
            placeholder:"",
            label:"Los  campos marcados con * son obligatorios",
            height:sizes.instructions
        ))
        container.append((
            cell:cells.terms,
            id:ids.autorization,
            value:"",
             type:nil,
            alert:"Autoriza el manejo de tus datos personales",
            placeholder:"",
            label:"¿Autoriza a Salud Total EPS-S el manejo de tus datos personales?",
            height:sizes.terms
        ))
        container.append((
            cell:cells.button,
            id:"",
            value:"",
            type:nil,
            alert:"",
            placeholder:"",
            label:"",
            height:sizes.button
        ))
    }
    func inflateTypePicker(){
        pickerData.append(TypeDocumentModel(
            name:"Seleccionar",
            id:"",
            code:""
        ))
        
        pickerData.append(TypeDocumentModel(
            name:"CEDULA DE CIUDADANIA",
            id:"C",
            code:"1"
        ))
        pickerData.append(TypeDocumentModel(
            name:"CARNET DIPLOMATICO",
            id:"CD",
            code:"2"
        ))
        pickerData.append(TypeDocumentModel(
            name:"CERTIFICADO NACIDO VIVO",
            id:"CN",
            code:"3"
        ))
        pickerData.append(TypeDocumentModel(
            name:"CEDULA DE EXTRANJERIA",
            id:"E",
            code:"4"
        ))
        pickerData.append(TypeDocumentModel(
            name:"PERSONERIA JURIDICA",
            id:"J",
            code:"5"
        ))
        pickerData.append(TypeDocumentModel(
            name:"MENOR DE EDAD",
            id:"M",
            code:"6"
        ))
        pickerData.append(TypeDocumentModel(
            name:"MENOR SIN IDENTIFICACION",
            id:"MS",
            code:"7"
        ))
        pickerData.append(TypeDocumentModel(
            name:"NIT",
            id:"N",
            code:"8"
        ))
        pickerData.append(TypeDocumentModel(
            name:"NÚMERO UNICO DE IDENTIFICACION PERSONAL",
            id:"NU",
            code:"9"
        ))
        pickerData.append(TypeDocumentModel(
            name:"PASAPORTE",
            id:"P",
            code:"10"
        ))
        pickerData.append(TypeDocumentModel(
            name:"Permiso Especial de Permanencia",
            id:"PE",
            code:"11"
        ))
        pickerData.append(TypeDocumentModel(
            name:"REGISTRO CIVIL",
            id:"R",
            code:"12"
        ))
        pickerData.append(TypeDocumentModel(
            name:"SALVOCONDUCTO",
            id:"SC",
            code:"13"
        ))
        pickerData.append(TypeDocumentModel(
            name:"TARJETA DE IDENTIDAD",
            id:"T",
            code:"14"
        ))
    }
}
