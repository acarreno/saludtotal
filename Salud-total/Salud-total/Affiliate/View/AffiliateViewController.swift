//
//  AffiliateViewController.swift
//  Salud-total
//
//  Created by iMac on 11/14/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class AffiliateViewController: UIViewController {
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var documentPicker: UIPickerView!
    @IBOutlet weak var contentDatePicker: UIView!
    
    var isCities=false
    let vm=AffiliateViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addHeader()
        table.separatorStyle = .none
        vm.inflateTable()
        //vm.inflateTypePicker()
        documentPicker.isHidden=true
        
        datePicker.isHidden=true
        datePicker.backgroundColor=UIColor(red: 180/255, green: 180/255, blue: 180/255, alpha: 1)
        contentDatePicker.isHidden=true
        showLoading()
        vm.login { (finish, res) in
            self.hiddenLoading()
            debugPrint(res)
        }
        
        //getTokenAffi()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        UserDefaults.standard.removeObject(forKey: "token")
        self.navigationController?.isNavigationBarHidden=true
    }
    
    @objc func tapSend(_ target : UIButton){
        self.view.endEditing(true)
        let validObj=vm.validateForm()
        if validObj.isError{
            alert(msj:validObj.alert) {
                print("close")
            }
        }else{
            showLoading()
            vm.register { (finish, res) in
                self.hiddenLoading()
                if finish{
                    let html="<span style='color:rgb(0,75,147)'>Pronto estaremos en contacto</span><br><span style='color:rgb(0,0,0)'>¡Tu salud no es a medias, debe ser total!</span><br><span style='color:rgb(128,128,128)'>Gracias por contactarnos</span>"
                    self.alert(msj:html , callback: {
                        self.navigationController?.popViewController(animated: true)
                    })
                }else{
                    self.alert(msj: res, callback: {
                         self.navigationController?.popViewController(animated: true)
                    })
                }
            }
        }
    }
   
    @objc func tapSelect(_ target : UIButton){
        self.view.endEditing(true)
        let item=vm.container[target.tag]
        
        switch item.id {
        case vm.ids.typeID:
            vm.inflateTypePicker()
            isCities=false
            documentPicker.isHidden=false
        case vm.ids.callOur:
            datePicker.isHidden=false
            contentDatePicker.isHidden=false
        case vm.ids.city:
            
            documentPicker.isHidden=false
            isCities=true
        default:
            break
        }
        documentPicker.reloadAllComponents()
       
    }
    
    @IBAction func tapDatePicker(_ sender: UIDatePicker) {
        resignFirstResponder()
        datePicker.isHidden=false
        vm.selectedDate=sender.date
        debugPrint(sender.date)
        table.reloadRows(at: [IndexPath(row: 9, section: 0)], with: .none)
    }
    
    @IBAction func tapOk(_ sender: Any) {
        contentDatePicker.isHidden=true
    }
    @objc func tapAccept(_ sender: UIButton) {
        self.view.endEditing(true)
        vm.isAccept = !vm.isAccept
        table.reloadRows(at:[ IndexPath(row: sender.tag, section: 0)], with: .none)
        
    }
    /*func getTokenAffi(){
        let url="\(URlGet().GetTokenAfiliarme)?Token=\(retToken())&Origen=APP"
        servGet(url: url) { (finish, res) in
            debugPrint(res)
        }
    }*/

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AffiliateViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item=vm.container[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: item.cell, for: indexPath) as! AffiliateTableViewCell
        switch item.cell {
        case vm.cells.select:
            cell.selectButton.tag=indexPath.row
            
            cell.lblPlaceHolder.text=item.placeholder
            let html="<p style='font-family:\"Futura\";font-size:14px'>\(item.label.uppercased()) <span style='color:rgb(45,79,140)'>*</span></p>".htmlToAttributedString
            cell.lblNaMeItem.attributedText=html
            cell.lblPlaceHolder.textColor=UIColor(red:186/255,green:186/255,blue:186/255,alpha:1)
            
            
            switch item.id{
            case vm.ids.typeID:
                cell.lblPlaceHolder.text=item.placeholder
                if vm.typeSelected != ""{
                    cell.lblPlaceHolder.text=vm.typeSelected
                    cell.lblPlaceHolder.textColor=UIColor(red:0,green:0,blue:0,alpha:1)
                }
            case vm.ids.callOur:
                cell.lblPlaceHolder.text=item.placeholder
                if vm.selectedDate != nil {
                    let dateStirng=vm.selectedDate!
                    //var hourArray=vm.selectedDate.split(separator: " ")
                    let formatter = DateFormatter()
                    formatter.dateFormat = "hh:mm a" // "a" prints "pm" or "am"
                    let hourString = formatter.string(from:dateStirng)
                    
                    cell.lblPlaceHolder.text = String(describing:"\(hourString)")
                    cell.lblPlaceHolder.textColor = UIColor(red:0,green:0,blue:0,alpha:1)
                }
                
            case vm.ids.city:
                cell.lblPlaceHolder.text=item.placeholder
                if vm.selectCity != ""{
                        cell.lblPlaceHolder.text=vm.selectCity
                        cell.lblPlaceHolder.textColor=UIColor(red:0,green:0,blue:0,alpha:1)
                }
                
            default:
                break
            }
           
                cell.selectButton.addTarget(self, action: #selector(tapSelect(_:)), for: .touchDown)
           
            
            
        case vm.cells.textfield:
            cell.txtItem.text=""
            cell.txtItem.placeholder=item.placeholder
            cell.txtItem.tag=indexPath.row
            cell.txtItem.delegate=self
            cell.txtItem.keyboardType = item.type!
            let html="<p style='font-family:\"Futura\";font-size:14px'>\(item.label.uppercased()) <span style='color:rgb(45,79,140)'>*</span></p>".htmlToAttributedString
            
            cell.lblNaMeItem.attributedText=html
            if item.value != ""{
                cell.txtItem.text=item.value
            }
        case vm.cells.textArea:
             let html="<p style='font-family:\"Futura\";font-size:14px'>\(item.label.uppercased()) <span style='color:rgb(45,79,140)'>*</span></p>".htmlToAttributedString
             
            cell.lblNaMeItem.attributedText=html
            cell.textarea.delegate=self
            cell.textarea.text = item.placeholder
            cell.textarea.textColor = UIColor.lightGray
            cell.textarea.tag=indexPath.row
             if item.value != ""{
                cell.textarea.text = item.value
            }
        case vm.cells.button:
            cell.btnSend.addTarget(self, action: #selector(tapSend(_:)), for: .touchDown)
        case vm.cells.terms:
            cell.checkButton.tag=indexPath.row
            cell.checkButton.addTarget(self, action: #selector(tapAccept(_:)), for: .touchDown)
            
            cell.checkButton.setImage(UIImage(named:"elipse412"), for: .normal)
            if vm.isAccept{
                 cell.checkButton.setImage(UIImage(named:"stGeneralCheckCheck"), for: .normal)
                
            }
        default:
            break
        }
        
        
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm.container.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(vm.container[indexPath.row].height)
    }
    
}
extension AffiliateViewController: UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {

        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = UIColor.black
            
        }
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        vm.container[textView.tag].value=textView.text!
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView.isFirstResponder {
            
            let validString = CharacterSet(charactersIn: " !@#$%^&*()_+{}[]|\"<>,.~`/:;?-=\\¥'£•¢")
            
            if (textView.textInputMode?.primaryLanguage == "emoji") || textView.textInputMode?.primaryLanguage == nil {
                return false
            }
            if let range = text.rangeOfCharacter(from: validString)
            {
                print(range)
                return false
            }
            
            
        }
        return true
    }
    
}
extension AffiliateViewController: UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        vm.container[textField.tag].value=textField.text!
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.isFirstResponder {
            if vm.container[textField.tag].id != vm.ids.mail{
                let validString = CharacterSet(charactersIn: " !@#$%^&*()_+{}[]|\"<>,.~`/:;?-=\\¥'£•¢")
                
                if (textField.textInputMode?.primaryLanguage == "emoji") || textField.textInputMode?.primaryLanguage == nil {
                    return false
                }
                if let range = string.rangeOfCharacter(from: validString)
                {
                    print(range)
                    return false
                }
            }
            if vm.container[textField.tag].id == vm.ids.identification || vm.container[textField.tag].id == vm.ids.mobile || vm.container[textField.tag].id == vm.ids.phone{
                guard let textFieldText = textField.text,
                    let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                        return false
                }
                let substringToReplace = textFieldText[rangeOfTextToReplace]
                let count = textFieldText.count - substringToReplace.count + string.count
                return count <= 10
            }
        }
      
        return true
    }
}
extension AffiliateViewController: UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
       return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var count=0
        if isCities{
            count=vm.cities.count
        }else{
            count=vm.pickerData.count
        }
        return count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var name = ""
        if isCities{
            name = vm.cities[row].Nombre
        }else{
            name = vm.pickerData[row].name
        }
        return name
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if isCities{
            if row != 0{
                vm.selectCity=vm.cities[row].Nombre
                vm.selectCityID=String(describing:vm.cities[row].CiudadId)
                documentPicker.isHidden=true
                table.reloadRows(at: [IndexPath(row: 8, section: 0)], with: .none)
            }
        }else{
            vm.typeSelected=""
            vm.typeCode=""
            
            if row != 0{
                vm.typeSelected=vm.pickerData[row].name
                vm.typeCode=vm.pickerData[row].code
                documentPicker.isHidden=true
                table.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .none)
            }
        }
        
    }
}
