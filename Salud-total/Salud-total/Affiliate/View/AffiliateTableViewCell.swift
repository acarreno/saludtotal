//
//  AffiliateTableViewCell.swift
//  Salud-total
//
//  Created by iMac on 11/14/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class AffiliateTableViewCell: UITableViewCell {

    @IBOutlet weak var lblNaMeItem: UILabel!
    
    @IBOutlet weak var lblPlaceHolder: UILabel!
    
    @IBOutlet weak var txtItem: UITextField!
    
    @IBOutlet weak var textarea: UITextView!
    
    
    @IBOutlet weak var lblLarge: UILabel!
    
    @IBOutlet weak var shortLabel: UILabel!
    
    
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var selectButton: UIButton!
    
    @IBOutlet weak var btnSend: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
