//
//  EditInformationManager.swift
//  Salud-total
//
//  Created by iMac on 12/23/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class EditInformationAffiliateManager:UIViewController{
    
    func sendForm(request:DtaContactReQuestModel,susses:@escaping (String)->(),failed:@escaping (String)->()){
        let url = "\(URlGet().ACtualizarDatosAfiliado)?BeneficiarioTipoId=\(request.BeneficiarioTipoId)&BeneficiarioId=\(request.BeneficiarioId)&direccioncotizante=\(request.direccioncotizante)&telefonocotizante=\(request.telefonocotizante)&sMail=\(request.sMail)&sCelular=\(request.sCelular)&sCiudadId=\(request.sCiudadId)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        servGet(url: url!, isHeaders: true) { (finish, res) in
            
            if finish{
                let respone=res as! [String:Any];
                susses(respone["Descripcion"] as! String)
            }else{
                failed(res as!String)
            }
        }
    }
    func getTowns(susses:@escaping ([TownModel])->(),failed:@escaping (String)->()){
        
        let url=URlGet().GetDepartamentos
        servGet(url: url, isHeaders: true) { (finish, res) in
            if finish{
                let list = res as! [[String:Any]]
                let listDep=Mapper<TownModel>().mapArray(JSONObject: (list) )!
                susses(listDep)
            }else{
                
                failed(res as! String)
            }
            
        }
    }
    func getCities(townId:String,susses:@escaping ([CityGenericModel])->(),failed:@escaping (String)->()){
        
        let url="\(URlGet().Getciudades)?coddepartamento=\(townId)"
        servGet(url: url, isHeaders: true) { (finish, res) in
            if finish{
                let list = res as! [[String:Any]]
                let listDep=Mapper<CityGenericModel>().mapArray(JSONObject: (list) )!
                susses(listDep)
            }else{
                
                failed(res as! String)
            }
            
        }
    }
}
