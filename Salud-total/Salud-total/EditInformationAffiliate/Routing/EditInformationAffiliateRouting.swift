//
//  EditInformationAffiliateRouting.swift
//  Salud-total
//
//  Created by iMac on 12/12/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//


import Foundation
import UIKit

extension EditInformationAffiliateViewController{
    static func instantiate() -> EditInformationAffiliateViewController {
        let storyboard = UIStoryboard(name: "EditInformationAffiliate", bundle: Bundle.main)
        
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! EditInformationAffiliateViewController
       
        return vc
    }
    func openPicker(list:[String],callback:@escaping (_ index:Int)->()){
        JACSSelectViewController.open(navController: self.navigationController!, list: list) { (index) in
            callback(index)
        }
    }
}
