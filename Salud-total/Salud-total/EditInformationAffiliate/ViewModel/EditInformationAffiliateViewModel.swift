//
//  EditInformationAffiliateViewModel.swift
//  Salud-total
//
//  Created by iMac on 12/12/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//


import Foundation
import UIKit
class EditInformationAffiliateViewModel:UIViewController  {
    
    struct Cells {
        let select="select"
        let textfield="textfield"
        let button="button"
        let separator="separator"
        let dir="dir"
        let section="section"
        let simpleSelect="simpleSelect"
    }
    
    struct Ids {
        let department="department"
        let town="town"
        let street="street"
        let numbreStreet="numbreStreet"
        let appartMent="appartMent"
        let telephone="telephone"
        let mobile="mobile"
        let email="email"
        
    }
    struct Sizes {
        let select=98
        let textfield=108
        let separator=20
        let dir=78
        let button=68
        let section=44
        let simpleSelect=60
    }
    
    typealias CellModel =  (cell:String,id:String,value:String,alert:String,placeholder:String,type:UIKeyboardType?,icon:String,label:String,height:Int,obligatory:Bool)
    
    var container=[CellModel]()
    let cells=Cells()
    let ids=Ids()
    let sizes=Sizes()
    var citySelect=CityGenericModel(JSON: ["":""])!
    var depSelect=City(JSON: ["":""])!
    var defCity=City(JSON: ["":""])!
    var listDep=[String]()
    var listCity=[CityGenericModel]()
    
    var listParcDep=[City]()
    
    var streetOpTions=[String]()
    let manager=EditInformationAffiliateManager()
    var request=DtaContactReQuestModel(JSON: ["":""])
    var firtsPartDir=""
    var secondPartDir=""
    var thirtPartDir=""
    var fourPartDir=""
    var beneficiary=BeneficiaryModel(JSON: ["":""])!
    var listTowns=[TownModel]()
    var selectedTown=TownModel(JSON: ["":""])!
    
    func initViewModel(){
        
        defCity = findCityBy(by: .CiudadId, word:  beneficiary.CiudadId)
        //citySelect = defCity
        depSelect = defCity
        
        
    }
    func inflateTable(){
        container.append((
            cell:cells.section,
            id:"",
            value:"",
            alert:"",
            placeholder:"",
            type:UIKeyboardType.alphabet,
            icon:"",
            label:"DATOS DE RESIDENCIA",
            height:sizes.section,
            obligatory:false
        ))
        container.append((
            cell:cells.separator,
            id:"",
            value:"",
            alert:"",
            placeholder:"",
            type:UIKeyboardType.alphabet,
            icon:"",
            label:"",
            height:sizes.separator,
            obligatory:false
        ))
        container.append((
            cell:cells.select,
            id:ids.department,
            value: depSelect.Departamento,
            alert:"Selecciona el departamento",
            placeholder:"Selecciona el departamento",
            type:UIKeyboardType.alphabet,
            icon:"stGeneralUbicacion",
            label:"Departamento",
            height:sizes.select,
            obligatory:true
        ))
        container.append((
            cell:cells.select,
            id:ids.town,
            value: defCity.NombreMostrar,
            alert:"Selecciona el municipio",
            placeholder:"Selecciona el municipio",
            type:UIKeyboardType.alphabet,
            icon:"stGeneralUbicacion",
            label:"Municipio / Ciudad",
            height:sizes.select,
            obligatory:true
        ))
        
        container.append((
            cell:cells.textfield,
            id:ids.street,
            value:beneficiary.Direccion,
            alert:"",
            placeholder:"Ej: Calle 14",
            type:UIKeyboardType.alphabet,
            icon:"stGeneralUbicacion",
            label:"Dirección",
            height:sizes.textfield,
            obligatory:true
        ))
        
        container.append((
            cell:cells.separator,
            id:"",
            value:"",
            alert:"",
            placeholder:"",
            type:UIKeyboardType.alphabet,
            icon:"",
            label:"",
            height:sizes.separator,
            obligatory:true
        ))
        container.append((
            cell:cells.section,
            id:"",
            value:"",
            alert:"",
            placeholder:"",
            type:UIKeyboardType.alphabet,
            icon:"",
            label:"DATOS ADICIONALES",
            height:sizes.section,
            obligatory:true
        ))
        
        container.append((
            cell:cells.textfield,
            id:ids.telephone,
            value:"\(beneficiary.Telefono)",
            alert:"Ingresa el número de teléfono",
            placeholder:"Ingresa el número de teléfono",
            type:UIKeyboardType.numberPad,
            icon:"stGeneralTelefono",
            label:"Teléfono fijo",
            height:sizes.textfield,
            obligatory:true
        ))
        
        container.append((
            cell:cells.textfield,
            id:ids.mobile,
            value:"\(beneficiary.Celular)",
            alert:"Ingresa el número celular",
            placeholder:"Ingresa el número celular",
            type:UIKeyboardType.numberPad,
            icon:"stGeneralCelular",
            label:"Teléfono celular",
            height:sizes.textfield,
            obligatory:true
        ))
        
        container.append((
            cell:cells.textfield,
            id:ids.email,
            value:beneficiary.CorreoElectronico,
            alert:"Ingresa el Correo electrónico",
            placeholder:"Ingresa el Correo electrónico",
            type:UIKeyboardType.emailAddress,
            icon:"stGeneralCorreoelectronico",
            label:"Correo electrónico",
            height:sizes.textfield,
            obligatory:true
        ))
        container.append((
            cell:cells.button,
            id:"",
            value:"",
            alert:"",
            placeholder:"",
            type:UIKeyboardType.alphabet,
            icon:"",
            label:"",
            height:sizes.button,
            obligatory:true
        ))
    }
    
    
    func inflateOptionsStreet(){
        streetOpTions.append("Calle")
        streetOpTions.append("Carrera")
        streetOpTions.append("Avenida")
        streetOpTions.append("Diagonal")
        streetOpTions.append("Transversal")
    }
    func sendFrm(susses:@escaping (String)->(),failed:@escaping (String)->()){
       // let user=retBeneficiary()
        let sessInfo=retUser()
        
        request?.BeneficiarioId=sessInfo.user.DocumentoId 
        request?.BeneficiarioTipoId=sessInfo.user.codeDocument
        
        request?.sCiudadId="\(selectedTown.CodDepartamento)\(citySelect.CodMunicipio)"
        manager.sendForm(request:request!,susses: { (res) in
            susses(res)
        }) { (res) in
            failed(res)
        }
    }
    
    func retTowns()->[String]{
        var list=[String]()
        for item in listTowns {
            list.append(item.Departamento)
        }
        return list
    }
    
    func retCities()->[String]{
        var list=[String]()
        for item in listCity {
            list.append(item.NombreMostrar)
        }
        return list
    }
    func validateForm()->(error:Bool,alert:String){
        
        for item in container {
            
            if item.cell==cells.simpleSelect || item.cell==cells.select || item.cell == cells.textfield{
                if item.value == "" {
                    return (error:true,alert:item.alert)
                }
                switch item.id{
                case ids.street:
                    request?.direccioncotizante="\(item.value)\(secondPartDir)#\(thirtPartDir)-\(fourPartDir)"
                    //request?.direccioncotizante=item.value
                case ids.telephone:
                    request?.telefonocotizante=item.value
                case ids.email:
                    request?.sMail=item.value
                case ids.mobile:
                    request?.sCelular=item.value
                case ids.town:
                    request?.sCiudadId=item.value
                default:
                    break
                }
            }
        }
        return (error:false,alert:"")
    }
    func initViewModel(susses:@escaping ()->(),failed:@escaping (String)->()){
        manager.getTowns(susses: { (res) in
            self.listTowns=res
            self.inflateTable()
            susses()
        }) { (error) in
            failed(error)
        }
    }
    func getCities(susses:@escaping ()->(),failed:@escaping (String)->()){
        
        manager.getCities(townId: "\(selectedTown.CodDepartamento)", susses: { (res) in
            self.listCity=res
            susses()
            
        }) { (error) in
            
            failed(error)
            
        }
    }
}
