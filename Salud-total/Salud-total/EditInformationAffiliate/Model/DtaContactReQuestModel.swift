//
//  DtaContactReQuestModel.swift
//  Salud-total
//
//  Created by iMac on 12/23/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper

class DtaContactReQuestModel : Mappable {
   
    var DtaContactReQuestModel=""
    var BeneficiarioTipoId=""
    var BeneficiarioId=""
    var direccioncotizante=""
    var telefonocotizante=""
    var sMail=""
    var sCelular=""
    var sCiudadId=""
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        DtaContactReQuestModel <- map["DtaContactReQuestModel"]
        BeneficiarioTipoId <- map["BeneficiarioTipoId"]
        BeneficiarioId <- map["BeneficiarioId"]
        direccioncotizante <- map["direccioncotizante"]
        telefonocotizante <- map["telefonocotizante"]
        sMail <- map["sMail"]
        sCelular <- map["sCelular"]
        sCiudadId <- map["sCiudadId"]
    }
    
    
}
