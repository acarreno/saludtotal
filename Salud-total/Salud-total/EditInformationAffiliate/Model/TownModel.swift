//
//  TownModel.swift
//  Salud-total
//
//  Created by iMac on 1/7/20.
//  Copyright © 2020 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper
class TownModel: Mappable {
    var CodDepartamento = ""
    var Departamento = ""
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        CodDepartamento <- map["CodDepartamento"]
        Departamento <- map["Departamento"]
    }
}


