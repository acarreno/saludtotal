//
//  gityGenericModel.swift
//  Salud-total
//
//  Created by iMac on 1/7/20.
//  Copyright © 2020 Wigilabs. All rights reserved.
//

import Foundation

import ObjectMapper

class CityGenericModel : Mappable {
    
    var NombreMostrar=""
    var CodDepartamento=""
    var CodMunicipio=""
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        NombreMostrar <- map["NombreMostrar"]
        CodDepartamento <- map["CodDepartamento"]
        CodMunicipio <- map["CodMunicipio"]
        
    }
    
    
}
