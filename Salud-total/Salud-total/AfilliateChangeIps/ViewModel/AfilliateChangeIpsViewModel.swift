//
//  AfilliateChangeIpsViewModel.swift
//  Salud-total
//
//  Created by iMac on 12/11/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//


import Foundation
import UIKit
class AfilliateChangeIpsViewModel:UIViewController  {
    
    struct Cells {
        let normal="normal"
        let button="button"
        let desc="desc"
    }
    
    struct Ids {
        let desc="desc"
        let city="city"
        let IPS="IPS"
        let IPSDent="IPSDent"
        let department="department"
        let town="town"
        let button="button"
    }
    
    typealias CellModel =  (cell:String,id:String,text:String,img:String,value:String)
    
    var container=[CellModel]()
    let ids=Ids()
    let cells=Cells()
    var beneficiary=BeneficiaryModel(JSON: ["":""])!
    let manager=AffiliateChangeIpsFormManager()
    var healtIpsDef=IPSModel(JSON: ["":""])!
    var odontoIpsDef=IPSModel(JSON: ["":""])!
    var ipsHealtList=[IPSModel]()
    var townList=[TownModel]()
    
    func inflateTable(){
        let city=findCityBy(by:.CiudadId, word:beneficiary.CiudadId )
        container.append((cell:cells.desc,id:ids.desc,text:"DATOS DE IPS ACTUAL",img:"",value:""))
        container.append((cell:cells.normal,id:ids.department,text:"Departamento",img:"stGeneralUbicacion",value:city.Departamento))
        container.append((cell:cells.normal,id:ids.city,text:"Ciudad",img:"stGeneralUbicacion",value:city.NombreMostrar))
        container.append((cell:cells.normal,id:ids.IPS,text:"IPS Médica",img:"stGeneralUbicacion",value:healtIpsDef.Nombre))
        container.append((cell:cells.normal,id:ids.IPSDent,text:"IPS Odontológica",img:"stGeneralUbicacion",value:odontoIpsDef.Nombre))
        container.append((cell:cells.button,id:ids.button,text:"",img:"",value:""))
        
    }
    func initViewModel(susses:@escaping ()->(),failed:@escaping (String)->()){
        let resultCity=findCityBy(by: .CiudadId, word: beneficiary.CiudadId)
        manager.getHealtIps(dep: resultCity.CodDepartamento, city: resultCity.CodMunicipio, susses: { (res) in
             self.ipsHealtList=res
            for item in res{
                if item.ipsid == self.beneficiary.ipsmedicaid{
                    self.healtIpsDef=item
                }
            }
            self.getOdontoIps(susses: {
                self.inflateTable()
                susses()
            }, failed: { (error) in
                failed(error)
            })
            
        }) { (error) in
            failed(error)
        }
    }
    func getOdontoIps(susses:@escaping ()->(),failed:@escaping (String)->()){
       
        manager.getOdontIps(idIpsMedica: beneficiary.ipsodontologicaid, susses: { (res) in
           
            for item in res{
                if item.ipsid == self.beneficiary.ipsmedicaid{
                    self.odontoIpsDef=item
                }
            }
            
            susses()
        }) { (error) in
            failed(error)
        }
    }
}
