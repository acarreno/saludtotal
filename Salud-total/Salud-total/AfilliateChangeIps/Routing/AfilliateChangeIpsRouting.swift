//
//  AfilliateChangeIpsRouting.swift
//  Salud-total
//
//  Created by iMac on 12/11/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

extension AfilliateChangeIpsViewController{
    static func instantiate() -> AfilliateChangeIpsViewController {
        let storyboard = UIStoryboard(name: "AfilliateChangeIps", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! AfilliateChangeIpsViewController
        
        return vc
    }
    func goToEdit(){
        let vc=AffiliateChangeIpsFormViewController.instantiate()
        vc.vm.beneficiary=vm.beneficiary
        vc.vm.defHeakIps=vm.healtIpsDef
        vc.vm.defOdontIps=vm.odontoIpsDef
        vc.vm.healtOptions=vm.ipsHealtList
        vc.vm.listTowns=vm.townList
        //vc.vm.
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
