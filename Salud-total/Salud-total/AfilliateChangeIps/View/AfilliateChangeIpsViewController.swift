//
//  AfilliateChangeIpsViewController.swift
//  Salud-total
//
//  Created by iMac on 12/10/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class AfilliateChangeIpsViewController: UIViewController {

    @IBOutlet weak var table: UITableView!
    
    let vm=AfilliateChangeIpsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vm.initViewModel(susses: {
            self.table.reloadData()
        }) { (error) in
            self.alert(msj: error, callback: {
                
            })
        }
        table.separatorStyle = .none
        hideBack()
        // Do any additional setup after loading the view.
    }
    
    @objc func tapChange(_ target: UIButton){
        goToEdit()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AfilliateChangeIpsViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item=vm.container[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: item.cell, for: indexPath) as! AfilliateChangeIpsTableViewCell
        
        switch item.cell {
        case vm.cells.normal:
            cell.icon.image=UIImage(named:item.img)!
            cell.lblProperty.text=item.text
            cell.lblValue.text=item.value
        case vm.cells.button:
            cell.btnChangeIps.addTarget(self, action: #selector(tapChange(_:)), for: .touchDown)
        case vm.cells.desc:
            break
        default:
            break
        }
        
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm.container.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item=vm.container[indexPath.row]
        var height=0
        switch item.cell {
        case vm.cells.normal:
            height=74
        case vm.cells.button:
            height=66
        case vm.cells.desc:
            height=44
        default:
            break
        }
        return CGFloat(height)
    }
    
}

