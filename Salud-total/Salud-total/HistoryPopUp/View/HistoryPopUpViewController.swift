//
//  HistoryPopUpViewController.swift
//  Salud-total
//
//  Created by Andrey Carreño on 11/16/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class HistoryPopUpViewController: UIViewController {

    @IBOutlet weak var lblInstructions: UILabel!
    
    var susses: ((_ id: String) -> Void)?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let html="<p style='text-align:center;font-family:\"Futura\";font-size:14px;color:rgb(128,128,128)'>Has seleccionado la siguiente historia clínica, puedes <span style='color:rgb( 0,159,227)'>enviarla al correo electrónico registrado o descargarlo en tu dispositivo</span></p>"
        lblInstructions.attributedText=html.htmlToAttributedString
        // Do any additional setup after loading the view.
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true) {
            self.susses?("send")
        }
    }
    @IBAction func tapSend(_ sender: Any) {
        self.dismiss(animated: true) {
            self.susses?("send")
        }
    }
    
    @IBAction func tapDownloand(_ sender: Any) {
        self.dismiss(animated: true) {
            self.susses?("send")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
