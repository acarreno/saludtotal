//
//  HistoryPopUpRouting.swift
//  Salud-total
//
//  Created by Andrey Carreño on 11/16/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation

import UIKit

extension HistoryPopUpViewController{
    
    static func instantiate() -> HomeViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HomeViewController
        return vc
    }
    
    static func open(navController:UINavigationController, callback:@escaping(String)->()) {
        let storyboard = UIStoryboard(name: "HistoryPopUp", bundle: Bundle.main)
        
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HistoryPopUpViewController
        vc.modalPresentationStyle = .overCurrentContext
        vc.susses={(id:String) -> Void in
            
            callback(id)
        }
        
        navController.present(vc, animated: true, completion: nil)
        
    }
    
}
