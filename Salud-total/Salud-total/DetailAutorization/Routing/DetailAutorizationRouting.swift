//
//  DetailAutorizationRouting.swift
//  Salud-total
//
//  Created by iMac on 12/27/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

extension DetailAutorizationViewController{
    static func instantiate() -> DetailAutorizationViewController {
        let storyboard = UIStoryboard(name: "DetailAutorization", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! DetailAutorizationViewController
        return vc
    }
    
}
