//
//  DetailAuthorizationModel.swift
//  Salud-total
//
//  Created by iMac on 12/30/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper
class DetailAuthorizationModel : Mappable {
    
    var cantidad = 0;
    var ciudad = "";
    var ciudadPrestador = "";
    var cod_Descripcion = "";
    var codigoServicio = 0;
    var direccion_Prestador = "";
    var errordescripcion = "";
    var errornum = 0;
    var iD_TM_ResultadoTramiteSolicitud = 0;
    var identificacion_Protegido = 0;
    var municipio_Prestador = ""
    var nap = "";
    var nombreServicio = "";
    var nombre_Prestador = "";
    var nombre_Protegido = "";
    var porcentaje = 0;
    var telefono_Prestador = 0;
    var tipo_Doc_Protegido = "";
    var tipo_Recaudo = "";
    var valor = 0;
    var valor_Maximo = 0;
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        cantidad <- map["cantidad"]
        ciudad <- map["ciudad"]
        ciudadPrestador <- map["ciudadPrestador"]
        cod_Descripcion <- map["cod_Descripcion"]
        codigoServicio <- map["codigoServicio"]
        direccion_Prestador <- map["direccion_Prestador"]
        errordescripcion <- map["errordescripcion"]
        errornum <- map["errornum"]
        iD_TM_ResultadoTramiteSolicitud <- map["iD_TM_ResultadoTramiteSolicitud"]
        identificacion_Protegido <- map["identificacion_Protegido"]
        municipio_Prestador <- map["municipio_Prestador"]
        nombre_Protegido <- map["nombre_Protegido"]
        tipo_Recaudo <- map["tipo_Recaudo"]
        valor <- map["valor"]
        valor_Maximo <- map["valor_Maximo"]
        
    }
    
    
}
