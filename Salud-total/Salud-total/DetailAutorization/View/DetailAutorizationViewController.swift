//
//  DetailAutorizationViewController.swift
//  Salud-total
//
//  Created by iMac on 12/27/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class DetailAutorizationViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblPatient: UILabel!
    @IBOutlet weak var lblService: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblHeadquarterName: UILabel!
    @IBOutlet weak var lblStreet: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblzone: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    
    var vm=AuthorizationsListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.contentSize.height=631
        initView()
        // Do any additional setup after loading the view.
    }
    func initView(){
        lblPatient.text=vm.selectedFamiliar!.nombre
        
        lblService.text=vm.selectedAuthorizations!.nombre_Area
        lblPrice.text="\(vm.detailOrder!.valor)".toPriceNumber()
        lblHeadquarterName.text=vm.selectedAuthorizations!.nombre_IPS
        lblStreet.text=vm.detailOrder!.direccion_Prestador
        lblCity.text=vm.detailOrder!.ciudad
        
        lblPhone.text="\(vm.detailOrder!.telefono_Prestador)"
    }

    @IBAction func tapSend(_ sender: Any) {
       
        JACSAlertMailViewController.open(navController: self.navigationController!, text: "") { (res) in
            self.vm.mail=res
             self.showLoading()
            self.vm.sendMail(susses: {
                self.hiddenLoading()
                
            }) { (error) in
                self.hiddenLoading()
                self.alert( msj: error, callback: {
                    print("error")
                })
            }
        }
        
        
    }
    @IBAction func tapRenovate(_ sender: Any) {
        let msj="este proceso se puede realizar <br> solo una vez<br> <span style='color:rgb(0, 159,227)'>este proceso se puede realizar solo una vez</span><br>¿Estás seguro que deseas realizar la renovación de esta autorización?"
        confirm(msj: msj) {
            self.showLoading()
            self.vm.renovateAuth(susses: {
                self.hiddenLoading()
                self.alert( msj: "Hemos actualizado tus datos correctamente", callback: {
                    self.navigationController?.popViewController(animated: true)
                })
            }, failed: { (error) in
                self.hiddenLoading()
                self.alert( msj: error, callback: {
                    
                })
            })
        }
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
