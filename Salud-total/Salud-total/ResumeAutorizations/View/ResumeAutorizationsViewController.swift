//
//  ResumeAutorizationsViewController.swift
//  Salud-total
//
//  Created by iMac on 12/30/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class ResumeAutorizationsViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblPatient: UILabel!
    @IBOutlet weak var lblService: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblHeadquarterName: UILabel!
    @IBOutlet weak var lblStreet: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblzone: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblQuantity: UILabel!
    
    var vm = CreateAutorizationViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.contentSize.height=630
        initView()
        hideBack()
        // Do any additional setup after loading the view.
    }
    
    func initView(){
        let price="\(vm.selectedHeadquarter!.valorServicio)".toPriceNumber()
        //let ciudad=findCityBy(by: .CiudadId, word: vm.selectedHeadquarter!.ciudad)
        lblPatient.text=vm.selectedFamiliar!.nombre
        lblCity.text=vm.selectedHeadquarter!.ciudad
        lblService.text=vm.selectedAutorizationServiceModel!.servicio
        lblQuantity.text=vm.container[3].value
        lblPrice.text=price
        lblHeadquarterName.text=vm.selectedHeadquarter!.nombreSede
        lblStreet.text=vm.selectedHeadquarter!.direccion
        lblzone.text=vm.selectedHeadquarter!.zona
        lblPhone.text=vm.selectedFamiliar!.telcontacto
    }
    
    @IBAction func tapAutorizate(_ sender: Any) {
        let msj="¿Estás seguro que deseas generar la autorización?"
        self.confirm(msj: msj) {
            self.showLoading()
            self.vm.sendForm(susses: {
                self.hiddenLoading()
                let textAlert="Has generado exitosamente tu autorización.<br><br> Recuerda que esta autorización está sujeta a la verificación de la orden médica por la IPS para la prestación del servicio."
                self.alert( msj: textAlert, callback: {
                    self.navigationController?.popViewController(animated: true)
                })
            }) { (error) in
                self.hiddenLoading()
                JACSAutorizationsAlertViewController.open(navController: self.navigationController!, callback: {
                    self.navigationController?.popViewController(animated: true)
                })
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
