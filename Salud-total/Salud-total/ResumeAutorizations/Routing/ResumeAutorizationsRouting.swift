//
//  ResumeAutorizationsRouting.swift
//  Salud-total
//
//  Created by iMac on 12/30/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit
extension ResumeAutorizationsViewController{
   
    static func instantiate() -> ResumeAutorizationsViewController {
        let storyboard = UIStoryboard(name: "ResumeAutorizations", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ResumeAutorizationsViewController
        return vc
    }
    
    

}
