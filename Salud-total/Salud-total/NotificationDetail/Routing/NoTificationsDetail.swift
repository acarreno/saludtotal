//
//  NoTificationsDetail.swift
//  Salud-total
//
//  Created by Andrey Carreño on 11/15/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit
extension NotificationsDetailViewController{
    
    static func instantiate() -> NotificationsDetailViewController {
        
        let storyboard = UIStoryboard(name: "NotificationsDetail", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! NotificationsDetailViewController
        return vc
    }
}
