//
//  DirectoryRoute.swift
//  Salud-total
//
//  Created by iMac on 12/17/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

extension DirectoryViewController{
    static func instantiate() -> DirectoryViewController {
        let storyboard = UIStoryboard(name: "Directory", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! DirectoryViewController
        return vc
    }
    func openFilter(){
        let vc = DirectoryFilterViewController.instantiate()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
