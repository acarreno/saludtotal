//
//  DirectoryManager.swift
//  Salud-total
//
//  Created by iMac on 12/17/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
class DirectoryManager:UIViewController  {
    func getTokenDirectory(susses:@escaping ()->(),failed:@escaping (String)->()){
        loginApp(){(finish,res)in
            if finish{
                let url = "\(URlGet().tokenDirectorio)/?Token=\(res)&Origen=APP"
                self.servGet(url:url , callback: { (finish, res) in
                    if (res is [String:Any]){
                        let response=GetTokenModel(JSON: (res as! [String:Any]))!
                        
                        if finish{
                            UserDefaults.standard.set("Bearer \(response.token)", forKey: "tokenAutorization")
                             susses()
                           
                        }else{
                            failed(response.Error)
                           
                        }
                    }
                })
            }else{
                 failed(res)
            }
           
            
          
            
        }
        
    }
    func getDirectory(susses:@escaping ([DirectoryModel])->(),failed:@escaping (String)->()) {
        let url=URlGet().Directorio
        getTokenDirectory(susses: { () in
            
            self.servGet(url:url, isHeaders: true) { (finish, res) in
                if finish{
                    let reponse=res as! [[String:Any]]
                    let listDirectory=Mapper<DirectoryModel>().mapArray(JSONObject: (reponse) )!
                    susses(listDirectory)
                }else{
                    failed(res as! String)
                }
            }
            
        }) { (res) in
            failed(res)
        }
        
    }
}
