//
//  DirectoryViewController.swift
//  Salud-total
//
//  Created by iMac on 12/17/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class DirectoryViewController: UIViewController {

    @IBOutlet weak var cv: UIView!
    @IBOutlet weak var contentTab: UIView!
    
    var vm=DirectoryViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addHeader()
        self.showLoading()
        self.hiddenLoading()
        let vc=DirectoryMapViewController.instantiate()
        vc.vm.container=self.vm.listDirectories
        self.changeView(controller:vc)
        //let vc=AffiliateBasicDataViewController.instantiate()
        
        
        
        //vc.vm.beneficiary=vm.beneficiary
                // Do any additional setup after loading the view.
    }
    
    @IBAction func tapMap(_ sender: UIButton) {
        changeActive(activeButton:sender)
        let vc=DirectoryMapViewController.instantiate()
        vc.vm.container=vm.listDirectories
        //vc.vm.beneficiary=vm.beneficiary
        changeView(controller:vc)
        
    }
    
    @IBAction func tapList(_ sender: UIButton) {
        changeActive(activeButton:sender)
        let vc=DirectoryListViewController.instantiate()
        vc.vm.container=vm.listDirectories
        changeView(controller:vc)
    }
    @IBAction func tapBasics(_ sender: UIButton) {
        changeActive(activeButton:sender)
        //let vc=AffiliateBasicDataViewController.instantiate()
        //vc.vm.beneficiary=vm.beneficiary
        //changeView(controller:vc)
    }
    
    func changeActive(activeButton:UIButton){
        for item in contentTab.subviews {
            if item is UIButton{
                let button=item as! UIButton
                button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
                button.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            }
        }
        activeButton.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        activeButton.setTitleColor(#colorLiteral(red: 0.2500286698, green: 0.3332767487, blue: 0.618616581, alpha: 1), for: .normal)
    }
    
    func changeView(controller:UIViewController){
        addChild(controller)
        
        controller.view.frame=cv.bounds
        cv.addSubview(controller.view)
        
    }
    @IBAction func tabOpenFilter(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
