//
//  DirectoryModel.swift
//  Salud-total
//
//  Created by iMac on 12/18/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper

class DirectoryModel : Mappable {
   
    var ciudad = 20238
    var direccion = "CL 8 6-47"
    var horario = ""
    var latitud = ""
    var longitud = ""
    var nombre = ""
    var nombreCiudad = ""
    var servicio = "";
    var telefono = 5255005;

    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        ciudad <- map["ciudad"]
        direccion <- map["direccion"]
        horario <- map["horario"]
        latitud <- map["latitud"]
        longitud <- map["longitud"]
        nombre <- map["nombre"]
        nombreCiudad <- map["nombreCiudad"]
        servicio <- map["servicio"]
        telefono <- map["telefono"]
    }
    
    
}
