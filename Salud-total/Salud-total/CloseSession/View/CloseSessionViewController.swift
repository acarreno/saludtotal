//
//  CloseSessionViewController.swift
//  Salud-total
//
//  Created by Andrey Carreño on 11/18/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class CloseSessionViewController: UIViewController {

    var susses: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func tapAccept(_ sender: Any) {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
            
        }
        self.dismiss(animated: true){
            self.susses!()
        }
    }
    
    @IBAction func tapCancel(_ sender: Any) {
        self.dismiss(animated: true,completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
