//
//  BeneficiaryModel.swift
//  Salud-total
//
//  Created by Andrey Carreño on 11/19/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper
class BeneficiaryModel: Mappable {
    
    var BeneficiarioConsecutivo = ""
    var BeneficiarioId = ""
    var BeneficiarioTipoDescripcion = ""
    var BeneficiarioTipoId = ""
    var Celular=""
    var CiudadId = ""
    var CorreoElectronico = ""
    var Contrato = ""
    var CotizanteId = ""
    var CotizanteTipoId = ""
    var Direccion = ""
    var DocumentosFaltantes = ""
    var Edad = ""
    var Genero = ""
    var EsCotizanteSiNo = 0;
    var EstadoDetallado = ""
    var EstadoGeneral = ""
    var EstadoServicio = ""
    var FechaNacimiento = ""
    var IdBeneficiario = 0
    var Nombres = ""
    var Parentesco = ""
    var Telefono = 0
    var TieneContratoVigente = false
    var TipoBeneficiarioId = ""
    var TipoDocumento = ""
    var UPC = ""
    var ipsmedicaid = ""
    var ipsodontologicaid = ""
    var Apellidos=""
    var NombreDepto=""
    var NombreMunicipio=""
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        BeneficiarioConsecutivo <- map["BeneficiarioConsecutivo"]
        BeneficiarioId <- map["BeneficiarioId"]
        BeneficiarioTipoDescripcion <- map["BeneficiarioTipoDescripcion"]
        BeneficiarioTipoId <- map["BeneficiarioTipoId"]
        CiudadId <- map["CiudadId"]
        Contrato <- map["Contrato"]
        CotizanteId <- map["CotizanteId"]
        CotizanteTipoId <- map["CotizanteTipoId"]
        Direccion <- map["Direccion"]
        DocumentosFaltantes <- map["DocumentosFaltantes"]
        Edad <- map["Edad"]
        EsCotizanteSiNo <- map["EsCotizanteSiNo"]
        EstadoDetallado <- map["EstadoDetallado"]
        EstadoGeneral <- map["EstadoGeneral"]
        EstadoServicio <- map["EstadoServicio"]
        IdBeneficiario <- map["IdBeneficiario"]
        Nombres <- map["Nombres"]
        Apellidos <- map["Apellidos"]
        Parentesco <- map["Parentesco"]
        Telefono <- map["Telefono"]
        TieneContratoVigente <- map["TieneContratoVigente"]
        TipoBeneficiarioId <- map["TipoBeneficiarioId"]
        TipoDocumento <- map["TipoDocumento"]
        UPC <- map["UPC"]
        ipsmedicaid <- map["ipsmedicaid"]
        ipsodontologicaid <- map["ipsodontologicaid"]
        FechaNacimiento <- map["FechaNacimiento"]
        Celular <- map["Celular"]
        CorreoElectronico <- map["CorreoElectronico"]
        Genero <- map["Genero"]
        NombreDepto <- map["NombreDepto"]
        NombreMunicipio <- map["NombreMunicipio"]
    }
}
