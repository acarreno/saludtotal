//
//  CloseSessionRouting.swift
//  Salud-total
//
//  Created by Andrey Carreño on 11/18/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

extension CloseSessionViewController{
    static func open(navController:UINavigationController, callback:@escaping()->()) {
        let storyboard = UIStoryboard(name: "CloseSession", bundle: Bundle.main)
        
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! CloseSessionViewController
        vc.modalPresentationStyle = .overCurrentContext
        vc.susses={() -> Void in
            
            callback()
        }
        
        navController.present(vc, animated: true, completion: nil)
        
    }
}
