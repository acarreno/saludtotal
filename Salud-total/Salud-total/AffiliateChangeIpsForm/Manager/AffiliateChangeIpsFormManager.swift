//
//  AffiliateChangeIpsFormManager.swift
//  Salud-total
//
//  Created by iMac on 12/24/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class AffiliateChangeIpsFormManager:UIViewController {
    
    func getHealtIps(dep:String,city:String,susses:@escaping ([IPSModel])->(),failed:@escaping (String)->()){
        let url = "\(URlGet().GetIpsMedica)?coddepartamento=\(dep)&codmunicipio=\(city)"
        servGet(url:url, isHeaders: true) { (finish, res) in
            if finish{
                if res is [[String:Any]]{
                    let response = res as![[String:Any]]
                    let list=Mapper<IPSModel>().mapArray(JSONObject: (response) )!
                    susses(list)
                }else{
                    failed("Error interno")
                }
            }else{
                debugPrint(res)
            }
        }
    }
    func getOdontIps(idIpsMedica:String,susses:@escaping ([IPSModel])->(),failed:@escaping (String)->()){
        let url = "\(URlGet().GetIpsOdontologica)?idIpsMedica=\(idIpsMedica)"
        servGet(url:url, isHeaders: true) { (finish, res) in
            if finish{
                if res is [[String:Any]]{
                    let response = res as![[String:Any]]
                    let list=Mapper<IPSModel>().mapArray(JSONObject: (response) )!
                    susses(list)
                }else{
                    failed("Error interno")
                }
            }else{
                failed("Error interno")
            }
        }
    }
    func sendForm(request:ChangeIPSRequest,susses:@escaping (String)->(),failed:@escaping (String)->()){
        let url = "\(URlGet().ACtualizarIPS)?IpsMedAnterior=\(request.IpsMedAnterior)&IpsMedNueva=\(request.IpsMedNueva)&IpsOdonAnterior=\(request.IpsOdonAnterior)&IpsOdontNueva=\(request.IpsOdontNueva) NED&BeneficiarioId=\(request.BeneficiarioId)&BeneficiarioTipoId=\(request.BeneficiarioTipoId)&Direccion=\(request.Direccion)&telefonofijo=\(request.telefonofijo)&ciudad=\(request.departamento)\(request.ciudad)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        servGet(url: url!, isHeaders: true) { (finish, res) in
            
            if finish{
                let response=res as! [String:Any];
                if response["Descripcion"] != nil || !(response["Descripcion"] is NSNull){
                   susses(response["Descripcion"] as! String)
                }else{
                    failed(response["Error"] as! String)
                    
                }
                
            }else{
                
                failed(res as!String)
            }
        }
    }
    func getTowns(susses:@escaping ([TownModel])->(),failed:@escaping (String)->()){
        
        let url=URlGet().GetDepartamentos
        servGet(url: url, isHeaders: true) { (finish, res) in
            if finish{
                let list = res as! [[String:Any]]
                let listDep=Mapper<TownModel>().mapArray(JSONObject: (list) )!
                susses(listDep)
            }else{
                
                failed(res as! String)
            }
            
        }
    }
    func getCities(townId:String,susses:@escaping ([CityGenericModel])->(),failed:@escaping (String)->()){
        
        let url="\(URlGet().Getciudades)?coddepartamento=\(townId)"
        servGet(url: url, isHeaders: true) { (finish, res) in
            if finish{
                let list = res as! [[String:Any]]
                let listDep=Mapper<CityGenericModel>().mapArray(JSONObject: (list) )!
                susses(listDep)
            }else{
                
                failed(res as! String)
            }
            
        }
    }
}
