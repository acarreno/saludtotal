//
//  AffiliateChangeIpsFormRoutin.swift
//  Salud-total
//
//  Created by iMac on 12/24/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

extension AffiliateChangeIpsFormViewController{
    static func instantiate() -> AffiliateChangeIpsFormViewController {
        let storyboard = UIStoryboard(name: "AffiliateChangeIpsForm", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! AffiliateChangeIpsFormViewController
        return vc
    }
    func openPicker(list:[String],callback:@escaping (_ index:Int)->()){
        JACSSelectViewController.open(navController: self.navigationController!, list: list) { (index) in
            callback(index)
        }
    }
}
