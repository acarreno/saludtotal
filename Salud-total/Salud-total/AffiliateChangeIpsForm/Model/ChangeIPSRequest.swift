//
//  ChangeIPSRequest.swift
//  Salud-total
//
//  Created by iMac on 12/26/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper

class ChangeIPSRequest : Mappable {
   
    var IpsMedAnterior=""
    var IpsMedNueva=""
    var IpsOdonAnterior=""
    var IpsOdontNueva=""
    var BeneficiarioTipoId=""
    var BeneficiarioId=""
    var Direccion=""
    var telefonofijo=""
    var ciudad=""
    var departamento=""
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        IpsMedAnterior <- map["IpsMedAnterior"]
        IpsMedNueva <- map["IpsMedNueva"]
        IpsOdonAnterior <- map["IpsOdonAnterior"]
        IpsOdontNueva <- map["IpsOdontNueva"]
        BeneficiarioTipoId <- map["BeneficiarioTipoId"]
        Direccion <- map["Direccion"]
        telefonofijo <- map["telefonofijo"]
        ciudad <- map["ciudad"]
        BeneficiarioId <- map["BeneficiarioId"]
        departamento <- map["departamento"]
    }
    
    
}
