//
//  IPSModel.swift
//  Salud-total
//
//  Created by iMac on 12/26/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper

class IPSModel : Mappable {
    
    var Nombre=""
    var direccion=""
    var ipsid=""
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        Nombre <- map["Nombre"]
        direccion <- map["direccion"]
        ipsid <- map["ipsid"]
    }
    
    
}
