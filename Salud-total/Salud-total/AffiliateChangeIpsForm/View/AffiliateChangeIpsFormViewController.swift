//
//  EditInformationAffiliateViewController.swift
//  Salud-total
//
//  Created by iMac on 12/12/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class AffiliateChangeIpsFormViewController: UIViewController {
    
    @IBOutlet weak var table: UITableView!
    
    var vm=AffiliateChangeIpsFormViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideBack()
        table.separatorStyle = .none
        //let cities = getCitiesDefaults()
        //vm.separateCities(list: cities)
        //vm.separateDeps(list: cities)
        //vm.inflateOptionsStreet()
        self.vm.inflateTable()
        vm.initViewModel(susses: {
            
            self.table.reloadData()
        }) { (error) in
            self.hiddenLoading()
            self.alert(msj: error, callback: {
                self.navigationController?.popViewController(animated: true)
            })
        }
        // Do any additional setup after loading the view.
    }
    @objc func  selectDepar(_ sender:UIButton){
        openPicker(list: vm.retTowns()) { (index) in
            //self.vm.depSelect=self.vm.listParcDep[index]
            self.vm.selectedTown=self.vm.listTowns[index]
            self.vm.container[3].value=""
            self.vm.container[9].value=""
            self.vm.container[10].value=""
            self.vm.container[sender.tag].value=self.vm.listTowns[index].Departamento
            
            let listIndespath = [
                IndexPath(row: sender.tag, section: 0),
                IndexPath(row: 3, section: 0),
                IndexPath(row: 9, section: 0),
                IndexPath(row: 10, section: 0),
            ]
            
            self.table.reloadRows(at: listIndespath, with: .none)
            self.showLoading()
            self.vm.getCities(susses: {
                self.hiddenLoading()
            }, failed: { (res) in
                self.hiddenLoading()
                self.alert(msj: res)
            })
            
        }
    }
    @objc func selectCity(_ sender:UIButton){
        openPicker(list: vm.retCities()) { (index) in
            //self.vm.citySelect=self.vm.listParcCity[index]
            self.vm.container[sender.tag].value=self.vm.listCity[index].NombreMostrar
            self.vm.citySelect=self.vm.listCity[index]
            self.table.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .none)
            self.showLoading()
            self.vm.getHealtIps(susses: {
                self.hiddenLoading()
            }, failed: { (error) in
                self.hiddenLoading()
                self.alert( msj: error, callback: {
                    self.navigationController?.popViewController(animated: true)
                })
            })
        }
    }
   
    @objc func selectHealtIps(_ sender:UIButton){
        let list=vm.retHealtOptions()
        openPicker(list:list ) { (index) in
            
            self.vm.container[sender.tag].value=self.vm.healtOptions[index].Nombre
            self.vm.newHeakIps=self.vm.healtOptions[index]
            self.table.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .none)
            
            self.showLoading()
            self.vm.getOdontIps(susses: {
                self.hiddenLoading()
            }, failed: { (error) in
                self.alert( msj: error, callback: {
                    print("error in odontology ")
                })
            })
        }
    }
    
    @objc func selectOdontIps(_ sender:UIButton){
        
        openPicker(list: vm.retOdontoOptions()) { (index) in
            
            self.vm.container[sender.tag].value=self.vm.healtOptions[index].Nombre
            self.vm.newOdontIps=self.vm.odontOptions[index]
            self.table.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .none)
        }
    }
    
    @objc func tapSend(_ sender:UIButton){
        let validObj=vm.validateForm()
        self.view.endEditing(true)
        if validObj.error{
            alert( msj: validObj.alert) {
                print("error ")
            }
        }else{
            showLoading()
            vm.sendFrm(susses: { (res) in
                self.hiddenLoading()
                self.alert( msj: res, callback: {
                    self.navigationController?.popViewController(animated: true)
                })
            }) { (error) in
                self.hiddenLoading()
                self.alert(msj: error, callback: {
                    self.navigationController?.popViewController(animated: true)
                })
            }
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension AffiliateChangeIpsFormViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item=vm.container[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: item.cell, for: indexPath) as! AffiliateChangeIpsFormTableViewCell
        switch item.cell {
        case vm.cells.section:
            
            cell.sectionTitle.text=item.label
            
        case vm.cells.select:
            
            cell.selectButton.tag=indexPath.row
            cell.lblPlaceHolder.text=item.placeholder
            cell.icon.image=UIImage(named:item.icon)
            let html="<p style='font-family:\"Futura\";font-size:14px'>\(item.label.uppercased()) <span style='color:rgb(45,79,140)'>*</span></p>".htmlToAttributedString
            cell.lblNaMeItem.attributedText=html
            cell.lblPlaceHolder.textColor=UIColor(red:186/255,green:186/255,blue:186/255,alpha:1)
            cell.selectButton.removeTarget(nil, action: nil, for: .allEvents)
            
            switch item.id{
            case vm.ids.department:
                cell.lblPlaceHolder.text=item.placeholder
                cell.selectButton.tag=indexPath.row
                cell.selectButton.addTarget(self, action: #selector(selectDepar(_:)), for: .touchDown)
                if item.value != ""{
                    cell.lblPlaceHolder.text=item.value
                    cell.lblPlaceHolder.textColor=UIColor(red:0,green:0,blue:0,alpha:1)
                }
            case vm.ids.town:
                cell.lblPlaceHolder.text=item.placeholder
                cell.selectButton.addTarget(self, action: #selector(selectCity(_:)), for: .touchDown)
                if item.value != ""{
                    cell.lblPlaceHolder.text=item.value
                    cell.lblPlaceHolder.textColor=UIColor(red:0,green:0,blue:0,alpha:1)
                }
            case vm.ids.street:
                cell.lblPlaceHolder.text=item.placeholder
              
                if item.value != ""{
                    cell.lblPlaceHolder.text=item.value
                    cell.lblPlaceHolder.textColor=UIColor(red:0,green:0,blue:0,alpha:1)
                }
            case vm.ids.healtIps:
                cell.lblPlaceHolder.text=item.placeholder
                cell.selectButton.addTarget(self, action: #selector(selectHealtIps(_:)), for: .touchDown)
                if item.value != ""{
                    cell.lblPlaceHolder.text=item.value
                    cell.lblPlaceHolder.textColor=UIColor(red:0,green:0,blue:0,alpha:1)
                }
            case vm.ids.odontologyIps:
                cell.lblPlaceHolder.text=item.placeholder
                cell.selectButton.addTarget(self, action: #selector(selectOdontIps(_:)), for: .touchDown)
                if item.value != ""{
                    cell.lblPlaceHolder.text=item.value
                    cell.lblPlaceHolder.textColor=UIColor(red:0,green:0,blue:0,alpha:1)
                }
            default:
                break
            }
            
            //cell.selectButton.addTarget(self, action: #selector(tapSelect(_:)), for: .touchDown)
            
            
        case vm.cells.simpleSelect:
            
            cell.lblPlaceHolder.textColor=UIColor(red:186/255,green:186/255,blue:186/255,alpha:1)
            cell.lblPlaceHolder.text=item.placeholder
            cell.selectButton.tag=indexPath.row
            switch item.id{
            case vm.ids.street:
                cell.lblPlaceHolder.text=item.placeholder
                //cell.selectButton.addTarget(self, action: #selector(selectDir(_:)), for: .touchDown)
                if item.value != ""{
                    cell.lblPlaceHolder.text=item.value
                    cell.lblPlaceHolder.textColor=UIColor(red:0,green:0,blue:0,alpha:1)
                }
            default:
                break
            }
            
            
        case vm.cells.textfield:
            cell.icon.image=UIImage(named:item.icon)
            cell.txtItem.text=""
            cell.txtItem.placeholder=item.placeholder
            cell.txtItem.tag=indexPath.row
            cell.txtItem.delegate=self
            cell.txtItem.keyboardType = item.type!
            let html="<p style='font-family:\"Futura\";font-size:14px'>\(item.label.uppercased()) <span style='color:rgb(45,79,140)'>*</span></p>".htmlToAttributedString
            
            cell.lblNaMeItem.attributedText=html
            if item.value != ""{
                cell.txtItem.text=item.value
            }
            
        case vm.cells.button:
            cell.btnSend.addTarget(self, action: #selector(tapSend(_:)), for: .touchDown)
            break
        case vm.cells.dir:
            cell.txt1.tag=666
            cell.txt1.delegate=self
            cell.txt2.tag=777
            cell.txt2.delegate=self
            cell.txt3.tag=888
            cell.txt3.delegate=self
        default:
            break
        }
        
        
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm.container.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(vm.container[indexPath.row].height)
    }
    
}
extension AffiliateChangeIpsFormViewController: UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = UIColor.black
            
        }
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        vm.container[textView.tag].value=textView.text!
    }
}
extension AffiliateChangeIpsFormViewController: UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        vm.container[textField.tag].value=textField.text!
        
        
    }
    
}
