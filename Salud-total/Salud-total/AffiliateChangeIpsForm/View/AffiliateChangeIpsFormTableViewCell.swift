//
//  AffiliateChangeIpsFormTableViewCell.swift
//  Salud-total
//
//  Created by iMac on 12/24/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class AffiliateChangeIpsFormTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblNaMeItem: UILabel!
    
    @IBOutlet weak var lblPlaceHolder: UILabel!
    
    @IBOutlet weak var txtItem: UITextField!
    
    @IBOutlet weak var sectionTitle: UILabel!
    
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var txt1: UITextField!
    @IBOutlet weak var txt2: UITextField!
    @IBOutlet weak var txt3: UITextField!
    @IBOutlet weak var btnSend: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
