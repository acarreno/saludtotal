//
//  EditInformationAffiliateViewModel.swift
//  Salud-total
//
//  Created by iMac on 12/12/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//


import Foundation
import UIKit
class AffiliateChangeIpsFormViewModel:UIViewController  {
    
    struct Cells {
        let select="select"
        let textfield="textfield"
        let button="button"
        let separator="separator"
        let dir="dir"
        let section="section"
        let simpleSelect="simpleSelect"
    }
    
    struct Ids {
        let department="department"
        let town="town"
        let street="street"
        let numbreStreet="numbreStreet"
        let appartMent="appartMent"
        let telephone="telephone"
        let mobile="mobile"
        let email="email"
        let healtIps="healtIps"
        let odontologyIps="odontologyIps"
        
    }
    struct Sizes {
        let select=98
        let textfield=108
        let separator=20
        let dir=78
        let button=68
        let section=44
        let simpleSelect=60
    }
    
    typealias CellModel =  (cell:String,id:String,value:String,alert:String,placeholder:String,type:UIKeyboardType?,icon:String,label:String,height:Int,obligatory:Bool)
    
    var container=[CellModel]()
    let cells=Cells()
    let ids=Ids()
    let sizes=Sizes()
    let manager=AffiliateChangeIpsFormManager()
    var request=ChangeIPSRequest(JSON: ["":""])
    var healtOptions=[IPSModel]()
    var odontOptions=[IPSModel]()
    
    var newHeakIps=IPSModel(JSON:["":""])!
    var newOdontIps=IPSModel(JSON:["":""])!
    var defHeakIps=IPSModel(JSON:["":""])!
    var defOdontIps=IPSModel(JSON:["":""])!
    var beneficiary=BeneficiaryModel(JSON: ["":""])!
    var defCity=City(JSON: ["":""])!
    var defTown=TownModel(JSON: ["":""])!
    var listTowns=[TownModel]()
    var selectedTown=TownModel(JSON: ["":""])!
    var listCity=[CityGenericModel]()
    var citySelect=CityGenericModel(JSON: ["":""])!
    
    func initViewModel(susses:@escaping ()->(),failed:@escaping (String)->()){
        defCity = findCityBy(by: .CiudadId, word:  beneficiary.CiudadId)
        
        
        /*getHealtIps(susses: {
            
            //self.selectedHeakIps = self.findIps(word: self.beneficiary.ipsmedicaid)
            //self.defHeakIps = self.selectedHeakIps
            
            self.getOdontIps(susses: {
                
                //self.selectedOdontIps=self.findOdontoIps(word: self.beneficiary.ipsodontologicaid)
                //self.defOdontIps=self.findOdontoIps(word: self.beneficiary.ipsodontologicaid)
                
                self.inflateTable()
                susses()
                
            }, failed: { (error) in
                failed(error)
            })
        }) { (error) in
            
        }*/
    }
    
    func findIps(word:String)->IPSModel{
        var concurrency=IPSModel(JSON:["":""])!
        
        
        for item in healtOptions {
            print(item.ipsid)
            if item.ipsid == word{
                concurrency=item
                return concurrency
            }
        }
        
        return concurrency
    }
    func findOdontoIps(word:String)->IPSModel{
        var concurrency=IPSModel(JSON:["":""])!
        
        
        for item in odontOptions {
            if item.ipsid == word{
                concurrency=item
                return concurrency
            }
        }
        
        return concurrency
    }
    
    func inflateTable(){
        container.append((
            cell:cells.section,
            id:"",
            value:"",
            alert:"",
            placeholder:"",
            type:UIKeyboardType.alphabet,
            icon:"",
            label:"DATOS DE RESIDENCIA",
            height:sizes.section,
            obligatory:false
        ))
        container.append((
            cell:cells.separator,
            id:"",
            value:"",
            alert:"",
            placeholder:"",
            type:UIKeyboardType.alphabet,
            icon:"",
            label:"",
            height:sizes.separator,
            obligatory:false
        ))
        container.append((
            cell:cells.select,
            id:ids.department,
            value:beneficiary.NombreDepto,
            alert:"Selecciona el departamento",
            placeholder:"Selecciona el departamento",
            type:UIKeyboardType.alphabet,
            icon:"stGeneralUbicacion",
            label:"Departamento",
            height:sizes.select,
            obligatory:true
        ))
        container.append((
            cell:cells.select,
            id:ids.town,
            value:beneficiary.NombreMunicipio,
            alert:"Selecciona el municipio",
            placeholder:"Selecciona el municipio",
            type:UIKeyboardType.alphabet,
            icon:"stGeneralUbicacion",
            label:"Municipio / Ciudad",
            height:sizes.select,
            obligatory:true
        ))
        container.append((
            cell:cells.section,
            id:"",
            value:"",
            alert:"",
            placeholder:"",
            type:UIKeyboardType.alphabet,
            icon:"",
            label:"DIRECCIÓN DE RESIDENCIA",
            height:sizes.section,
            obligatory:true
        ))
        container.append((
            cell:cells.textfield,
            id:ids.street,
            value:beneficiary.Direccion,
            alert:"",
            placeholder:"Ej: Calle 14",
            type:UIKeyboardType.alphabet,
            icon:"stGeneralUbicacion",
            label:"Dirección",
            height:sizes.textfield,
            obligatory:true
        ))
        
        
        container.append((
            cell:cells.separator,
            id:"",
            value:"",
            alert:"",
            placeholder:"",
            type:UIKeyboardType.alphabet,
            icon:"",
            label:"",
            height:sizes.separator,
            obligatory:true
        ))
        container.append((
            cell:cells.section,
            id:"",
            value:"",
            alert:"",
            placeholder:"",
            type:UIKeyboardType.alphabet,
            icon:"",
            label:"DATOS ADICIONALES",
            height:sizes.section,
            obligatory:true
        ))
        
        container.append((
            cell:cells.textfield,
            id:ids.telephone,
            value:"\(beneficiary.Telefono)",
            alert:"Ingresa el número de teléfono",
            placeholder:"Ingresa el número de teléfono",
            type:UIKeyboardType.numberPad,
            icon:"stGeneralTelefono",
            label:"Teléfono fijo",
            height:sizes.textfield,
            obligatory:true
        ))
        container.append((
            cell:cells.select,
            id:ids.healtIps,
            value:defHeakIps.Nombre,
            alert:"Selecciona la IPS Médica",
            placeholder:"Selecciona la IPS Médica",
            type:UIKeyboardType.numberPad,
            icon:"stGeneralUbicacion",
            label:"IPS Médica",
            height:sizes.select,
            obligatory:true
        ))
        container.append((
            cell:cells.select,
            id:ids.odontologyIps,
            value:defOdontIps.Nombre,
            alert:"Selecciona la IPS Odontología",
            placeholder:"Selecciona la IPS Odontología",
            type:UIKeyboardType.numberPad,
            icon:"stGeneralUbicacion",
            label:"IPS Odontología",
            height:sizes.textfield,
            obligatory:true
        ))
        container.append((
            cell:cells.button,
            id:"",
            value:"",
            alert:"",
            placeholder:"",
            type:UIKeyboardType.alphabet,
            icon:"",
            label:"",
            height:sizes.button,
            obligatory:true
        ))
    }
    
    
    func sendFrm(susses:@escaping (String)->(),failed:@escaping (String)->()){
        
        let sessInfo=retUser()
        
        request?.BeneficiarioId=sessInfo.user.DocumentoId
        request?.BeneficiarioTipoId=sessInfo.user.codeDocument
        request?.IpsOdonAnterior=defOdontIps.ipsid
        request?.IpsMedAnterior=defOdontIps.ipsid
        request?.departamento=defTown.CodDepartamento
        manager.sendForm(request:request!,susses: { (res) in
            susses(res)
        }) { (res) in
            failed(res)
        }
    }
    func validateForm()->(error:Bool,alert:String){
        
        for item in container {
            
            if item.cell==cells.simpleSelect || item.cell==cells.select || item.cell == cells.textfield{
                if item.value == "" {
                    return (error:true,alert:item.alert)
                }
                switch item.id{
                case ids.street:
                    request?.Direccion="\(item.value)"
                //request?.direccioncotizante=item.value
                case ids.telephone:
                    request?.telefonofijo=item.value
              
                case ids.town:
                    request?.ciudad="\(defCity.CiudadId)"
                    
                case ids.healtIps:
                    request?.IpsMedNueva=newHeakIps.ipsid
                
                case ids.odontologyIps:
                    request?.IpsOdontNueva=newOdontIps.ipsid
                    break;
                default:
                    break
                }
            }
        }
        return (error:false,alert:"")
    }
    
    func retHealtOptions()->[String]{
        var list=[String]()
        for item in healtOptions {
            list.append(item.Nombre)
        }
        return list
    }
    
    func retOdontoOptions()->[String]{
        var list=[String]()
        for item in odontOptions {
            list.append(item.Nombre)
        }
        return list
    }
    func getHealtIps(susses:@escaping ()->(),failed:@escaping (String)->()){
       
        let codDep = selectedTown.CodDepartamento
        let codCity = citySelect.CodMunicipio
        manager.getHealtIps(dep: codDep, city: String(describing:codCity), susses: { (res) in
            
            self.healtOptions=res
            susses()
            print(res)
        }) { (error) in
            failed(error)
            print("error")
        }
    }
    
    func getOdontIps(susses:@escaping ()->(),failed:@escaping (String)->()){
        //let id=selectedHeakIps.ipsid
        let id=newHeakIps.ipsid
        manager.getOdontIps(idIpsMedica:id, susses: { (res) in
            
            self.odontOptions=res
            susses()
            print(res)
        }) { (error) in
            failed(error)
            print("error")
        }
        
       
    }
    func retTowns()->[String]{
        var list=[String]()
        for item in listTowns {
            list.append(item.Departamento)
        }
        return list
    }
    
    func retCities()->[String]{
        var list=[String]()
        for item in listCity {
            list.append(item.NombreMostrar)
        }
        return list
    }
    func getCities(susses:@escaping ()->(),failed:@escaping (String)->()){
        let townId=selectedTown.CodDepartamento
        manager.getCities(townId: townId, susses: { (res) in
            self.listCity=res
            susses()
        }) { (error) in
            failed(error)
        }
    }
}
