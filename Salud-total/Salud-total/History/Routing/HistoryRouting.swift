//
//  HistoryRouting.swift
//  Salud-total
//
//  Created by Andrey Carreño on 11/15/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//


import Foundation
import UIKit
extension HistoryViewController{
    
    static func instantiate() -> HistoryViewController {
        
        let storyboard = UIStoryboard(name: "History", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HistoryViewController
        return vc
        
    }
    func openPop(){
        HistoryPopUpViewController.open(navController: self.navigationController!) { (res) in
            print(res)
        }
    }
}
