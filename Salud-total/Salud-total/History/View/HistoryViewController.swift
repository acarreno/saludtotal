//
//  HistoryViewController.swift
//  Salud-total
//
//  Created by Andrey Carreño on 11/15/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController {
    
    @IBOutlet weak var table: UITableView!
    
    let vm = HistoryViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vm.inflateTable()
        addHeader()
        table.separatorStyle = .none
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension HistoryViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item=vm.container[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: item.cell, for: indexPath) as! HistoryTableViewCell
        cell.contentView.addLine(position: .LINE_POSITION_BOTTOM, color: UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.09), width:cell.contentView.frame.width-30 )
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm.container.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        openPop()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item=vm.container[indexPath.row]
        return CGFloat(item.height)
    }
}
