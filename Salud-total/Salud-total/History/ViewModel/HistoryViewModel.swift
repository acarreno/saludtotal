//
//  HistoryViewModel.swift
//  Salud-total
//
//  Created by Andrey Carreño on 11/15/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation

class HistoryViewModel {
    struct Cells {
        let details = "details"
        let item = "item"
    }
    struct Sizes {
        let details = 109
        let item = 114
    }
    
    typealias CellModel =  (cell:String,height:Int,obj:Any?)
    
    let cells=Cells()
    let sizes=Sizes()
    var container=[CellModel]()
    
    
    func inflateTable(){
        container.append(
            (
                cell:cells.details,
                height:sizes.item,
                obj:nil
            )
        )
        container.append(
            (
                cell:cells.item,
                height:sizes.item,
                obj:nil
            )
        )
    }
}
