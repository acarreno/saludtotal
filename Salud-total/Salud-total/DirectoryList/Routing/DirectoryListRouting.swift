//
//  DirectoryListRouting.swift
//  Salud-total
//
//  Created by iMac on 12/17/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

extension DirectoryListViewController{
    static func instantiate() -> DirectoryListViewController {
        let storyboard = UIStoryboard(name: "DirectoryList", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! DirectoryListViewController
        return vc
    }
    
}
