//
//  DirectoryListViewController.swift
//  Salud-total
//
//  Created by iMac on 12/17/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class DirectoryListViewController: UIViewController {

    @IBOutlet weak var table: UITableView!
    
    let vm=DirectoryListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table.separatorStyle = .none
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DirectoryListViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item=vm.container[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DirectoryListTableViewCell
        cell.lblName.text=item.nombre
        cell.lblStreet.text=item.direccion
        cell.lblHorary.text="Horario: \n \(item.horario)"
        cell.contentView.addLine(position: .LINE_POSITION_BOTTOM, color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.09), width: table.frame.width-30)
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm.container.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 113
        
    }
    
}
