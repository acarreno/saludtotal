//
//  LaboratorioPopUpViewController.swift
//  Salud-total
//
//  Created by csanchez on 12/23/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class LaboratorioPopUpViewController: UIViewController {

    @IBOutlet weak var lblInstructions: UILabel!
    @IBOutlet weak var dates: UILabel!
    @IBOutlet weak var idResult: UILabel!
    @IBOutlet weak var nameResult: UILabel!
    @IBOutlet weak var sede: UILabel!
    
    var getData = DiagnosticoModel(JSON: [:])!
    
    var susses: ((_ id: String) -> Void)?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let html="<p style='text-align:center;font-family:\"Futura\";font-size:14px;color:rgb(128,128,128)'>Has seleccionado el siguiente resultado de laboratorio, puedes <span style='color:rgb( 0,159,227)'>enviarlo al correo electrónico registrado o descargarlo en tu dispositivo</span></p>"
        lblInstructions.attributedText=html.htmlToAttributedString
        dates.text = getData.FechaServicio
        idResult.text = getData.IdAyudaDx
        nameResult.text = getData.Servicio
        sede.text = getData.IpsAutorizada
        // Do any additional setup after loading the view.
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true) {
            self.susses?("cancel")
        }
    }
    
    @IBAction func tapSend(_ sender: Any) {
        self.dismiss(animated: true) {
            self.susses?("cancel")
        }
    }
    
    @IBAction func tapDownloand(_ sender: Any) {
        self.dismiss(animated: true) {
            self.susses?("send")
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
