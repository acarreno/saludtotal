//
//  LaboratoriosViewController.swift
//  Salud-total
//
//  Created by csanchez on 11/20/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class LaboratoriosViewController: MasterView {
    
    @IBOutlet weak var afiliadoNombre: UILabel!
    @IBOutlet weak var lblDateInit: UILabel!
    @IBOutlet weak var lblDateFinish: UILabel!
    @IBOutlet weak var btnConsultar: UIButton!
    
    let vm = LaboratoriosViewModel()
    var toolBar = UIToolbar()
    let datePicker = UIDatePicker()
    var typeDate = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideBack()
        
        initView()
        // Do any additional setup after loading the view.
    }
    
    func initView(){
        addHeader()
        afiliadoNombre.isHidden = true
        btnConsultar.isEnabled = false
        btnConsultar.backgroundColor = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
        
        vm.delegate = self
        vm.getAfiliados()
    }
    
    func loadData(){
        afiliadoNombre.isHidden = false
        afiliadoNombre.text = vm.userAfiliado.Nombres
    }
    
    func showDatePicker(){
        datePicker.datePickerMode = .date
        datePicker.backgroundColor = .white
        
        datePicker.autoresizingMask = .flexibleWidth
        datePicker.datePickerMode = .date
        
        datePicker.frame = CGRect(
            x: 0.0, y: view.frame.size.height - datePicker.frame.size.height,
            width: view.frame.size.width,
            height: datePicker.frame.size.height
        )
        
        toolBar = UIToolbar(
            frame: CGRect(x: 0, y: view.frame.size.height - datePicker.frame.size.height - 40,
            width: UIScreen.main.bounds.size.width,
            height: 50
        ))
        toolBar.barStyle = .default
        let doneButton = UIBarButtonItem(title: "Listo", style: .plain, target: self, action: #selector(donedatePicker));
        doneButton.tintColor=#colorLiteral(red: 0, green: 0.6841722131, blue: 0.8184788823, alpha: 1)
        
        toolBar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), doneButton]
        toolBar.sizeToFit()
        
        self.view.addSubview(toolBar)
        self.view.addSubview(datePicker)
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let getDate = formatter.string(from: datePicker.date)
        if typeDate == BtnDate.dInit {
            lblDateInit.text = getDate
        }else if typeDate == BtnDate.dFinist {
            lblDateFinish.text = getDate
        }
        
        if lblDateFinish.text != "Hasta" && lblDateInit.text != "Desde" {
            btnConsultar.isEnabled = true
            btnConsultar.backgroundColor = #colorLiteral(red: 0.1921568627, green: 0.2509803922, blue: 0.5490196078, alpha: 1)
        }
        
        closeDatePiker()
    }
    
    @IBAction func openDatePiker(_ btn: UIButton) {
        closeDatePiker()
        typeDate = btn.tag
        showDatePicker()
    }
    
    
    @IBAction func openAffiliates(_ sender: Any) {
        closeDatePiker()
        openPicker(list: vm.userAffiliates) { (index) in
            self.vm.userAfiliado = self.vm.listaAfiliados[index]
            self.afiliadoNombre.text = self.vm.userAfiliado.Nombres
        }
    }
    
    @IBAction func consultar(_ sender: Any) {
        
        let data = GetDataHomeLaboratorioModel(JSON: [
            "dateInit": lblDateInit.text!,
            "dateFinish": lblDateFinish.text!,
            "userAfiliado": ConsultaGrupoFamiliarModel(JSON: [:])!
        ])!
        data.userAfiliado = vm.userAfiliado
        goToRsultadosLaboratorio(data)
    }
    
    func closeDatePiker(){
        toolBar.removeFromSuperview()
        datePicker.removeFromSuperview()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LaboratoriosViewController: LaboratoriosViewModelProtocol {
    
    func loadDataInit() {
        loadData()
    }
    
    func subShowLoading() {
        showLoading()
    }
    
    func subHiddenLoading() {
        hiddenLoading()
    }
    
    func alert(_ text: String, popView: Bool) {
        
        alert( msj: text) {
            if popView {
                self.navigationController?.popViewController(animated: true)
            }
            
        }
        
    }
    
}
