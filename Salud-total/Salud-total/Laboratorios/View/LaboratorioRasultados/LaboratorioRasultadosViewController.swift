//
//  LaboratorioRasultadosViewController.swift
//  Salud-total
//
//  Created by csanchez on 12/19/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class LaboratorioRasultadosViewController: UIViewController {
    
    @IBOutlet weak var table: UITableView!
    
    let vm = LaboratorioRasultadosViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        hideBack()
        addHeader()
        
        vm.delegate = self
        vm.initContainer()
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LaboratorioRasultadosViewController: LaboratorioRasultadosViewModelProtocol {
    
    func subShowLoading() {
        showLoading()
    }
    
    func subHiddenLoading() {
        hiddenLoading()
    }
    
    func reload() {
        table.reloadData()
    }
    
    func alert(_ text: String, popView: Bool) {
        
        alert(msj: text) {
            if popView {
                self.navigationController?.popViewController(animated: true)
            }
        }
        
    }
    
}

extension LaboratorioRasultadosViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm.container.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = vm.container[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: item.cell) as! LaboratoriosTableViewCell
        
        if item.cell == CellIDLaboratoryRaesult.dataAffliate {
            cell.nameAffiliated.text = item.nameAfiliate
            cell.dates.text = item.dates
        }else if item.cell == CellIDLaboratoryRaesult.resultado {
            let dataResult = item.resulLaLaboratory
            cell.idResult.text = dataResult.IdAyudaDx
            cell.nameResult.text = dataResult.Servicio
            cell.dateServices.text = dataResult.FechaServicio
            cell.sede.text = dataResult.IpsAutorizada
        }
        
        // set cell's textLabel.text property
        // set cell's detailTextLabel.text property
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row != 0{
            self.vm.generateResult(indexPath.row)
        }
        
        /*let item = vm.container[indexPath.row]
        openPop(getData: item.resulLaLaboratory) { (res) in
            if res == "send" {
                
            }
        }*/
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = vm.container[indexPath.row]
        return CGFloat(item.alto)
    }
}
