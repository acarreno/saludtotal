//
//  CertificacionesTableViewCell.swift
//  Salud-total
//
//  Created by csanchez on 11/20/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class LaboratoriosTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameAffiliated: UILabel!
    @IBOutlet weak var dates: UILabel!
    @IBOutlet weak var idResult: UILabel!
    @IBOutlet weak var nameResult: UILabel!
    @IBOutlet weak var dateServices: UILabel!
    @IBOutlet weak var sede: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
