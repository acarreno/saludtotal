//
//  LaboratorioRasultadosViewModel.swift
//  Salud-total
//
//  Created by csanchez on 12/19/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

struct CellIDLaboratoryRaesult {
    static var dataAffliate = "CellDataAffliate"
    static var resultado = "CellResultado"
}

protocol LaboratorioRasultadosViewModelProtocol {
    func alert(_ text: String, popView: Bool)
    func subShowLoading()
    func subHiddenLoading()
    func reload()
}


class LaboratorioRasultadosViewModel: MasterViewModel {
    
    var delegate: LaboratorioRasultadosViewModelProtocol!
    var container = [LaboratoriosModel]()
    var data = GetDataHomeLaboratorioModel(JSON: [:])!
    var codeDoc = TypeDocumentModel()
    
    func initContainer(){
        delegate.subShowLoading()
        let dataCell = LaboratoriosModel(JSON: [
            "cell": CellIDLaboratoryRaesult.dataAffliate,
            "nameAfiliate": data.userAfiliado.Nombres,
            "dates": "\(data.dateInit) - \(data.dateFinish)",
            "alto": 105 
        ])
        
        codeDoc = retTypeDoc(searchBy: .name, search: data.userAfiliado.BeneficiarioTipoDescripcion)!
        container.append(dataCell!)
        
        DiagnosticoManager.ConsultaReporteDianostico(
            numeroDocumento: data.userAfiliado.BeneficiarioId,
            tipoDocumento: codeDoc.code,
            pInicioFechaLaboratorio: data.dateInit,
            pFinFechaLaboratorio: data.dateFinish
        ){ (response, finish, error) in
            self.delegate.subHiddenLoading()
            if finish {
                for item in response{
                    let dataCell = LaboratoriosModel(JSON: [
                        "cell": CellIDLaboratoryRaesult.resultado,
                        "alto": 149
                    ])!
                    dataCell.resulLaLaboratory = item
                    self.container.append(dataCell)
                }
            }else{
                self.delegate.alert(error, popView: false)
            }
            self.delegate.reload()
        }
    }
    
    func generateResult(_ result: Int){
        delegate.subShowLoading()
        let affiliate = container[result]
        if affiliate.cell == CellIDLaboratoryRaesult.resultado {
            let email = "testing@saludtotal.co"
            DiagnosticoManager.GenerarResultados(
                numeroDocumento: data.userAfiliado.BeneficiarioId,
                tipoDocumento: codeDoc.code,
                pIdResultado: affiliate.resulLaLaboratory.IdAyudaDx,
                pTipoResultado: affiliate.resulLaLaboratory.TipoResultado,
                pEmail: email
            ){ (response, finish, error) in
                self.delegate.subHiddenLoading()
                if finish {
                    let msj="el resultado de laboratorio fue enviada exitosamente al correo:<br><br> <span style='color:rgb(45,79,140)'>\(email)</span>"
                    self.delegate.alert(msj, popView: false)
                }else{
                    self.delegate.alert(error, popView: false)
                }
            }
        }
    }
    
}
