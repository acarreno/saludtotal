//
//  LaboratoriosViewModel.swift
//  Salud-total
//
//  Created by csanchez on 11/20/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

struct BtnDate {
    static let dInit = 1
    static let dFinist = 2
}

protocol LaboratoriosViewModelProtocol {
    func alert(_ text: String, popView: Bool)
    func subShowLoading()
    func subHiddenLoading()
    func loadDataInit()
}

class LaboratoriosViewModel: MasterViewModel {

    var delegate : LaboratoriosViewModelProtocol!
    let descripcionPantalla = "<i style='text-align:center;font-size:14px;color:rgb(128,128,128)'>Genera tus certificados de afiliación aquí. <span style='color:rgb( 0,159,227)'>Selecciona los pacientes</span> de los cuales necesitas la certificación. Esta te <span style='color:rgb( 0,159,227)'>será enviada al correo electrónico registrado.</span></i>"
    var listaAfiliados = [ConsultaGrupoFamiliarModel]()
    var userAfiliado = ConsultaGrupoFamiliarModel(JSON: [:])!
    var userAffiliates = [String]()
    
    
    func getAfiliados(){
        delegate.subShowLoading()
        
        AfiliadosManager.ConsultaGrupoFamiliar(BeneficiarioId: active_user!.DocumentoId, BeneficiarioTipoId: active_user!.codeDocument) { (response, finish, error) in
            self.delegate.subHiddenLoading()
            if finish {
                
                self.delegate.subHiddenLoading()
                for item in response {
                    self.userAffiliates.append(item.Nombres)
                    self.listaAfiliados.append(item)
                }
                
                if self.listaAfiliados.count > 0 {
                    self.userAfiliado = self.listaAfiliados[0]
                }
                
                self.delegate.loadDataInit()
            }else{
                self.delegate.alert(error, popView: false)
            }
        }
    }
    
}

