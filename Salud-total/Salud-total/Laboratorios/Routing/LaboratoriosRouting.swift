//
//  LaboratoriosRouting.swift
//  Salud-total
//
//  Created by csanchez on 11/20/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

extension LaboratoriosViewController {
    
    static func instantiate() -> LaboratoriosViewController {
        
        let storyboard = UIStoryboard(name: "Laboratorios", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! LaboratoriosViewController
        return vc
        
    }
    
    func openPicker(list:[String],callback:@escaping (_ index:Int)->()){
        JACSSelectViewController.open(navController: self.navigationController!, list: list) { (index) in
            callback(index)
        }
    }
    
    func goToRsultadosLaboratorio(_ data: GetDataHomeLaboratorioModel){
        let vc = LaboratorioRasultadosViewController.instantiate()
        vc.vm.data = data
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

