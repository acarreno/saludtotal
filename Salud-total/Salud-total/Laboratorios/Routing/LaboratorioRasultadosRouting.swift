//
//  LaboratorioRasultadosRouting.swift
//  Salud-total
//
//  Created by csanchez on 12/19/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

extension LaboratorioRasultadosViewController {
    
    static func instantiate() -> LaboratorioRasultadosViewController {
        
        let storyboard = UIStoryboard(name: "LaboratorioRasultados", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! LaboratorioRasultadosViewController
        return vc
        
    }
    
    func openPop(getData: DiagnosticoModel, completion: @escaping (_ res: String)->()){
        LaboratorioPopUpViewController.open(data: getData, navController: self.navigationController!) { (res) in
            completion(res)
        }
    }
    
}
