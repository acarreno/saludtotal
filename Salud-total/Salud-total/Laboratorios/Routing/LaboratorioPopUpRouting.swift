//
//  LaboratorioPopUpRouting.swift
//  Salud-total
//
//  Created by csanchez on 12/23/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

extension LaboratorioPopUpViewController {
    
    /*static func instantiate() -> HomeViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HomeViewController
        return vc
    }*/
    
    static func open(data: DiagnosticoModel,navController: UINavigationController, callback:@escaping(String)->()) {
        let storyboard = UIStoryboard(name: "LaboratorioPopUp", bundle: Bundle.main)
        
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! LaboratorioPopUpViewController
        vc.getData = data
        vc.modalPresentationStyle = .overCurrentContext
        vc.susses={(id:String) -> Void in
            callback(id)
        }
        
        navController.present(vc, animated: true, completion: nil)
        
    }
    
}
