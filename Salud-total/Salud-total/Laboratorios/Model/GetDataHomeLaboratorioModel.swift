//
//  GetDataHomeLaboratorioModel.swift
//  Salud-total
//
//  Created by csanchez on 12/19/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper

class GetDataHomeLaboratorioModel: Mappable {
    
    var dateInit = ""
    var dateFinish = ""
    var userAfiliado = ConsultaGrupoFamiliarModel(JSON: [:])!
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        dateInit <- map["dateInit"]
        dateFinish <- map["dateFinish"]
        userAfiliado <- map["userAfiliado"]
        
    }
    
}
