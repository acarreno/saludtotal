//
//  LaboratoriosModel.swift
//  Salud-total
//
//  Created by csanchez on 11/26/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper

class LaboratoriosModel: Mappable {
    
    var cell = ""
    var nameAfiliate = ""
    var dates = ""
    var alto = 0
    
    var resulLaLaboratory = DiagnosticoModel(JSON: [:])!
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        cell <- map["cell"]
        nameAfiliate <- map["nameAfiliate"]
        dates <- map["dates"]
        alto <- map["alto"]
        
        resulLaLaboratory <- map["resulLaLaboratory"]
        
    }
    
}
