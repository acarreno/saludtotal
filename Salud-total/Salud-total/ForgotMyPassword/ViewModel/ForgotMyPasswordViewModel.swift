//
//  ForgotMyPasswordViewModel.swift
//  Salud-total
//
//  Created by csanchez on 12/23/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

protocol ForgotMyPasswordViewModelProtocol {
    func alert(_ text: String, popView: Bool)
    func subShowLoading()
    func subHiddenLoading()
}

class ForgotMyPasswordViewModel: MasterViewModel {
    
    var delegate: ForgotMyPasswordViewModelProtocol!
    var listDocumentType = [String]()
    
    func login(){
        delegate.subShowLoading()
        RegisterManager.preLogin { (isError, text) in
            self.delegate.subHiddenLoading()
            if isError {
                self.delegate.alert(text, popView: true)
            }else{
                for item in self.pickerData {
                    self.listDocumentType.append(item.name)
                }
            }
        }
    }
    
    func validateFrm(documentType: String, documentNumber: String) -> String {
        var mensaje = ""
        
        if documentType == "" {
            mensaje = "Selecciona tipo identificación"
        }else if documentNumber == "" {
            mensaje = "Ingresa tu número de identificación"
        }
        
        return mensaje
    }
    
    func sendFogotMyPasword(documentType: String, documentNumber: String){
        let document = retTypeDoc(searchBy: .name, search: documentType)!
        let documentId = Int(document.code)!
        
        RegisterManager.recoverPassword(documentType: documentId, documentNumber: documentNumber, susses: { (text) in
            self.delegate.alert(text, popView: true)
        }) { (errorText) in
            self.delegate.alert(errorText, popView: false)
        }
        
    }
}
