//
//  ForgotMyPasswordRouting.swift
//  Salud-total
//
//  Created by csanchez on 12/23/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

extension ForgotMyPasswordViewController {
    
    static func instantiate() -> ForgotMyPasswordViewController {
        
        let storyboard = UIStoryboard(name: "ForgotMyPassword", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ForgotMyPasswordViewController
        return vc
        
    }
    
    func openPicker(list:[String],callback:@escaping (_ index:Int)->()){
        JACSSelectViewController.open(navController: self.navigationController!, list: list) { (index) in
            callback(index)
        }
    }
    
}
