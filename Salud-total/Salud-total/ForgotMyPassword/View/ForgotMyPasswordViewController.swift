//
//  ForgotMyPasswordViewController.swift
//  Salud-total
//
//  Created by csanchez on 12/23/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class ForgotMyPasswordViewController: UIViewController {
    
    @IBOutlet weak var txtIdentification: UITextField!
    @IBOutlet weak var lblDocumentType: UILabel!
    
    let vm = ForgotMyPasswordViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        // Do any additional setup after loading the view.
    }
    
    func initView(){
        addHeader()
        
        vm.delegate = self
        vm.login()
    }
    
    @IBAction func openDocumentType(_ sender: Any) {
        openPicker(list: vm.listDocumentType) { (index) in
            self.lblDocumentType.text = self.vm.listDocumentType[index]
        }
    }
    
    @IBAction func validateData(_ sender: Any) {
        let getDocumentType = lblDocumentType.text == "Selecciona el tipo de identificación" ? "": lblDocumentType.text
        let mensaje = vm.validateFrm(documentType: getDocumentType!, documentNumber: txtIdentification.text!)
        
        if mensaje != "" {
            alert(msj: mensaje){}
        }else{
            vm.sendFogotMyPasword(documentType: getDocumentType!, documentNumber: txtIdentification.text!)
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ForgotMyPasswordViewController: ForgotMyPasswordViewModelProtocol {
    
    func subShowLoading() {
        showLoading()
    }
    
    func subHiddenLoading() {
        hiddenLoading()
    }
    
    func alert(_ text: String, popView: Bool) {
        
        alert( msj: text) {
            if popView {
                self.navigationController?.popViewController(animated: true)
            }
        }
        
    }
    
}
