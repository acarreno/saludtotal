//
//  DirectoryDeparmentModel.swift
//  Salud-total
//
//  Created by iMac on 12/19/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper

class DirectoryFilterModel : Mappable {
    
    var nombre=""
    var codigo=""
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        nombre <- map["nombre"]
        codigo <- map["codigo"]
        
    }
    
    
}
