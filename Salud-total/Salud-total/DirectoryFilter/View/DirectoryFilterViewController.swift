//
//  DirectoryFilterViewController.swift
//  Salud-total
//
//  Created by iMac on 12/18/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit
import CoreLocation

class DirectoryFilterViewController: UIViewController {

    @IBOutlet weak var lblNameDep: UILabel!
    @IBOutlet weak var lblCityName: UILabel!
    @IBOutlet weak var lblTypeName: UILabel!
    @IBOutlet weak var btnBuscar: UIButton!
    @IBOutlet weak var sCloseToMe: UISwitch!
    
    let locationManager = CLLocationManager()
    let vm=DirectoryFilterViewModel()
    var geocoderLocationManager = CLLocationCoordinate2D()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideBack()
        addHeader()
        btnBuscar.backgroundColor=#colorLiteral(red: 0.5019607843, green: 0.5019607843, blue: 0.5019607843, alpha: 1)
        btnBuscar.isEnabled=false
        self.showLoading()
        vm.initViewModel(susses: {
            self.hiddenLoading()
        }) { (error) in
            self.hiddenLoading()
            self.alert(msj: error, callback: {
                self.navigationController?.popViewController(animated: true)
            })
        }
    
        // Do any additional setup after loading the view.
    }
    
    @IBAction func selectDep(_ sender: Any) {
        showLoading()
        vm.getDeps(susses: {
            self.hiddenLoading()
           
            JACSSelectViewController.open(navController: self.navigationController!, list: self.vm.listNames, callback: { (index) in
                self.vm.depSelected=self.vm.listDesps[index]
                self.lblNameDep.text=self.vm.depSelected?.nombre
                self.lblNameDep.textColor=#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            })
        }) { (error) in
            self.hiddenLoading()
            self.alert( msj: error, callback: {
                self.navigationController?.popViewController(animated: true)
            })
        }
    }
    
    @IBAction func tapSelectCity(_ sender: Any) {
        vm.getCities(susses: {
            self.hiddenLoading()
            
            JACSSelectViewController.open(navController: self.navigationController!, list: self.vm.listNames, callback: { (index) in
                self.vm.citySelected=self.vm.listCities[index]
                self.lblCityName.text=self.vm.citySelected?.nombre
                self.lblCityName.textColor=#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            })
        }) { (error) in
            self.hiddenLoading()
            self.alert( msj: error, callback: {
                self.navigationController?.popViewController(animated: true)
            })
        }
    }
    
    @IBAction func tapSelectType(_ sender: Any) {
        vm.getTypes(susses: {
            self.hiddenLoading()
            self.btnBuscar.backgroundColor=#colorLiteral(red: 0.2500286698, green: 0.3332767487, blue: 0.618616581, alpha: 1)
            self.btnBuscar.isEnabled=true
            JACSSelectViewController.open(navController: self.navigationController!, list: self.vm.listNames, callback: { (index) in
                self.vm.typeSelected=self.vm.listTypes[index]
                self.lblTypeName.text=self.vm.typeSelected?.nombre
                self.lblTypeName.textColor=#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            })
        }) { (error) in
            self.hiddenLoading()
            self.alert( msj: error, callback: {
                self.navigationController?.popViewController(animated: true)
            })
        }
    }
    
    @IBAction func changeCloseToMe(_ sender: UISwitch) {
        if sender.isOn{
            if vm.typeSelected != nil{
                prepareLocationManager()
            }else{
                sCloseToMe.isOn=false
            }
            
            
        }
        
    }
    func closeToMe(){
        showLoading()
        if vm.typeSelected != nil{
            vm.closeToMe(susses: {
                self.hiddenLoading()
                
                
            }) { (error) in
                self.hiddenLoading()
                self.alert( msj: error, callback: {
                    print("error")
                })
            }
        }else{
            self.alert( msj: "Debes escoger un servicio", callback: {
                print("error")
            })
        }
        
    }
    @IBAction func TapSearch(_ sender: Any) {
        
        if sCloseToMe.isOn{
            debugPrint(vm.listDirectories.toJSON())
            goToDirectories(list: vm.listDirectories)
        }else{
            showLoading()
            vm.getDirectories(susses: {
                self.hiddenLoading()
                self.goToDirectories(list: self.vm.listDirectories)
            }, failed: { error in
                self.hiddenLoading()
                self.alert( msj: error, callback: {
                    print("error")
                })
            })
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DirectoryFilterViewController:CLLocationManagerDelegate{
    func prepareLocationManager(){
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted, .denied, .notDetermined:
            break
        default:
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[locations.count - 1]
        vm.lat=location.coordinate.latitude
        vm.lgn=location.coordinate.longitude
        closeToMe()
        if location.horizontalAccuracy > 0 {
            
            locationManager.stopUpdatingLocation()
            geocoderLocationManager = location.coordinate
            
            
            
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
       
        
    }
    
}
