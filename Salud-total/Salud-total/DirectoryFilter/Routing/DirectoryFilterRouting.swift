//
//  DirectoryFilterRouting.swift
//  Salud-total
//
//  Created by iMac on 12/18/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//


import Foundation
import UIKit

extension DirectoryFilterViewController{
    static func instantiate() -> DirectoryFilterViewController {
        let storyboard = UIStoryboard(name: "DirectoryFilter", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! DirectoryFilterViewController
        return vc
    }
    func goToDirectories(list:[DirectoryModel]){
        let vc=DirectoryViewController.instantiate()
        vc.vm.listDirectories=list
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
