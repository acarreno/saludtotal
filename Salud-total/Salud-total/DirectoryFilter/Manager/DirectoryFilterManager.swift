//
//  DirectoryFilterManager.swift
//  Salud-total
//
//  Created by iMac on 12/19/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper

class DirectoryFilterManager: UIViewController{
    
    func getTokenDirectory(susses:@escaping ()->(),failed:@escaping (String)->()){
        loginApp(){(finish,res)in
            if finish{
                let url = "\(URlGet().tokenDirectorio)/?Token=\(res)&Origen=APP"
                self.servGet(url:url , callback: { (finish, res) in
                    if (res is [String:Any]){
                        let response=GetTokenModel(JSON: (res as! [String:Any]))!
                        
                        if finish{
                            UserDefaults.standard.set("Bearer \(response.token)", forKey: "tokenAutorization")
                            susses()
                            
                        }else{
                            failed(response.Error)
                            
                        }
                    }
                })
            }else{
                failed(res)
            }
        }
        
    }
    func getDep(susses:@escaping ([DirectoryFilterModel])->(),failed:@escaping (String)->()) {
        let url=URlGet().Departamentos
        self.servGet(url:url, isHeaders: true) { (finish, res) in
            if finish{
                let reponse=res as! [[String:Any]]
                let listDeps=Mapper<DirectoryFilterModel>().mapArray(JSONObject: (reponse) )!
                susses(listDeps)
            }else{
                failed(res as! String)
            }
        }
        
    }
    func getCities(codeDep:String,susses:@escaping ([DirectoryFilterModel])->(),failed:@escaping (String)->()) {
        let url="\(URlGet().Ciudades)?Departamento=\(codeDep)"
        self.servGet(url:url, isHeaders: true) { (finish, res) in
            if finish{
                let reponse=res as! [[String:Any]]
                let listDeps=Mapper<DirectoryFilterModel>().mapArray(JSONObject: (reponse) )!
                susses(listDeps)
            }else{
                failed(res as! String)
            }
        }
        
    }
    
    func getTypes(codeDep:String,codeCity:String,susses:@escaping ([DirectoryFilterModel])->(),failed:@escaping (String)->()) {
        let url="\(URlGet().TipoPunto)?Departamento=\(codeDep)&Municipio=\(codeCity)"
        self.servGet(url:url, isHeaders: true) { (finish, res) in
            if finish{
                let reponse=res as! [[String:Any]]
                let listDeps=Mapper<DirectoryFilterModel>().mapArray(JSONObject: (reponse) )!
                susses(listDeps)
            }else{
                failed(res as! String)
            }
        }
        
    }
    func getCloseToMe(lat:Double,lgn:Double,type:String,susses:@escaping ([DirectoryModel])->(),failed:@escaping (String)->()){
        let url="\(URlGet().MasCercano)?Latitud=\(lat)&Longitud=\(lgn)&Tipo=\(type)"
        self.servGet(url:url, isHeaders: true) { (finish, res) in
            if finish{
                let reponse=res as! [[String:Any]]
                let listDeps=Mapper<DirectoryModel>().mapArray(JSONObject: (reponse) )!
                susses(listDeps)
            }else{
                failed(res as! String)
            }
        }
        
    }
    func getDirectory(dep:String,city:String,type:String,susses:@escaping ([DirectoryModel])->(),failed:@escaping (String)->()) {
        let url="\(URlGet().Directorio)?Departamento=\(dep)&Municipio=\(city)&Tipo=\(type)"
        getTokenDirectory(susses: { () in
            
            self.servGet(url:url, isHeaders: true) { (finish, res) in
                if finish{
                    let reponse=res as! [[String:Any]]
                    let listDirectory=Mapper<DirectoryModel>().mapArray(JSONObject: (reponse) )!
                    susses(listDirectory)
                }else{
                    failed(res as! String)
                }
            }
            
        }) { (res) in
            failed(res)
        }
        
    }
}
