//
//  DirectoryFilterViewModel.swift
//  Salud-total
//
//  Created by iMac on 12/18/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//


import Foundation
import UIKit
import ObjectMapper

class DirectoryFilterViewModel  {
    
    let manager=DirectoryFilterManager()
    var listDesps=[DirectoryFilterModel]()
    var listCities=[DirectoryFilterModel]()
    var listTypes=[DirectoryFilterModel]()
    var listDirectories=[DirectoryModel]()
    var depSelected:DirectoryFilterModel?
    var citySelected:DirectoryFilterModel?
    var typeSelected:DirectoryFilterModel?
    var listNames=[String]()
    var lat=Double(0)
    var lgn=Double(0)
    
    func initViewModel(susses:@escaping ()->(),failed:@escaping (String)->()){
        depSelected=DirectoryFilterModel(JSON: ["codigo":"11"])
        citySelected=DirectoryFilterModel(JSON: ["codigo":"001"])
        manager.getTokenDirectory(susses: {
            susses()
        }) { (error) in
            failed(error)
        }
    }
    func getDeps(susses:@escaping ()->(),failed:@escaping (String)->()){
        manager.getDep(susses: { (res) in
            debugPrint(res)
            self.listDesps=res
            self.filterDeps(res )
            susses()
        }) { (error) in
            failed(error)
        }
    }
    func getCities(susses:@escaping ()->(),failed:@escaping (String)->()){
        manager.getCities(codeDep:depSelected!.codigo,susses: { (res) in
            debugPrint(res)
            self.listCities=res
            self.filterDeps(res )
            susses()
        }) { (error) in
            failed(error)
        }
    }
    
    func getTypes(susses:@escaping ()->(),failed:@escaping (String)->()){
        manager.getTypes(codeDep: depSelected!.codigo, codeCity:citySelected!.codigo, susses: { (res) in
            debugPrint(res)
            self.listTypes=res
            self.filterDeps(res)
            susses()
        }) { (error) in
            failed(error)
        }
        
    }
    
    func filterDeps(_ list:[DirectoryFilterModel] ){
        listNames=[]
        for dep in list {
            listNames.append(dep.nombre)
        }
       
    }
    func closeToMe(susses:@escaping ()->(),failed:@escaping (String)->()){
        manager.getCloseToMe(lat: lat, lgn: lgn,type:typeSelected!.codigo, susses: { (res) in
            self.listDirectories=res
            susses()
        }) { (error) in
            failed(error)
        }
    }
    func getDirectories(susses:@escaping ()->(),failed:@escaping (String)->()){
        manager.getDirectory(dep: depSelected!.codigo, city: citySelected!.codigo, type:typeSelected!.codigo , susses: { (res) in
            self.listDirectories=res
            susses()
        }) { (error) in
            failed(error)
        }
    }
}
