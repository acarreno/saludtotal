//
//  Enums.swift
//  Salud-total
//
//  Created by Andrey Carreño on 11/19/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
enum SearchBy {
    case Id
    case name
    case code
}
enum cityAttr {
    case CiudadId
    case CodDepartamento
    case CodMunicipio
   case Departamento
    case EpsSucursalId
    case Nombre
    case NombreMostrar
    case TipoUbicacionId
    case cobertura
    
}
