//
//  DiagnosticoManager.swift
//  Salud-total
//
//  Created by csanchez on 12/18/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit
import ObjectMapper


class DiagnosticoManager {
    
    static func GenerarResultados(
        numeroDocumento: String,
        tipoDocumento:String,
        pIdResultado:String,
        pTipoResultado:String,
        pEmail:String,
        callback: @escaping (_ result: GenerarResultadoLaboratorioModel, _ finish:Bool, _ error:String)->()
    ){
        
        DiagnosticoManager.validarToken(){ (finish,error) in
            var response = GenerarResultadoLaboratorioModel(JSON: [:])!
            
            if finish {
                let urlBase = "https://pruebas.saludtotal.com.co/ApiApoyoDX/api/GenerarResultadoApoyoDiagnostico"
                var url = "\(urlBase)?NumeroDocumento=\(numeroDocumento)&TipoDocumento=\(tipoDocumento)&pIdResultado=\(pIdResultado)"
                url += "&pTipoResultado=\(pTipoResultado)&pEmail=\(pEmail)&AplicativoOrigen=3&ClasificacionOrigen=1"
                
                Services().get(url: url, isHeaders: true) { (res, finish) in
                    print(res)
                    if finish {
                        if let getRes = res as? [String:Any] {
                            response = GenerarResultadoLaboratorioModel(JSON: getRes)!
                            if response.MensajError != "" {
                                callback(response, false, response.MensajError)
                            }else{
                                callback(response, true, "")
                            }
                        }else{
                            callback(response, false, "Error en el formato de la respuesta")
                        }
                    }else{
                        callback(response,false, (res as! String))
                    }
                }
                
            }else{
                callback(response, false, error)
            }
                
        }
    }
    
    
    
    static func ConsultaReporteDianostico(
        numeroDocumento: String,
        tipoDocumento:String,
        pInicioFechaLaboratorio:String,
        pFinFechaLaboratorio:String,
        callback: @escaping (_ result: [DiagnosticoModel], _ finish:Bool, _ error:String)->()
    ){
        
        DiagnosticoManager.validarToken(){ (finish,error) in
            var response = [DiagnosticoModel]()
            
            if finish {
                let urlBase = "https://pruebas.saludtotal.com.co/ApiApoyoDX/api/ReporteApoyoDX"
                
                var url = "\(urlBase)?NumeroDocumento=\(numeroDocumento)&TipoDocumento=\(tipoDocumento)&pInicioFechaLaboratorio=\(pInicioFechaLaboratorio)"
                url += "&pFinFechaLaboratorio=\(pFinFechaLaboratorio)&AplicativoOrigen=3&ClasificacionOrigen=1"
                
                Services().get(url: url, isHeaders: true) { (res, finish) in
                    if finish {
                        if let getRes = res as? [[String:Any]] {
                            var data = [DiagnosticoModel]()
                            for item in getRes{
                                let resumen = DiagnosticoModel(JSON: item)
                                data.append(resumen!)
                            }
                            response = data
                            if response[0].MensajError != "" {
                                callback(response, false, response[0].MensajError)
                            }else{
                                callback(response, true, "")
                            }
                        }else{
                            if let isError = res as? String {
                                callback(response, false, isError)
                            }else{
                                callback(response, false, "Error interno")
                            }
                        }
                    }else{
                        callback(response,false, (res as! String) )
                    }
                }
            }else{
                callback(response, false, error)
            }
        }
        
    }
    
    static func validarToken(callback: @escaping (_ finish:Bool, _ error:String)->()){
        if let tokenSesion = UserDefaults.standard.string(forKey: "token"), tokenSesion != "" {
            let stateTonkenLive = DiagnosticoManager.validateTokenLife()
            if let tokenAfiliados = UserDefaults.standard.string(forKey: "token_diagnostico"), tokenAfiliados != "", stateTonkenLive {
                UserDefaults.standard.set(tokenAfiliados, forKey: "tokenAutorization")
                callback(true,"")
            }else{
                DiagnosticoManager.GetToken(callback: callback)
            }
        }else{
            callback(false, "Debe iniciar sesión de nuevo" )
        }
    }
    
    static func GetToken(callback: @escaping (_ finish:Bool, _ error:String)->()){
        
        let tokenSesion = UserDefaults.standard.string(forKey: "token")!
        let urlBase = "https://pruebas.saludtotal.com.co/ApiApoyoDX/api/Gettoken/Gettoken"
        let url="\(urlBase)?token=\(tokenSesion)&aporigen=APP"
        
        Services().post(url: url, parameters: [:]) { (res, finish) in
            if finish {
                let response = GetTokenModel(JSON: (res as! [String:Any]) )!
                
                if response.Token != "" {
                    let date = Date().adding(minutes: 14)
                    let dateMl = date.millisecondsSince1970
                    let mlToString = String(dateMl)
                    UserDefaults.standard.set(mlToString, forKey: "tokenLife_diagnostico")
                    UserDefaults.standard.set(response.Token, forKey: "token_diagnostico")
                    UserDefaults.standard.set(response.Token, forKey: "tokenAutorization")
                    callback(true, "")
                }else{
                    callback(false, response.Message)
                }
                
            }else{
                callback(false, "Error al intentar ingresar al modulo" )
            }
        }
        
    }
    
    static func validateTokenLife() -> Bool {
        var stateTonkenLive = false
        
        if let dataTonkenLive = UserDefaults.standard.string(forKey: "tokenLife_diagnostico") {
            let intDataTonkenLive = Int64(dataTonkenLive)!
            let actualDataMl = Date().millisecondsSince1970
            if intDataTonkenLive > actualDataMl {
                stateTonkenLive = true
            }
        }
        
        return stateTonkenLive
    }
}
