//
//  GetTokenModel.swift
//  Salud-total
//
//  Created by csanchez on 11/25/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import ObjectMapper

class GetTokenModel: Mappable {
    
    var Token = ""
    var Valido = false
    var Error = ""
    var token = ""
    var Message = ""
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        Token <- map["Token"]
        Valido <- map["Valido"]
        Error <- map["Error"]
        token <- map["token"]
        Message <- map["Message"]
    }
}
