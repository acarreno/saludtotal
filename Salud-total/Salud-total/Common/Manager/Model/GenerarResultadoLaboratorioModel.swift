//
//  GenerarResultadoLaboratorioModel.swift
//  Salud-total
//
//  Created by csanchez on 12/23/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper

class GenerarResultadoLaboratorioModel: Mappable {
    
    var Contrato = ""
    var FechaLaboratorio = ""
    var IdAyudaDx = ""
    var TipoResultado = ""
    var IpsAutorizada = ""
    var Servicio = ""
    var FechaServicio = ""
    var OrigenError = ""
    var MensajError = ""
    var UrlResultado = ""
    var MensajeRespuesta = ""
    var EmailUsuario = ""
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        Contrato <- map["Contrato"]
        FechaLaboratorio <- map["FechaLaboratorio"]
        IdAyudaDx <- map["IdAyudaDx"]
        TipoResultado <- map["TipoResultado"]
        IpsAutorizada <- map["IpsAutorizada"]
        Servicio <- map["Servicio"]
        FechaServicio <- map["FechaServicio"]
        OrigenError <- map["OrigenError"]
        MensajError <- map["MensajError"]
        UrlResultado <- map["UrlResultado"]
        MensajeRespuesta <- map["MensajeRespuesta"]
        EmailUsuario <- map["EmailUsuario"]
        
    }
}
