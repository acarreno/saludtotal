//
//  ForgotMyPasswordModel.swift
//  Salud-total
//
//  Created by csanchez on 12/26/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import ObjectMapper

class ForgotMyPasswordModel: Mappable {
    
    var Descripcion = ""
    var Respuesta = ""
    var Error = ""
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        Descripcion <- map["Descripcion"]
        Respuesta <- map["Respuesta"]
        Error <- map["Error"]
    }
}
