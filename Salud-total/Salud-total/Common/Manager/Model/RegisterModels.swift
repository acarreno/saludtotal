//
//  RegisterModels.swift
//  Salud-total
//
//  Created by csanchez on 12/27/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import ObjectMapper

class previousValidationModel: Mappable {
    
    var Valido = ""
    var Error = ""
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        Valido <- map["Valido"]
        Error <- map["Error"]
    }
}

class generateTempPasswordModel: Mappable {
    
    var Respuesta = ""
    var Descripcion = ""
    var Vigencia = ""
    var Numeroenviado = ""
    var TextoAdicional = ""
    var Error = ""
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        Respuesta <- map["Respuesta"]
        Descripcion <- map["Descripcion"]
        Vigencia <- map["Vigencia"]
        Numeroenviado <- map["Numeroenviado"]
        TextoAdicional <- map["TextoAdicional"]
        Error <- map["Error"]
    }
}

class responseRegidterModel: Mappable {
    
    var tipoUsuario = 0
    var tipoIdEmpleador = ""
    var IdEmpleador = ""
    var tipoIdUsuario = 1
    var IdUsuario = ""
    var RazonSocial = ""
    var Apellidos = ""
    var Nombres = ""
    var telefono = ""
    var Celular = ""
    var CiudadUsuario = 0
    var Correo = ""
    var Clave = ""
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        tipoUsuario <- map["tipoUsuario"]
        tipoIdEmpleador <- map["tipoIdEmpleador"]
        IdEmpleador <- map["IdEmpleador"]
        tipoIdUsuario <- map["tipoIdUsuario"]
        IdUsuario <- map["IdUsuario"]
        RazonSocial <- map["RazonSocial"]
        Apellidos <- map["Apellidos"]
        Nombres <- map["Nombres"]
        telefono <- map["telefono"]
        Celular <- map["Celular"]
        CiudadUsuario <- map["CiudadUsuario"]
        Correo <- map["Correo"]
        Clave <- map["Clave"]
    }
}


