//
//  ConsultaGrupoFamiliarModel.swift
//  Salud-total
//
//  Created by csanchez on 11/25/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper

class ConsultaGrupoFamiliarModel: Mappable {
    
    var BeneficiarioId = "";
    var BeneficiarioTipoDescripcion = "";
    var TipoDocumento = "";
    var BeneficiarioTipoId = "";
    var BeneficiarioConsecutivo = "";
    var Nombres = "";
    var Direccion = "";
    var Telefono = 0;
    var CiudadId = "";
    var ipsmedicaid = "";
    var ipsodontologicaid = "";
    var CotizanteId = 0;
    var CotizanteTipoId = "";
    var Parentesco = "";
    var EstadoDetallado = "";
    var TipoBeneficiarioId = false;
    var UPC = "";
    var EsCotizanteSiNo = "";
    var TieneContratoVigente = "";
    var Contrato = "";
    var EstadoServicio = "";
    var Edad = "";
    var IdBeneficiario = "";
    var DocumentosFaltantes = "";
    var EstadoGeneral = "";
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        BeneficiarioId <- map["BeneficiarioId"]
        BeneficiarioTipoDescripcion <- map["BeneficiarioTipoDescripcion"]
        TipoDocumento <- map["TipoDocumento"]
        BeneficiarioTipoId <- map["BeneficiarioTipoId"]
        BeneficiarioConsecutivo <- map["BeneficiarioConsecutivo"]
        Nombres <- map["Nombres"]
        Direccion <- map["Direccion"]
        Telefono <- map["Telefono"]
        CiudadId <- map["CiudadId"]
        ipsmedicaid <- map["ipsmedicaid"]
        ipsodontologicaid <- map["ipsodontologicaid"]
        CotizanteId <- map["CotizanteId"]
        CotizanteTipoId <- map["CotizanteTipoId"]
        Parentesco <- map["Parentesco"]
        EstadoDetallado <- map["EstadoDetallado"]
        TipoBeneficiarioId <- map["TipoBeneficiarioId"]
        UPC <- map["UPC"]
        EsCotizanteSiNo <- map["EsCotizanteSiNo"]
        TieneContratoVigente <- map["TieneContratoVigente"]
        Contrato <- map["Contrato"]
        EstadoServicio <- map["EstadoServicio"]
        Edad <- map["Edad"]
        IdBeneficiario <- map["IdBeneficiario"]
        DocumentosFaltantes <- map["DocumentosFaltantes"]
        EstadoGeneral <- map["EstadoGeneral"]
        
    }
    
}
