//
//  GeneracionCertificadoModel.swift
//  Salud-total
//
//  Created by csanchez on 11/25/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper

class GeneracionCertificadoModel: Mappable {
    
    var OrigenError = ""
    var MensajError = ""
    var IdEstado = ""
    var DescripcionErrorNegocio = ""
    var MensajeRespuesta = ""
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        OrigenError <- map["OrigenError"]
        MensajError <- map["MensajError"]
        IdEstado <- map["IdEstado"]
        DescripcionErrorNegocio <- map["DescripcionErrorNegocio"]
        MensajeRespuesta <- map["MensajeRespuesta"]
    }
}
