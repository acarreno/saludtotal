//
//  AfiliadosManager.swift
//  Salud-total
//
//  Created by csanchez on 11/25/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit
import ObjectMapper

class AfiliadosManager {
    
    static func ConsultaGrupoFamiliar(BeneficiarioId: String,BeneficiarioTipoId:String, callback: @escaping (_ result: [ConsultaGrupoFamiliarModel], _ finish:Bool, _ error:String)->()) {
        
        AfiliadosManager.validarToken(){ (finish,error) in
            var response = [ConsultaGrupoFamiliarModel]()
            
            if finish {
                let urlBase = "https://pruebas.saludtotal.com.co/STAPI_AfiliadosPBS/api/ConsultaAfiliados/ConsultaGrupoFamiliar"
                let url = "\(urlBase)?BeneficiarioId=\(BeneficiarioId)&BeneficiarioTipoId=\(BeneficiarioTipoId)"
                
                Services().get(url: url, isHeaders: true) { (res, finish) in
                    if let listaFamilyGroup = res as? [[String:Any]] {
                        if listaFamilyGroup.count > 0 {
                            for item in listaFamilyGroup {
                                let itemInfoPersonas = ConsultaGrupoFamiliarModel(JSON: item)!
                                response.append(itemInfoPersonas)
                            }
                            callback(response, true, "")
                        }else{
                            callback(response, false, "No se encontró información")
                        }
                    }else{
                        if let isError = res as? String {
                            callback(response, false, isError)
                        }else{
                            callback(response, false, "Error interno")
                        }
                    }
                }
            }else{
                callback(response, false, error)
            }
        }
        
    }
    
    static func GeneracionCertificado(BeneficiarioId:String,BeneficiarioTipoId:String, callback: @escaping (_ result: GeneracionCertificadoModel, _ finish:Bool, _ error:String)->()) {
        
        AfiliadosManager.validarToken(){ (finish,error) in
            var response = GeneracionCertificadoModel(JSON:[:])!
            
            if finish {
                let urlBase = "https://pruebas.saludtotal.com.co/STAPI_AfiliadosPBS/api/Certificaciones/GeneracionCertificado"
                let url="\(urlBase)?BeneficiarioId=\(BeneficiarioId)&BeneficiarioTipoId=\(BeneficiarioTipoId)"
                
                Services().get(url: url, isHeaders: true) { (res, finish) in
                    if finish {
                        if let getRes = res as? [String:Any] {
                            response = GeneracionCertificadoModel(JSON: (getRes) )!
                            if response.IdEstado == "1" {
                                callback(response, true, "")
                            }else{
                                callback(response,false,response.DescripcionErrorNegocio)
                            }
                        }else{
                            if let isError = res as? String {
                                callback(response, false, isError)
                            }else{
                                callback(response, false, "Error interno")
                            }
                        }
                    }else{
                        callback(response,false, (res as! String) )
                    }
                }
            }else{
                callback(response, false, error)
            }
        }
    }
    
    static func validarToken(callback: @escaping (_ finish:Bool, _ error:String)->()){
        if let tokenSesion = UserDefaults.standard.string(forKey: "token"), tokenSesion != "" {
            let stateTonkenLive = AfiliadosManager.validateTokenLife()
            if let tokenAfiliados = UserDefaults.standard.string(forKey: "token_afiliados"), tokenAfiliados != "", stateTonkenLive {
                UserDefaults.standard.set(tokenAfiliados, forKey: "tokenAutorization")
                callback(true,"")
            }else{
                AfiliadosManager.GetToken(callback: callback)
            }
        }else{
            callback(false, "Debe iniciar sesión de nuevo" )
        }
    }
    
    static func GetToken(callback: @escaping (_ finish:Bool, _ error:String)->()){
        
        let tokenSesion = UserDefaults.standard.string(forKey: "token")!
        let urlBase = "https://pruebas.saludtotal.com.co/STAPI_AfiliadosPBS/api/Token/GetToken"
        let url="\(urlBase)?Token=\(tokenSesion)&Origen=APP"
        
        Services().get(url: url) { (res, finish) in
            if finish {
                let response = GetTokenModel(JSON: (res as! [String:Any]) )!
                
                if response.Valido {
                    let date = Date().adding(minutes: 14)
                    let dateMl = date.millisecondsSince1970
                    let mlToString = String(dateMl)
                    UserDefaults.standard.set(mlToString, forKey: "tokenLife_afiliados")
                    UserDefaults.standard.set(response.Token, forKey: "token_afiliados")
                    UserDefaults.standard.set(response.Token, forKey: "tokenAutorization")
                    callback(true, "")
                }else{
                    callback(false, response.Error)
                }
                
            }else{
                callback(false, "Error al intentar ingresar al modulo" )
            }
        }
        
    }
    
    static func validateTokenLife() -> Bool {
        var stateTonkenLive = false
        
        if let dataTonkenLive = UserDefaults.standard.string(forKey: "tokenLife_afiliados") {
            let intDataTonkenLive = Int64(dataTonkenLive)!
            let actualDataMl = Date().millisecondsSince1970
            if intDataTonkenLive > actualDataMl {
                stateTonkenLive = true
            }
        }
        
        return stateTonkenLive
    }
    

}
