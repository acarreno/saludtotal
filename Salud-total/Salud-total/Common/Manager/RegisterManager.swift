//
//  RegisterManager.swift
//  Salud-total
//
//  Created by csanchez on 12/23/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit
import ObjectMapper


class RegisterManager {
    
    static func preLogin(failed: @escaping (_ state: Bool, _ textError: String)->()){
        let baseUrl = "https://pruebas.saludtotal.com.co/ApiSeguridad/api/LogIn"
        let url = "\(baseUrl)?PerfilUsuario=AFI&TipoEmpleadorId=N&EmpleadorId=800149453&TipoDocumentoId=C&DocumentoId=999&Clave=saludtotal&aplicativo=APP"
        
        Services().get(url: url, isHeaders: false) { (res, finish) in
            if finish{
                print(res)
                if let response = res as? [String:Any] {
                    if response["Valido"] as! Int == 1 {
                        UserDefaults.standard.set("\(response["Token"] as! String)", forKey: "token")
                        failed(false, "")
                    }else{
                        failed(true, response["Error"] as! String)
                    }
                }
            }else{
                failed(true, res as! String)
            }
        }
    }
    
    static func recoverPassword(documentType: Int, documentNumber: String, susses:@escaping (_ data: String)->(), failed:@escaping (String)->()){
        RegisterManager.stateTokenSesion { (finish, error) in
            if finish{
                let url = "https://pruebas.saludtotal.com.co/STAPI_RegistroPBS/api/Recuperar/RecuperarContrasena"
                let getParameters = [
                    "IdPerfil": "AFI",
                    "TipoDocumentoId": documentType,
                    "DocumentoId": documentNumber,
                    "TipoDocumentoIdPadre": "",
                    "DocumentoIdPadre": "",
                    "CanalventaId": ""
                    
                    ] as [String : Any]
                Services().post(url: url, parameters: getParameters, isHeaders: true) { (res, finish) in
                    if finish {
                        let response = ForgotMyPasswordModel(JSON: res as! [String: Any])!
                        if response.Respuesta == "1" {
                            susses(response.Descripcion)
                        }else{
                            failed(response.Error)
                        }
                    }else{
                        failed("Error interno intentelo de nuevo más tarde (1)")
                    }
                }
            }else{
                failed(error)
            }
        }
    }
    
    static func Registro(
        documentType: Int,
        documentNumber: String,
        code: String,
        data: responseRegidterModel,
        susses:@escaping ()->(),
        failed:@escaping (String)->()
    ){
        RegisterManager.validatePassword(
            tipoDocumento: documentType,
            documento: documentNumber,
            claveTemporal: code
        ) { (finish, error) in
            if finish{
                RegisterManager.stateTokenSesion { (finish, error) in
                    let url = "https://pruebas.saludtotal.com.co/STAPI_RegistroPBS/api/Registro/CrearUsuarioInternet"
                    let getParameters = data.toJSON()
                    Services().post(url: url, parameters: getParameters, isHeaders: true) { (res, finish) in
                        if finish {
                             let response = ForgotMyPasswordModel(JSON: res as! [String: Any])!
                             if response.Respuesta == "1" {
                                //print(response.Descripcion)
                                susses()
                             }else{
                                failed("Error al registrase")
                             }
                        }else{
                            failed(res as! String)
                        }
                    }
                }
            }else{
                failed(error)
            }
        }
    }
    
    static func previousValidation(
        tipoDocumento: String,
        documento: String,
        correo: String,
        callback: @escaping (_ finish:Bool, _ error:String)->()
        ){
        
        RegisterManager.stateTokenSesion { (finish, error) in
            if finish {
                let urlBase = "https://pruebas.saludtotal.com.co/STAPI_RegistroPBS/api/Registro/ValidacionPrevia"
                let url = "\(urlBase)?TipoDocumento=\(tipoDocumento)&Documento=\(documento)&Correo=\(correo)"
                
                Services().get(url: url, isHeaders: true) { (res, finish) in
                    if finish {
                        let response = previousValidationModel(JSON: res as! [String: Any])!
                        if response.Valido == "1" {
                            callback(true, "")
                        }else{
                            callback(false, response.Error)
                        }
                    }else{
                        callback(false, res as! String)
                    }
                }
            }else{
                callback(false, error)
            }
        }
    }
    
    static func generateTempPassword(
        tipoDocumento: String,
        documento: String,
        correo: String,
        susses:@escaping ()->(),
        failed:@escaping (String)->()
    ){
        RegisterManager.stateTokenSesion { (finish, error) in
            if finish {
                RegisterManager.previousValidation(tipoDocumento: tipoDocumento, documento: documento, correo: correo) { (finish, error) in
                    if finish {
                        let urlBase = "https://pruebas.saludtotal.com.co/STAPI_RegistroPBS/api/Registro/GenerarClaveTemp"
                        let url = "\(urlBase)?TipoDocumento=\(tipoDocumento)&Documento=\(documento)"
                        
                        Services().get(url: url, isHeaders: true) { (res, finish) in
                            if finish {
                                let response = generateTempPasswordModel(JSON: res as! [String: Any])!
                                if response.Respuesta == "1" {
                                    var code = response.Numeroenviado.components(separatedBy: "*****")
                                    UserDefaults.standard.set(code[1], forKey: "code_number")
                                    if let codeNumber = UserDefaults.standard.string(forKey: "code_number"), codeNumber != "" {
                                        susses()
                                    }else{
                                        failed("Error Interno")
                                    }
                                }else{
                                    failed(response.Error)
                                }
                            }else{
                                failed(res as! String)
                            }
                        }
                    }else{
                        failed(error)
                    }
                }
            }else{
                failed(error)
            }
        }
    }
    
    static func validatePassword(
        tipoDocumento: Int,
        documento: String,
        claveTemporal: String,
        callback: @escaping (_ finish:Bool, _ error:String)->()
    ){
        let urlBase = "https://pruebas.saludtotal.com.co/STAPI_RegistroPBS/api/Registro/ValidarClave"
        let url = "\(urlBase)?TipoDocumento=\(tipoDocumento)&Documento=\(documento)&ClaveTemporal=\(claveTemporal)"
        
        Services().get(url: url, isHeaders: true) { (res, finish) in
            if finish {
                let response = ForgotMyPasswordModel(JSON: res as! [String: Any])!
                if response.Respuesta == "1" {
                    //print(response.Descripcion)
                    callback(true, "")
                }else{
                    callback(false, response.Descripcion)
                }
            }else{
                callback(false, res as! String)
            }
        }
    }
    
    static func generatePassword(
        tipoDocumento: Int,
        documento: String,
        claveTemporal: String,
        callback: @escaping (_ finish:Bool, _ error:String)->()
        ){
        let urlBase = "https://pruebas.saludtotal.com.co/STAPI_RegistroPBS/api/Registro/GenerarClaveTemp"
        let url = "\(urlBase)?TipoDocumento=\(tipoDocumento)&Documento=\(documento) "
        
        Services().get(url: url, isHeaders: true) { (res, finish) in
            if finish {
                let response = ForgotMyPasswordModel(JSON: res as! [String: Any])!
                if response.Respuesta == "1" {
                    //print(response.Descripcion)
                    callback(true, "")
                }else{
                    callback(false, response.Error)
                }
            }else{
                callback(false, res as! String)
            }
        }
    }
    
    static func stateTokenSesion(callback: @escaping (_ finish:Bool, _ error:String)->()){
        if let tokenSesion = UserDefaults.standard.string(forKey: "token"), tokenSesion != "" {
            RegisterManager.validateTokenSesion() {(finish, error) in
                if finish {
                    callback(true,"")
                }else{
                    callback(false, error)
                }
            }
        }else{
            RegisterManager.preLogin { (isError, _) in
                if isError {
                    callback(true, "Error interno intentelo de nuevo más tarde (2)")
                }else{
                    RegisterManager.validateTokenSesion(){(finish, error) in
                        if finish {
                            callback(true,"")
                        }else{
                            callback(false, error)
                        }
                    }
                }
            }
        }
    }
    
    static func validateTokenSesion(callback: @escaping (_ finish:Bool, _ error:String)->()){
        let stateTonkenLive = DiagnosticoManager.validateTokenLife()
        if let fogorMyPassword = UserDefaults.standard.string(forKey: "token_fogorMyPassword"), fogorMyPassword != "", stateTonkenLive {
        UserDefaults.standard.set(fogorMyPassword, forKey: "tokenAutorization")
            callback(true,"")
        }else{
            RegisterManager.getTokensecion(callback: callback)
        }
    }
    
    static func getTokensecion(callback: @escaping (_ finish:Bool, _ error:String)->()){
        
        let tokenSesion = UserDefaults.standard.string(forKey: "token")!
        let urlBase = "https://pruebas.saludtotal.com.co/STAPI_RegistroPBS/api/Token/GetToken"
        let url="\(urlBase)?Token=\(tokenSesion)&Origen=APP"
        
        Services().get(url: url) { (res, finish) in
            if finish {
                let response = GetTokenModel(JSON: (res as! [String:Any]) )!
                
                if response.Valido {
                    let date = Date().adding(minutes: 10)
                    let dateMl = date.millisecondsSince1970
                    let mlToString = String(dateMl)
                    UserDefaults.standard.set(mlToString, forKey: "tokenLife_fogorMyPassword")
                    UserDefaults.standard.set(response.Token, forKey: "token_fogorMyPassword")
                    UserDefaults.standard.set(response.Token, forKey: "tokenAutorization")
                    callback(true, "")
                }else{
                    callback(false, response.Error)
                }
                
            }else{
                callback(false, res as! String)
            }
        }
        
    }
    
    static func validateTokenLife() -> Bool {
        var stateTonkenLive = false
        
        if let dataTonkenLive = UserDefaults.standard.string(forKey: "tokenLife_fogorMyPassword") {
            let intDataTonkenLive = Int64(dataTonkenLive)!
            let actualDataMl = Date().millisecondsSince1970
            if intDataTonkenLive > actualDataMl {
                stateTonkenLive = true
            }
        }
        
        return stateTonkenLive
    }
    
    static func tokenRemoveUserDefauld(){
        UserDefaults.standard.removeObject(forKey: "token")
        UserDefaults.standard.removeObject(forKey: "tokenLife_fogorMyPassword")
        UserDefaults.standard.removeObject(forKey: "token_fogorMyPassword")
        UserDefaults.standard.removeObject(forKey: "tokenAutorization")
    }
    
}
