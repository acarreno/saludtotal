//
//  HederRouting.swift
//  Salud-total
//
//  Created by iMac on 11/6/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit
extension HeaderViewController{
    
    static func instantiate() -> HeaderViewController {
        
        let storyboard = UIStoryboard(name: "Header", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HeaderViewController
        return vc
    
    }
}
