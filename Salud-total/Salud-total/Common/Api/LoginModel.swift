//
//  LoginModel.swift
//  Salud-total
//
//  Created by Andrey Carreño on 11/17/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper
class LoginModel: Mappable {
    var Error = ""
    var Token = ""
    var Valido = false
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        Error <- map["Error"]
        Token <- map["Token"]
        Valido <- map["Valido"]
    }
}
