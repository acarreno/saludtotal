//
//  ApiVars.swift
//  Salud-total
//
//  Created by Andrey Carreño on 11/17/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation

struct URlGet {

    let login = "https://pruebas.saludtotal.com.co/ApiSeguridad/api/LogIn"
    let ValidaToken = "https://pruebas.saludtotal.com.co/ApiSeguridad/api/ValidaToken"
    let getTokenAfiliados = "https://pruebas.saludtotal.com.co/STAPI_AfiliadosPBS/api/Token/GetToken"
    let ConsultaGrupoFamiliarDetalle = "https://pruebas.saludtotal.com.co/STAPI_AfiliadosPBS/api/ConsultaAfiliados/ConsultaGrupoFamiliarDetalle"
    let ConsultaAfiliados = "https://pruebas.saludtotal.com.co/STAPI_AfiliadosPBS/api/ConsultaAfiliados/ConsultaGrupoFamiliar"
    let GeneracionCertificado = "https://pruebas.saludtotal.com.co/STAPI_AfiliadosPBS/api/Certificaciones/GeneracionCertificado"
    let ConsultaGrupoFamiliar = "https://pruebas.saludtotal.com.co/STAPI_AfiliadosPBS/api/ConsultaAfiliados/ConsultaGrupoFamiliar"
    let GetTokenAfiliarme = "https://pruebas.saludtotal.com.co/STAPI_ComoAfiliarme/api/Token/GetToken"
    let TraerCiudadesPac = "https://pruebas.saludtotal.com.co/STAPI_ComoAfiliarme/api/Ciudades/TraerCiudadesPac"
    let RegistrarSolicitud = "https://pruebas.saludtotal.com.co/STAPI_ComoAfiliarme/api/Registro/RegistrarSolicitud"
    let AppRelacionadas="https://pruebas.saludtotal.com.co/STAPI_ImagenAppPBS/api/AppRelacionadas/AppRelacionadas?Producto=1"
    let GetTokenApps="https://pruebas.saludtotal.com.co/STAPI_ImagenAppPBS/api/Token/GetToken"
    let ValidaTokenNoti="https://pruebas.saludtotal.com.co/ApiSeguridad/api/ValidaToken"
    let notificacion="https://pruebas.saludtotal.com.co/ApiSeguridad/api/notificacion"
    let ObteneImagen="https://pruebas.saludtotal.com.co/STAPI_ImagenAppPBS/api/Imagen/ObteneImagen?Producto=1"
    let tokenDirectorio="https://pruebas.saludtotal.com.co/APIDirectorio/API/Token"
    let Directorio="https://pruebas.saludtotal.com.co/APIDirectorio/API/Directorio"
    let Departamentos="https://pruebas.saludtotal.com.co/APIDirectorio/API/Departamentos"
    let Ciudades="https://pruebas.saludtotal.com.co/APIDirectorio/API/Ciudades"
    let TipoPunto="https://pruebas.saludtotal.com.co/APIDirectorio/API/TipoPunto"
    let MasCercano="https://pruebas.saludtotal.com.co/APIDirectorio/API/MasCercano"
    let ACtualizarDatosAfiliado="https://pruebas.saludtotal.com.co/STAPI_AfiliadosPBS/api/ActualizarDatos/ACtualizarDatosAfiliado"
    let GetIpsMedica = "https://pruebas.saludtotal.com.co/STAPI_AfiliadosPBS/api/GetDocumentos/GetIpsMedica"
    let GetIpsOdontologica="https://pruebas.saludtotal.com.co/STAPI_AfiliadosPBS/api/GetDocumentos/GetIpsOdontologica"
    let ACtualizarIPS="https://pruebas.saludtotal.com.co/STAPI_AfiliadosPBS/api/ActualizarDatos/ACtualizarIPS"
    let getTokenAutorizaciones="https://pruebas.saludtotal.com.co/APIAutorizaciones/API/token"
    let AutorizacionGrupoFamiliar="https://pruebas.saludtotal.com.co/APIAutorizaciones/API/AutorizacionGrupoFamiliar"
    let ConsultaServicioAutorizaApp="https://pruebas.saludtotal.com.co/APIAutorizaciones/API/ConsultaServicioAutorizaApp"
    let ConsultaNapaAutorizaApp="https://pruebas.saludtotal.com.co/APIAutorizaciones/API/ConsultaNapaAutorizaApp"
    let ConsultaDireccionamientoAutorizaApp="https://pruebas.saludtotal.com.co/APIAutorizaciones/API/ConsultaDireccionamientoAutorizaApp"
    let ValidaCantidadMaxServicioAutorizaApp="https://pruebas.saludtotal.com.co/APIAutorizaciones/API/ValidaCantidadMaxServicioAutorizaApp"
    let AutorizaServicioApp="https://pruebas.saludtotal.com.co/APIAutorizaciones/API/AutorizaServicioApp"
    let AutorizacionPorAfiliado="https://pruebas.saludtotal.com.co/APIAutorizaciones/API/AutorizacionPorAfiliado"
    let AutorizacionDetallePorIDResultadoTramite = "https://pruebas.saludtotal.com.co/APIAutorizaciones/API/AutorizacionDetallePorIDResultadoTramite"
    let ID_ResultadoTramite="https://pruebas.saludtotal.com.co/APIAutorizaciones/API/AutorizacionRptEnviarCorreo"
    let ProrrogarServicioApp="https://pruebas.saludtotal.com.co/APIAutorizaciones/API/ProrrogarServicioApp"
    let GetDepartamentos = "https://pruebas.saludtotal.com.co/STAPI_AfiliadosPBS/api/GetDocumentos/GetDepartamentos"
    let Getciudades="https://pruebas.saludtotal.com.co/STAPI_AfiliadosPBS/api/GetDocumentos/Getciudades"
}
