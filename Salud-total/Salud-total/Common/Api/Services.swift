//
//  Services.swift
//  Salud-total
//
//  Created by Andrey Carreño on 11/17/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import Alamofire

class Services {
    //let baseUrl="https://pruebas.saludtotal.com.co"
    
    func get(url:String,closure: @escaping (_ result: Any,_ finish:Bool)->()){
        if NetworkReachabilityManager()!.isReachable {
           
            //let urlString = "\(baseUrl)\(url)"
            let urlString = url
            
            Alamofire.request(urlString, method: .get,headers: nil).responseJSON { response in
                
                switch(response.result) {
                    
                case .success(_):
                    
                    if let value = response.result.value{
                        
                        closure(value as Any,true)
                        
                    }
                    break
                    
                case .failure(_):
                    
                    closure("Error interno",false)
                    break
                    
                }
                
            }
        }else{
            closure(["response":"No hay conexión a internet"],false)
        }
    }
    
    func get(url:String,isHeaders:Bool,closure: @escaping (_ result: Any,_ finish:Bool)->()){
        if NetworkReachabilityManager()!.isReachable {
            var  headers: HTTPHeaders?
            if isHeaders{
                if let token=UserDefaults.standard.string(forKey: "tokenAutorization"){
                    headers = [
                        "Authorization": token,
                    ]
                }
            }
            
            //let urlString="\(baseUrl)\(url)"
            let urlString = url
            
            Alamofire.request(urlString, method: .get,headers: headers).responseJSON { response in
                
                switch(response.result) {
                    
                case .success(_):
                    
                    if let value = response.result.value{
                        closure(value as Any,true)
                    }
                    break
                    
                case .failure(_):
                    
                    closure("Error interno",false)
                    break
                    
                }
                
            }
        }else{
            closure(["response":"No hay conexión a internet"],false)
        }
    }
    
    func post(url:String, parameters: [String: Any], closure: @escaping (_ result: Any,_ finish:Bool)->()){
        
        if NetworkReachabilityManager()!.isReachable {
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                switch(response.result) {
                    
                case .success(_):
                    
                    if let value = response.result.value{
                        
                        closure(value as Any,true)
                        
                    }
                    break
                    
                case .failure(_):
                    
                    closure("Error interno",false)
                    break
                    
                }
            }
        
        }else{
            closure(["response":"No hay conexión a internet"],false)
        }
    }
    
    func post(url:String, parameters: [String: Any],isHeaders:Bool, closure: @escaping (_ result: Any,_ finish:Bool)->()){
        
        if NetworkReachabilityManager()!.isReachable {
            var  headers: HTTPHeaders?
            if isHeaders{
                if let token=UserDefaults.standard.string(forKey: "tokenAutorization"){
                    headers = [
                        "Authorization": token,
                        "Content-Type":"application/json"
                    ]
                }
            }
            Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
                switch(response.result) {
                    
                case .success(_):
                    
                    if let value = response.result.value{
                        
                        closure(value as Any,true)
                        
                    }
                    break
                    
                case .failure(_):
                    
                    closure("Error interno",false)
                    break
                    
                }
            }
            
        }else{
            closure(["response":"No hay conexión a internet"],false)
        }
    }
}
