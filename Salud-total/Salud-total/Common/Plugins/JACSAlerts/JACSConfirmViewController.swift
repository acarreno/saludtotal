//
//  JACSConfirmViewController.swift
//  Salud-total
//
//  Created by Andrey Carreño on 12/29/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class JACSConfirmViewController: UIViewController {

    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var text=""
    var susses: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let html="<p style='text-align:center;width:100%;font-family:\"Futura\"'>\(text)</p>"
        textView.attributedText=html.htmlToAttributedString
        adjustUITextViewHeight(arg : textView)
        btnAccept.frame.origin.y=textView.contentSize.height+textView.frame.origin.y
        btnCancel.frame.origin.y=textView.contentSize.height+textView.frame.origin.y
        alertView.frame.size.height=btnAccept.frame.origin.y+btnAccept.frame.height+20
        alertView.frame.origin.y=(view.frame.height-alertView.frame.size.height)/2
        btnAccept.frame.origin.y=alertView.frame.size.height-btnAccept.frame.height-20
        btnCancel.frame.origin.y=alertView.frame.size.height-btnAccept.frame.height-20
        // Do any additional setup after loading the view.
    }
    
    @IBAction func tapAccepr(_ sender: Any) {
        self.dismiss(animated: true) {
            self.susses!()
        }
    }
    @IBAction func tapCancel(_ sender: Any) {
        self.dismiss(animated: true) {
           
        }
    }
    func adjustUITextViewHeight(arg : UITextView)
    {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
        arg.frame.size.width=alertView.frame.width-10
        arg.frame.origin.x=(alertView.frame.width-arg.frame.size.width)/2
    }
    static func open(navController:UINavigationController,text:String, callback:@escaping()->()) {
        let storyboard = UIStoryboard(name: "JACSAlerts", bundle: Bundle.main)
        
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! JACSConfirmViewController
        vc.text=text
        vc.modalPresentationStyle = .overCurrentContext
        //vc.navigatioVC=navController
        vc.susses={() -> Void in
            
            callback()
        }
        navController.present(vc, animated: true, completion: nil)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
