//
//  JACSAlertViewController.swift
//  Salud-total
//
//  Created by Andrey Carreño on 11/16/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class JACSAlertViewController: UIViewController {
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var btnAccept: UIButton!
    
    var text=""
    var susses: ((_ id: String) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let html="<p style='color:rgb(128,128,128);text-align:center;width:100%;font-family:\"FuturaStd-Medium \"'>\(text)</p>"
        textView.attributedText=html.htmlToAttributedString
        textView.font=UIFont(name:"FuturaStd-BookOblique" , size:CGFloat(14))
        adjustUITextViewHeight(arg : textView)
        btnAccept.frame.origin.y=textView.contentSize.height+textView.frame.origin.y
        alertView.frame.size.height=btnAccept.frame.origin.y+btnAccept.frame.height+20
        alertView.frame.origin.y=(view.frame.height-alertView.frame.size.height)/2
        btnAccept.frame.origin.y=alertView.frame.size.height-btnAccept.frame.height-20
        // Do any additional setup after loading the view.
    }
    
    @IBAction func tapAccepr(_ sender: Any) {
        self.dismiss(animated: true) {
            self.susses!("true")
        }
    }
    func adjustUITextViewHeight(arg : UITextView)
    {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
        arg.frame.size.width=alertView.frame.width-10
        arg.frame.origin.x=(alertView.frame.width-arg.frame.size.width)/2
    }
    static func open(navController:UINavigationController,text:String, callback:@escaping(String)->()) {
        let storyboard = UIStoryboard(name: "JACSAlerts", bundle: Bundle.main)
        
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! JACSAlertViewController
        vc.text=text
        vc.modalPresentationStyle = .overCurrentContext
        //vc.navigatioVC=navController
        vc.susses={(id:String) -> Void in
            
            callback(id)
        }
        navController.present(vc, animated: true, completion: nil)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
