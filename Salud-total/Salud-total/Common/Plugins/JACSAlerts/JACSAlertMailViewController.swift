//
//  JACSAlertMailViewController.swift
//  Salud-total
//
//  Created by iMac on 12/31/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class JACSAlertMailViewController: UIViewController {

    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    @IBOutlet weak var txtMail: UITextField!
    
    var susses: ((_ id: String) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        
        // Do any additional setup after loading the view.
    }
    @IBAction func tapAccepr(_ sender: Any) {
        self.dismiss(animated: true) {
            self.susses!(self.txtMail.text!)
        }
    }
    @IBAction func tapCancel(_ sender: Any) {
        self.dismiss(animated: true) {
            
        }
    }
    func adjustUITextViewHeight(arg : UITextView)
    {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
        arg.frame.size.width=alertView.frame.width-10
        arg.frame.origin.x=(alertView.frame.width-arg.frame.size.width)/2
    }
    static func open(navController:UINavigationController,text:String, callback:@escaping(String)->()) {
        let storyboard = UIStoryboard(name: "JACSAlerts", bundle: Bundle.main)
        
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! JACSAlertMailViewController
        
        vc.modalPresentationStyle = .overCurrentContext
        //vc.navigatioVC=navController
        vc.susses={(id:String) -> Void in
            
            callback(id)
        }
        navController.present(vc, animated: true, completion: nil)
        
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
