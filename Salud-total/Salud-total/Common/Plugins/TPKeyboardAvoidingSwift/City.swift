//
//  City.swift
//  Salud-total
//
//  Created by Andrey Carreño on 11/20/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper
class City: Mappable {
    
    var CiudadId = 0
    var CodDepartamento = ""
    var CodMunicipio = ""
    var Departamento = ""
    var EpsSucursalId = ""
    var Nombre = ""
    var NombreMostrar = ""
    var TipoUbicacionId = ""
    var cobertura = ""

    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        CiudadId <- map["CiudadId"]
        CodDepartamento <- map["CodDepartamento"]
        CodMunicipio <- map["CodMunicipio"]
        Departamento <- map["Departamento"]
        EpsSucursalId <- map["EpsSucursalId"]
        Nombre <- map["Nombre"]
        NombreMostrar <- map["NombreMostrar"]
        TipoUbicacionId <- map["TipoUbicacionId"]
        cobertura <- map["cobertura"]
        
    }
}
