//
//  JACSSelectViewController.swift
//  Salud-total
//
//  Created by Andrey Carreño on 12/13/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class JACSSelectViewController: UIViewController {
    
    @IBOutlet weak var picker: UIPickerView!
    
    let vm=JACSSelectPickerViewModel()
    var susses: ((_ index: Int) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vm.inflatePicker()
        // Do any additional setup after loading the view.
    }
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension JACSSelectViewController: UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return vm.container.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if row>0{
            self.dismiss(animated: true) {
                self.susses!(row-1)
            }
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return vm.container[row]
    }
}
