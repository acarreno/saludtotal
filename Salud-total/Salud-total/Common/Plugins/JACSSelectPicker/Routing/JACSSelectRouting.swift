//
//  JACSSelectRouting.swift
//  Salud-total
//
//  Created by Andrey Carreño on 12/13/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

extension JACSSelectViewController{
    
    static func instantiate() -> JACSSelectViewController {
        
        let storyboard = UIStoryboard(name: "", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! JACSSelectViewController
        
        return vc
        
    }
    
    static func open(navController:UINavigationController,list:[String], callback:@escaping(Int)->()) {
        let storyboard = UIStoryboard(name: "JACSSelectPicker", bundle: Bundle.main)
        
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! JACSSelectViewController
        vc.vm.container=list
        vc.modalPresentationStyle = .overCurrentContext
        //vc.navigatioVC=navController
        vc.susses={(id:Int) -> Void in
            
            callback(id)
        }
        navController.present(vc, animated: true, completion: nil)
        
    }
}
