//
//  WebViewRouting.swift
//  Salud-total
//
//  Created by iMac on 12/2/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

extension WebViewViewController{
    static func instantiate() -> WebViewViewController {
        let storyboard = UIStoryboard(name: "WebView", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! WebViewViewController
        return vc
    }
    
}
