//
//  WebViewViewController.swift
//  Salud-total
//
//  Created by iMac on 12/2/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class WebViewViewController: UIViewController {

    @IBOutlet weak var lblTitulo: UILabel!
    @IBOutlet weak var WebView: UIWebView!
    
    var vm=WebViewViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addHeader()
        lblTitulo.text=vm.title
        
        let url=URLRequest(url: URL(string: vm.url)!)
        WebView.loadRequest(url)
        showLoading()
        WebView.delegate=self
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension WebViewViewController: UIWebViewDelegate{
    func webViewDidFinishLoad(_ webView: UIWebView) {
        hiddenLoading()
    }
}
