//
//  MasterViewModel.swift
//  Salud-total
//
//  Created by csanchez on 11/22/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class MasterViewModel {
    
    let defaults = UserDefaults.standard
    var active_user = UserModel(JSON: [:])
    var pickerData = [TypeDocumentModel]()
    
    init() {
        getActiveUser()
        inflateDocumentType()
    }
    
    func getActiveUser(){
        if let user = UserDefaults.standard.dictionary(forKey: "user") {
            active_user = UserModel(JSON: user)!
        }
    }
    
    func retTypeDoc(searchBy:SearchBy,search:String)->TypeDocumentModel?{
        var key="id"
        var type=TypeDocumentModel()
        let list=[
            [
                "name":"CEDULA DE CIUDADANIA",
                "id":"C",
                "code":"1"
            ],
            [
                "name":"CARNET DIPLOMATICO",
                "id":"CD",
                "code":"2"
            ],
            [
                "name":"CERTIFICADO NACIDO VIVO",
                "id":"CN",
                "code":"3"
            ],
            [
                "name":"CEDULA DE EXTRANJERIA",
                "id":"E",
                "code":"4"
            ],
            [
                "name":"PERSONERIA JURIDICA",
                "id":"J",
                "code":"5"
            ],
            [
                "name":"MENOR DE EDAD",
                "id":"M",
                "code":"6"
            ],
            [
                "name":"MENOR SIN IDENTIFICACION",
                "id":"MS",
                "code":"7"
            ],
            [
                "name":"NIT",
                "id":"N",
                "code":"8"
            ],
            [
                "name":"NÚMERO UNICO DE IDENTIFICACION PERSONAL",
                "id":"NU",
                "code":"9"
            ],
            [
                "name":"PASAPORTE",
                "id":"P",
                "code":"10"
            ],
            [
                "name":"Permiso Especial de Permanencia",
                "id":"PE",
                "code":"11"
            ],
            [
                "name":"REGISTRO CIVIL",
                "id":"R",
                "code":"12"
            ],
            [
                "name":"SALVOCONDUCTO",
                "id":"SC",
                "code":"13"
            ],
            [
                "name":"TARJETA DE IDENTIDAD",
                "id":"T",
                "code":"14"
            ]
        ]
        
        switch searchBy {
        case .Id:
            key="id"
        case .code:
            key="code"
        case .name:
            key="name"
        default:
            break
        }
        
        for item in list {
            if item[key]==search.trimmingCharacters(in: .whitespacesAndNewlines){
                type = TypeDocumentModel(name: item["name"]!, id: item["id"]!, code: item["code"]!)
            }
        }
        
        return type
    }
    
    func inflateDocumentType(){
        pickerData.append(TypeDocumentModel(
            name:"CEDULA DE CIUDADANIA",
            id:"C",
            code:"1"
        ))
        pickerData.append(TypeDocumentModel(
            name:"CARNET DIPLOMATICO",
            id:"CD",
            code:"2"
        ))
        pickerData.append(TypeDocumentModel(
            name:"CERTIFICADO NACIDO VIVO",
            id:"CN",
            code:"3"
        ))
        pickerData.append(TypeDocumentModel(
            name:"CEDULA DE EXTRANJERIA",
            id:"E",
            code:"4"
        ))
        pickerData.append(TypeDocumentModel(
            name:"PERSONERIA JURIDICA",
            id:"J",
            code:"5"
        ))
        pickerData.append(TypeDocumentModel(
            name:"MENOR DE EDAD",
            id:"M",
            code:"6"
        ))
        pickerData.append(TypeDocumentModel(
            name:"MENOR SIN IDENTIFICACION",
            id:"MS",
            code:"7"
        ))
        pickerData.append(TypeDocumentModel(
            name:"NIT",
            id:"N",
            code:"8"
        ))
        pickerData.append(TypeDocumentModel(
            name:"NUMERO UNICO DE IDENTIFICACION PERSONAL",
            id:"NU",
            code:"9"
        ))
        pickerData.append(TypeDocumentModel(
            name:"PASAPORTE",
            id:"P",
            code:"10"
        ))
        pickerData.append(TypeDocumentModel(
            name:"Permiso Especial de Permanencia",
            id:"PE",
            code:"11"
        ))
        pickerData.append(TypeDocumentModel(
            name:"REGISTRO CIVIL",
            id:"R",
            code:"12"
        ))
        pickerData.append(TypeDocumentModel(
            name:"SALVOCONDUCTO",
            id:"SC",
            code:"13"
        ))
        pickerData.append(TypeDocumentModel(
            name:"TARJETA DE IDENTIDAD",
            id:"T",
            code:"14"
        ))
    }
    
    func validateEmail(_ email: String) -> Bool {
        
        let emailRegEx  = "^[A-Z0-9a-z._-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let emailResult = emailTest.evaluate(with: email)
        
        return emailResult
        
    }
    
}
