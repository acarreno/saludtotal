//
//  RelatedAppModel.swift
//  Salud-total
//
//  Created by iMac on 11/25/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper

class RelatedAppModel : Mappable {
    var NombreAPP = ""

    var Url_Imagen = ""

    var Url_TiendaAndroid = ""
    var PesoAPP_Android = ""
    var Url_TiendaIOS = ""
    var PesoAPP_IOS = ""
    var Producto =  0
    var Error = ""
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        NombreAPP <- map["nameVarMap"]
        Url_Imagen <- map["Url_Imagen"]
        NombreAPP <- map["NombreAPP"]
        Url_TiendaAndroid <- map["Url_TiendaAndroid"]
        Url_TiendaIOS <- map["Url_TiendaIOS"]
        PesoAPP_IOS <- map["PesoAPP_IOS"]
        Producto <- map["Producto"]
        Error <- map["Error"]
    }
    
    
}
