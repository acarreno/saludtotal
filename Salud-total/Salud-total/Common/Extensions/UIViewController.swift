//
//  UIViewController.swift
//  miClaro
//
//  Created by Jeisson González on 26/06/18.
//  Copyright © 2018 wigilabs. All rights reserved.
//
import Foundation
import UIKit
import ZVProgressHUD
import ObjectMapper
//import SwiftyUserDefaults
//import FirebaseAnalytics
//import ObjectMapper

extension UIViewController{
    
    func showLoading(){
        ZVProgressHUD.maskType = .black
        ZVProgressHUD.show()
    }
    
    func hiddenLoading(){
        ZVProgressHUD.dismiss()
    }
    func hideBack(){
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.backItem?.title=""
        self.navigationController?.navigationItem.backBarButtonItem?.title=""
        let BarButtonItemAppearance = UIBarButtonItem.appearance()
        BarButtonItemAppearance.setTitleTextAttributes([.foregroundColor: UIColor.clear], for: .normal)
    }
    func sessionManager(loading: Bool, completion: @escaping ()->()){
        if validLogin() {
            if loading{
                showLoading()
            }
            
            
            let url = "\(URlGet().ValidaToken)?token=\(retToken())&aplicativo=APP"
            let defaults = UserDefaults.standard
            let vc = LoginViewController.instantiate()
            
            servGet(url: url, isHeaders: true) { (finish, res) in
                if finish {
                    let response = res as! [String: Any]
                    let valido = response["Valido"] as! Int
                    if valido == 1 {
                        completion()
                    }else{
                        defaults.removeObject(forKey: "tokenAutorization")
                        defaults.removeObject(forKey: "token")
                        let name=self.nibName
                        if name != "uMY-5i-3OA-view-xZ3-Ar-Wgt"{
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                       self.hiddenLoading()
                    }
                }else{
                    defaults.removeObject(forKey: "tokenAutorization")
                    defaults.removeObject(forKey: "token")
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                if loading{
                    self.hiddenLoading()
                }
            }
            
        }
    }
    
    func addHeader(){
       
        self.navigationController?.setNavigationBarHidden(false, animated: false   )
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.backItem?.title=""
        self.navigationController?.navigationBar.tintColor=UIColor.white
        self.navigationController?.navigationBar.shadowImage = UIImage()
        let rightBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "stGeneralSaludtotallogo"), style: .done, target: self, action:nil)
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        
        self.navigationController?.navigationItem.rightBarButtonItem?.image=UIImage(named: "stGeneralSaludtotallogo")
        let gradient = CAGradientLayer()
        var bounds = self.navigationController!.navigationBar.bounds
        bounds.size.height += UIApplication.shared.statusBarFrame.size.height
        gradient.frame = bounds
        gradient.colors = [UIColor(red: 0/255, green: 128/255, blue: 227/255, alpha: 1).cgColor, UIColor(red: 57/255, green: 99/255, blue: 159/255, alpha: 1).cgColor]
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 0, y: 1)

        if let image = getImageFrom(gradientLayer: gradient) {
            self.navigationController!.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
        }
       
        //self.navigationController?.view.addSubview(HeaderViewController.instantiate().view)
    
        //self.view.addSubview(HeaderViewController.instantiate().view)
    }
    func getImageFrom(gradientLayer:CAGradientLayer) -> UIImage? {
        var gradientImage:UIImage?
        UIGraphicsBeginImageContext(gradientLayer.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            gradientLayer.render(in: context)
            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
        }
        UIGraphicsEndImageContext()
        return gradientImage
    }
    func alert(msj:String,callback:@escaping ()->()){
        
        JACSAlertViewController.open(navController:self.navigationController!,text: msj) { (res) in
            callback()
        }
    }
    func alert(msj:String){
        
        JACSAlertViewController.open(navController:self.navigationController!,text: msj) { (res) in
            
        }
    }
    func confirm(msj:String,callback:@escaping ()->()){
        JACSConfirmViewController.open(navController: self.navigationController!, text: msj) { () in
            callback()
        }
        
    }
    func servGet(url:String,callback:@escaping (Bool,Any)->()){
        let services=Services()
        services.get(url:url){
            (respond,finish)in
            callback(finish,respond)
        }
    }
    
    func servPost(url:String,params:[String:Any],isHeaders:Bool,callback:@escaping (Bool,Any)->()){
        let services=Services()
        services.post(url: url, parameters: params,isHeaders: true) { (res, finish) in
            callback(finish,res)
        }
        
    }
    
    func servGet(url:String,isHeaders:Bool,callback:@escaping (Bool,Any)->()){
        let services=Services()
        services.get(url:url,isHeaders:isHeaders ){
            (respond,finish)in
            callback(finish,respond)
        }
    }
    func getCitiesDefaults()->[City]{
        var cities = [City]()
        if let citiesString = UserDefaults.standard.string(forKey: "listCities"){
            let listCities=citiesString.convertToList()
            cities=Mapper<City>().mapArray(JSONObject: (listCities) )!
            
        }
       return cities
    }
    
    func findCityBy(by:cityAttr,word:String)->City{
        var city = City(JSON:["":""])!
        var key=""
        switch by {
        case .CiudadId:
            key="CiudadId"
        case .CodDepartamento:
            key="CodDepartamento"
        case .cobertura:
            key="cobertura"
        case .CodMunicipio:
            key="CodMunicipio"
        case .Departamento:
            key="Departamento"
        case .EpsSucursalId:
            key="EpsSucursalId"
        case .Nombre:
            key="Nombre"
        case .NombreMostrar:
            key="NombreMostrar"
        case .TipoUbicacionId:
            key="TipoUbicacionId"
        
        }
        if let citiesString = UserDefaults.standard.string(forKey: "listCities"){
            let listCities=citiesString.convertToList()
            for item in listCities!{
                let compare=String(describing:item[key]!)
                if compare == word{
                    city=City(JSON:item)!
                }
            }
            
        }
        return city
    }
    
    func validLogin()->Bool{
        var haveToken=false
        if let token = UserDefaults.standard.string(forKey: "token"), token != "" {
            haveToken=true
        }
        return haveToken
    }
    func retToken()->String{
       
        let token=UserDefaults.standard.string(forKey: "token")
        
        return token!
    }
    func retAtorizationToken()->String{
        
        let token=UserDefaults.standard.string(forKey: "tokenAutorization")
        
        return token!
    }
    
    func retUser()->(error:Bool,user:UserModel){
        var userM = UserModel(JSON:["":""])!
        var error = false
        if let user=UserDefaults.standard.dictionary(forKey: "user"){
            userM=UserModel(JSON: user)!
        }else{
           error=true
        }
        
        return (error:error,user:userM)
    }
    
    func retBeneficiary()->BeneficiaryModel{
        let beneficiary=UserDefaults.standard.dictionary(forKey: "beneficiary")
        return BeneficiaryModel(JSON: beneficiary!)!
    }
    
    func retTypeDoc(searchBy:SearchBy,search:String)->TypeDocumentModel?{
        var key="id"
        var type=TypeDocumentModel()
        let list=[
            [
                "name":"CEDULA DE CIUDADANIA",
                "id":"C",
                "code":"1"
            ],
            [
                "name":"CARNET DIPLOMATICO",
                "id":"CD",
                "code":"2"
            ],
            [
                "name":"CERTIFICADO NACIDO VIVO",
                "id":"CN",
                "code":"3"
            ],
            [
                "name":"CEDULA DE EXTRANJERIA",
                "id":"E",
                "code":"4"
            ],
            [
                "name":"PERSONERIA JURIDICA",
                "id":"J",
                "code":"5"
            ],
            [
                "name":"MENOR DE EDAD",
                "id":"M",
                "code":"6"
            ],
            [
                "name":"MENOR SIN IDENTIFICACION",
                "id":"MS",
                "code":"7"
            ],
            [
                "name":"NIT",
                "id":"N",
                "code":"8"
            ],
            [
                "name":"NÚMERO UNICO DE IDENTIFICACION PERSONAL",
                "id":"NU",
                "code":"9"
            ],
            [
                "name":"PASAPORTE",
                "id":"P",
                "code":"10"
            ],
            [
                "name":"Permiso Especial de Permanencia",
                "id":"PE",
                "code":"11"
            ],
            [
                "name":"REGISTRO CIVIL",
                "id":"R",
                "code":"12"
            ],
            [
                "name":"SALVOCONDUCTO",
                "id":"SC",
                "code":"13"
            ],
            [
                "name":"TARJETA DE IDENTIDAD",
                "id":"T",
                "code":"14"
            ]
        ]
        
        switch searchBy {
        case .Id:
            key="id"
        case .code:
            key="code"
        case .name:
            key="name"
        
        }
        
        for item in list {
            if item[key]==search{
                type = TypeDocumentModel(name: item["name"]!, id: item["id"]!, code: item["code"]!)
            }
        }
        return type
    }
    func loginApp(callback:@escaping (Bool,String)->()){
        
            let url="\(URlGet().login)?PerfilUsuario=AFI&TipoEmpleadorId=N&EmpleadorId=800149453&TipoDocumentoId=C&DocumentoId=999&Clave=saludtotal&aplicativo=APP"
            servGet(url: url) { (finish, res) in
                debugPrint(res)
                if finish{
                    if let response=LoginModel(JSON: res as! [String:Any]){
                        if response.Valido{
                            callback(true,response.Token)
                        }else{
                            callback(false,response.Error)
                        }
                    }
                    
                }
                else{
                    callback(false,res as!String)
                }
                
                
            }
        
    }
    func closeSeSSion(){
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
            
        }
    }
    /*var cuenta_activa: CuentaModel{
        //let cuenta_activa = CuentaModel(JSON: (Defaults[.cuentaActiva] as! [String:Any]) )!
        
        if Defaults.hasKey(.cuentaActiva) {
            let cuenta_activa = CuentaModel(JSON: (Defaults[.cuentaActiva] as! [String:Any]) )!
            return cuenta_activa
        }else{
            let cuenta_activa = CuentaModel(JSON: ["AccountId":""] )!
            return cuenta_activa
        }
    }
    var cuenta_activa_empresas: CuentaModel{
        //let cuenta_activa = CuentaModel(JSON: (Defaults[.cuentaActiva] as! [String:Any]) )!
        
        if Defaults.hasKey(.cuentaActivaEmpresas) {
            let cuenta_activa = CuentaModel(JSON: (Defaults[.cuentaActivaEmpresas] as! [String:Any]) )!
            return cuenta_activa
        }else{
            let cuenta_activa = CuentaModel(JSON: ["AccountId":""] )!
            return cuenta_activa
        }
    }
    
    var cuenta_activa_he: CuentaModel{
        if Defaults.hasKey(.cuentaActivaHE) {
            let cuenta_activa = CuentaModel(JSON: (Defaults[.cuentaActivaHE] as! [String:Any]) )!
            return cuenta_activa
        }else{
            let cuenta_activa = CuentaModel(JSON: ["AccountId":""] )!
            return cuenta_activa
        }
    }
    
    var usuario_activo: UsuarioModel{
        
        if Defaults.hasKey(.usuarioActivo) {
            let usuario_activo = UsuarioModel(JSON: (Defaults[.usuarioActivo] as! [String:Any]) )!
            return usuario_activo
        }else{
            let usuario_activo = UsuarioModel(JSON: ["UserProfileID":""] )!
            return usuario_activo
        }
        
    }
    
    var usuario_activo_empresas: UsuarioActivoEmpresas{
        //let cuenta_activa = CuentaModel(JSON: (Defaults[.cuentaActiva] as! [String:Any]) )!
        
        if Defaults.hasKey(.cuentaActiva) {
            let usuarioActivo = UsuarioActivoEmpresas(JSON: (Defaults[.usuarioActivoEmpresas] as! [String:Any]) )!
            return usuarioActivo
        }else{
            let usuarioActivo = UsuarioActivoEmpresas(JSON: ["UserProfileID":""] )!
            return usuarioActivo
        }
    }
    
    func esCuentaAsociada(_ cuenta:String) -> Bool{
        
        var container=[CuentaModel]()
        
        if Defaults.hasKey(.cuentasPostpago) {
            container += Mapper<CuentaModel>().mapArray(JSONObject: (Defaults[.cuentasPostpago] as! [[String:Any]]) )!
        }
        
        if Defaults.hasKey(.cuentasPrepago) {
            container += Mapper<CuentaModel>().mapArray(JSONObject: (Defaults[.cuentasPrepago] as! [[String:Any]]) )!
        }
        
        for item in container {
            if item.AccountId == cuenta {
                return true
            }
        }
        
        return false
    }
    
    func getTagUrl(_ tag:String) -> String {
        
        return sidePanelController.getTagUrlSwift(tag)
    }
    func openUrl(nav:UINavigationController,url:String,showLogo:Bool){
       
        sidePanelController.openUrlSwift(nav, url: url, conLogo: true)
    }
    
    
    
    func openControllerPopup(_ controller : UIViewController){
        
        controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        controller.providesPresentationContextTransitionStyle = true
        controller.definesPresentationContext = true
        controller.modalPresentationStyle = .overCurrentContext
        self.present(controller, animated: true, completion: nil)
    }
    
    func abrirURL(_ url:String){
        let mainstoryboard = UIStoryboard (name: "Main", bundle: Bundle.main)
        let view = mainstoryboard.instantiateViewController(withIdentifier: "NavegadorWebView") as! NavegadorWebViewViewController
        view.url=url;
        view.ocultarLogo=true;
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    func abrirPortalPagos(_ data:[String:Any]){
        
        do {
            
            var dataAnalytics:[String:Any] = data["data"] as! [String:Any]
            dataAnalytics["url"] = data["url"]
            marcar("web_view_con_formulario", withParams: dataAnalytics)
            
            let jsonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
            
            //Convert back to string. Usually only do this for debugging
            if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                print(JSONString)
                let base64 = JSONString.toBase64()
                print(base64)
                
                let data = [
                    "data":JSONString
                ]
                
                Services.postSwift(const.serv_GatewayLog, parameters: data) { (finished, res) in
                    print(res ?? "no se envio info al log de pagos")
                }
                
                let urlpath = Bundle.main.url(forResource: "utils", withExtension: "bundle")
                
                let url = ("proxy.html?data=\(base64)")
                let urlHtml = "\(urlpath!)" + "\(url)"
                print(urlHtml)
                //self.navigationController?.pushViewController(ModulosEmpresas.JACSWebView(titulo: "Portal Pagos", url: urlHtml), animated: true)
                
                JASidePanelController().openUrl(self.navigationController, url: urlHtml, conLogo: false)
            }
        }catch {
            alert("No se puede realizar la transacción en este momento.")
        }
    }
    func abrirPortalPagosEmp(_ data:[String:Any]){
        
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
            
            //Convert back to string. Usually only do this for debugging
            if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                print(JSONString)
                let base64 = JSONString.toBase64()
                print(base64)
                
                let urlpath = Bundle.main.url(forResource: "utils", withExtension: "bundle")
                
                let url = ("proxy.html?data=\(base64)")
                let urlHtml = "\(urlpath!)" + "\(url)"
                print(urlHtml)
               self.navigationController?.pushViewController(ModulosEmpresas.JACSWebView(titulo: "Portal Pagos", url: urlHtml), animated: true)
                
                //JASidePanelController().openUrl(self.navigationController, url: urlHtml, conLogo: false)
            }
        }catch {
            alert("No se puede realizar la transacción en este momento.")
        }
    }
    
    func convertirTipoDocumento(tipo:String) -> String{
        
        switch tipo {
        case "1":
            return "CC"
        case "2":
            return "CE"
        case "3":
            return "PP"
        case "4":
            return "CD"
        case "5":
            return "NIT"
        default:
            return ""
        }
    }
    
    var screen_width : CGFloat {
        return UIScreen.main.bounds.width
    }
    
    var screen_height : CGFloat{
        return UIScreen.main.bounds.height
    }
    
    func hideBackButton(){
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem.init(title: "", style: .plain, target: nil, action: nil)
    }
    
    func showLoading(_ completion: (() -> Void)? = nil){
        
        let obj = [
            "parent":self,
            "tipo":const.alert_loading
            ] as [String : Any]
        
        ModulosApp.openPopUp(obj:obj)
        if let callback = completion {
            callback()
        }
    }
    func promt(texto:String,completion: (() -> Void)? = nil){
        
        
        DispatchQueue.main.async{
            let obj = [
                "parent":self,
                "tipo":const.alert_promt,
                "texto":texto
                ] as [String : Any]
            
            ModulosApp.openPopUp(obj:obj)
            if let callback = completion {
                callback()
            }
        }
    }
    func hideLoading(_ completion: (() -> Void)? = nil){
        
        DispatchQueue.main.async{
            ModulosApp.close(completion)
        }
    }
    
    
    func showSheetHour(_ callback: @escaping ((_ hora:String) -> Void)){
        
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screen_width - 16, height: 250))
        
        datePicker.datePickerMode = .time
        let loc = Locale(identifier: "es_CO")
        datePicker.locale = loc
        
        let dateChooserAlert = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
        dateChooserAlert.view.addSubview(datePicker)
        dateChooserAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { action in
            // Your actions here if "Done" clicked...
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            //dateFormatter.amSymbol = "AM"
            //dateFormatter.pmSymbol = "PM"
            let selectedDate = dateFormatter.string(from: datePicker.date)
            print(selectedDate)
            
            callback(selectedDate)
            
        }))
        let height: NSLayoutConstraint = NSLayoutConstraint(item: dateChooserAlert.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.1, constant: 300)
        
        dateChooserAlert.view.addConstraint(height)
        self.present(dateChooserAlert, animated: true, completion: nil)
        
        
    }
    
    func showSheetDate(yearMin:Int = -1,yearMax:Int = 0,format:String = "dd/MM/yyyy",_ callback: @escaping ((_ fecha:String) -> Void)){
        
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screen_width - 16, height: 250))
        
        let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
        let currentDate: NSDate = NSDate()
        let components: NSDateComponents = NSDateComponents()
        
        components.year = yearMin
        let minDate: NSDate = gregorian.date(byAdding: components as DateComponents, to: currentDate as Date, options: NSCalendar.Options(rawValue: 0))! as NSDate
        
        components.year = yearMax
        let maxDate: NSDate = gregorian.date(byAdding: components as DateComponents, to: currentDate as Date, options: NSCalendar.Options(rawValue: 0))! as NSDate
        
        datePicker.minimumDate = minDate as Date
        datePicker.maximumDate = maxDate as Date
        
        datePicker.datePickerMode = .date
        let loc = Locale(identifier: "es_CO")
        datePicker.locale = loc
        
        
        let dateChooserAlert = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
        dateChooserAlert.view.addSubview(datePicker)
        dateChooserAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { action in
            // Your actions here if "Done" clicked...
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = format
            let selectedDate = dateFormatter.string(from: datePicker.date)
            print(selectedDate)
            
            callback(selectedDate)
            
        }))
        let height: NSLayoutConstraint = NSLayoutConstraint(item: dateChooserAlert.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.1, constant: 300)
        
        dateChooserAlert.view.addConstraint(height)
        self.present(dateChooserAlert, animated: true, completion: nil)
        
        
    }
    
    func showSheetDateByDay(dayMax:Int = 8,format:String = "yyyy-MM-dd",_ callback: @escaping ((_ fecha:String) -> Void)){
        
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screen_width - 16, height: 250))
        
        let f = Date()
        let maxDateNew = Calendar.current.date(byAdding: .day, value: dayMax, to: f)
        print(maxDateNew as! Date)
        //print(maxDate)
        datePicker.minimumDate = NSDate() as Date
        datePicker.maximumDate = (maxDateNew as! Date)
        
        datePicker.datePickerMode = .date
        let loc = Locale(identifier: "es_CO")
        datePicker.locale = loc
        
        
        let dateChooserAlert = UIAlertController(title: "", message: nil, preferredStyle: .actionSheet)
        dateChooserAlert.view.addSubview(datePicker)
        dateChooserAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { action in
            // Your actions here if "Done" clicked...
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = format
            let selectedDate = dateFormatter.string(from: datePicker.date)
            print(selectedDate)
            
            callback(selectedDate)
            
        }))
        let height: NSLayoutConstraint = NSLayoutConstraint(item: dateChooserAlert.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.1, constant: 300)
        
        dateChooserAlert.view.addConstraint(height)
        self.present(dateChooserAlert, animated: true, completion: nil)
        
        
    }
    
    
    func alert(_ mensaje:String = "", _ callback: (() -> Void)? = nil){
        
        let data = [
            "mensaje": mensaje,
            "popup": const.popup_alert,
            "mostrarLogo":1,
            "parent":self
            ] as [String : Any]
        
        
        DispatchQueue.main.async{
            JASidePanelController().showAlert(data,callbackClose:callback)
        }
        
    }
    
    func confirm(_ mensaje:String = "",callbackError: (() -> Void)? = nil,callbackOK: (() -> Void)? = nil){
        
        let data = [
            "mensaje": mensaje,
            "popup": const.popup_confirm,
            "mostrarLogo":1,
            "txt_confirm":"Aceptar",
            "txt_cancelar":"Cancelar",
            "parent":self
            ] as [String : Any]
        
        DispatchQueue.main.async{
            JASidePanelController().showAlert(data, callbackError: callbackError, callbackOK: callbackOK)
        }
        
        
    }
    
    func marcarPantalla(tag:String){
        Analytics.setScreenName(tag, screenClass: NSStringFromClass(self.classForCoder))
    }
    
    func marcarBoton(tag:String) {
        
        var sendParams:[String:Any] = [:]
        
        if Defaults.hasKey(.usuarioActivo) {
            sendParams["UserProfileID"] = usuario_activo.UserProfileID
        }
        
        if Defaults.hasKey(.cuentaActiva) {
            sendParams["AccountId"] = cuenta_activa.AccountId
            sendParams["LineOfBusiness"] = cuenta_activa.LineOfBusiness
        }
        
        
        Analytics.logEvent(tag, parameters: sendParams)
    }
    
    func marcar(_ tag:String, withParams params:[String:Any] = [:]) {
        
        var sendParams = params
        
        if Defaults.hasKey(.usuarioActivo) {
            sendParams["UserProfileID"] = usuario_activo.UserProfileID
        }
        
        if Defaults.hasKey(.cuentaActiva) {
            sendParams["AccountId"] = cuenta_activa.AccountId
            sendParams["LineOfBusiness"] = cuenta_activa.LineOfBusiness
        }
        
        
        Analytics.logEvent(tag, parameters: sendParams)
    }
    
    func inicioMes() -> String{
        let date = Date()
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        
        let monthText = (month<10) ? "0\(month)" : "\(month)"
        
        return "\(year)-\(monthText)-01"
    }
    
    func fechaActual() -> String{
        let date = Date()
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        
        let monthText = (month<10) ? "0\(month)" : "\(month)"
        let dayText = (day<10) ? "0\(day)" : "\(day)"
        
        
        return "\(year)-\(monthText)-\(dayText)"
    }
    
    func mesActual() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM"
        let strMonth = dateFormatter.string(from: Date())
        return strMonth
    }
    
    func anioActual() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY"
        let strAnio = dateFormatter.string(from: Date())
        return strAnio
    }
    
    func mesAnterior() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM"
        let strMonth = dateFormatter.string(from: Date())
        return strMonth
    }
    
    func callTo(number:String){
        if let url = URL(string: "tel://\(number)") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func setHeader(tipo:String){
        
        navigationController?.setNavigationBarHidden(false, animated: false   )
        navigationController?.navigationBar.backItem?.title=" "
       navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.isTranslucent=false
        self.navigationController?.navigationBar.isOpaque=false
        UIBarButtonItem.appearance()
            .setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Roboto-Bold", size: 12)!],
                                    for: .normal)
       
        switch tipo {
        case constansEmp.rojoConLogo:
            self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.8857421279, green: 0.24863258, blue: 0.1403026879, alpha: 1)
            let logo = UIImage(named: "320_IOSclaro")
            let imageView = UIImageView(image:logo)
            self.navigationItem.titleView = imageView
        case constansEmp.grisConLogo:
            self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.3529411765, green: 0.3529411765, blue: 0.3529411765, alpha: 1)
           let logo = UIImage(named: "320_IOSclaro")
            let imageView = UIImageView(image:logo)
            self.navigationItem.titleView = imageView
        case constansEmp.blancoConLogo:
            self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.2941176471, green: 0.2941176471, blue: 0.2941176471, alpha: 1)
            self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            let logo = UIImage(named: "icoZpMiclarologoNR")
            let imageView = UIImageView(image:logo)
            self.navigationItem.titleView = imageView
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
        case constansEmp.gris:
            self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.3529411765, green: 0.3529411765, blue: 0.3529411765, alpha: 1)
            self.navigationItem.titleView?.isHidden=true
            
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1),NSAttributedString.Key.font: UIFont(name: "Roboto-Bold", size: 16)!]
            self.navigationController?.navigationBar.isTranslucent=false
            self.navigationController?.navigationBar.isOpaque=false
            
        case constansEmp.rojo:
           self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.8857421279, green: 0.24863258, blue: 0.1403026879, alpha: 1)
           self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)]
            self.navigationController?.navigationBar.isTranslucent=false
            self.navigationController?.navigationBar.isOpaque=false
          
        default:
            print("default")
        }
        
    }
    func rightButtomTopBar(title:String?,image:String?,callback:@escaping ()->Void){
        if let imageName = image{
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image:UIImage(named:imageName), action: {
                callback()
            })
        }
        if let titleButton = title{
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: titleButton, action: {
                callback()
            })
        }
    }
    func checkMaxLength(textField: UITextField!, maxLength: Int) {
        if (textField.text!).count > maxLength {
            textField.deleteBackward()
        }
    }
    
    func starTimeout(){
        
        NotificationCenter.default.post(name: .noti_extender_tiempo_sesion, object: nil, userInfo: nil)
    }
    func stopTimeout(){
        
        NotificationCenter.default.post(name: .noti_parar_tiempo_sesion, object: nil, userInfo: nil)
    }
    func isTransac()->Bool{
        if usuario_activo_empresas.roleID == "1" && usuario_activo_empresas.tipoUsuarioID=="2"{
            return true
        }else{
            return false
        }
    }
    func tokenizacion(usuario:UsuarioModel,callback:@escaping (
        _ finalized:Bool,_ res:[String:Any])->Void){
        
        print("Tokenizacion!!....");
        let data = [
            "nombreUsuario":usuario.UserProfileID,
            "clave":usuario.clave
        ];
        
        Services.postSwift(const.serv_tokenizacion, parameters: data) { (finish, res) in
            let response = res as! [String:Any]
            if finish{
                callback(true,response)
            }else{
                callback(false,response)
                
            }
            
            
        }
    } */
}

private var KeyMaxLength: Int = 0

extension UITextField {
    
    @IBInspectable var maxLength: Int {
        get {
            if let length = objc_getAssociatedObject(self, &KeyMaxLength) as? Int {
                return length
            } else {
                return Int.max
            }
        }
        set {
            objc_setAssociatedObject(self, &KeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
            addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
        }
    }
    
    @objc func checkMaxLength(textField: UITextField) {
        guard let prospectiveText = self.text,
            prospectiveText.count > maxLength
            else {
                return
        }
        
        let selection = selectedTextRange
        let maxCharIndex = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        text = prospectiveText.substring(to: maxCharIndex)
        selectedTextRange = selection
    }
    
}
