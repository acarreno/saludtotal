//
//  UIView.swift
//  miClaro
//
//  Created by Jeisson González on 25/06/18.
//  Copyright © 2018 wigilabs. All rights reserved.
//

import Foundation
//import Lottie
import UIKit


@IBDesignable
open class SuperView: UIView {
    
    @IBInspectable open var cornerRadius: CGFloat = 0{
        didSet{
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable open var shadow: Bool = false{
        didSet{
            self.layer.shadowColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.07).cgColor
            self.layer.shadowOffset = CGSize(width:0,height:0)
            self.layer.shadowOpacity = 0.9
            self.layer.shadowRadius = 1.5
            self.layer.masksToBounds = false;
        }
    }
    
    @IBInspectable open var borderColor: UIColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0){
        didSet{
            self.layer.borderColor = borderColor.cgColor
            self.layer.borderWidth = 1
        }
    }
}
@IBDesignable
open class TopBarView: UIView {
    
    @IBInspectable open var imagen: CGFloat = 0{
        didSet{
            self.layer.cornerRadius = imagen
        }
    }
    
    @IBInspectable open var shadow: Bool = false{
        didSet{
            self.layer.shadowColor = UIColor(red: 140.0/255.0, green: 140.0/255.0, blue: 140.0/255.0, alpha: 1.0).cgColor
            self.layer.shadowOffset = CGSize(width:0,height:0)
            self.layer.shadowOpacity = 0.9
            self.layer.shadowRadius = 1.5
            self.layer.masksToBounds = false;
        }
    }
}

enum LINE_POSITION {
    case LINE_POSITION_TOP
    case LINE_POSITION_BOTTOM
}

extension UIView{
    
    func viewShadow(){
        self.layer.shadowColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.07).cgColor
        self.layer.shadowOffset = CGSize(width:0,height:0)
        self.layer.shadowOpacity = 0.9
        self.layer.shadowRadius = 1.5
        self.layer.masksToBounds = false;
    }
    func removeViewShadow(){
       
        self.layer.shadowOpacity = 0
        
    }
    func borderRadius(){
        
        self.layer.cornerRadius = 5
        
    }
    
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.rotation")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values=[M_PI / -50,M_PI/50,M_PI / -50,M_PI/50,M_PI / -50]
        animation.repeatCount = 10000
       
        layer.add(animation, forKey: "transform.rotation")
    }
    
    func noAnimation(){
        let animation = CAKeyframeAnimation(keyPath: "transform.rotation")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values=[0,0,0]
        animation.repeatCount = 1
        
        layer.add(animation, forKey: "transform.rotation")
    }
    
    func slideRightToLeft(){
        UIView.animate(withDuration: 0.5, animations: {
            self.frame.origin.x=CGFloat(0)
        }, completion: nil)
    }
    
    func slideLeftToRight(){
        UIView.animate(withDuration: 0.5, animations: {
            self.frame.origin.x=self.frame.width
        }, completion: nil)
    }
    func fadeIn(){
        UIView.animate(withDuration: 0.5, animations: {
            self.alpha=1
        }, completion: nil)
    }
    func fadeOut(){
        UIView.animate(withDuration: 0.5, animations: {
            self.alpha=0
        }, completion: nil)
    }
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    func loading(){
        
        
        /*for view in self.subviews {
            view.removeFromSuperview()
        }
        
        
        let animationView = AnimationView(name: "cargando_interna")
        
        let with = ((self.frame.size.width > self.frame.size.height) ? self.frame.size.height : self.frame.size.width)
        let heigth = with * 640 / 640
        
        animationView.frame.size.width=with
        animationView.frame.size.height=heigth
        animationView.frame.origin.x = (self.frame.size.width / 2) - (animationView.frame.size.width / 2)
        animationView.frame.origin.y = (self.frame.size.height / 2) - (animationView.frame.size.height / 2)
        animationView.loopMode = .loop
        animationView.play()
        
        self.addSubview(animationView)
        */
    }
    func addLine(position : LINE_POSITION, color: UIColor, width: CGFloat) {
        let lineView = UIView()
        lineView.backgroundColor = color
        lineView.translatesAutoresizingMaskIntoConstraints = false // This is important!
        lineView.frame.size.height=1
        lineView.frame.size.width=width
        lineView.frame.origin.x=(self.frame.width-width)/2
        switch position {
        case .LINE_POSITION_TOP:
           lineView.frame.origin.y=0
            break
        case .LINE_POSITION_BOTTOM:
           lineView.frame.origin.y=self.frame.height-1
           
            break
        }
        self.addSubview(lineView)
    }
}
@IBDesignable
class GradientView: UIView {

    @IBInspectable var startColor:   UIColor = .black { didSet { updateColors() }}
    @IBInspectable var endColor:     UIColor = .white { didSet { updateColors() }}
    @IBInspectable var startLocation: Double =   0.05 { didSet { updateLocations() }}
    @IBInspectable var endLocation:   Double =   0.95 { didSet { updateLocations() }}
    @IBInspectable var horizontalMode:  Bool =  false { didSet { updatePoints() }}
    @IBInspectable var diagonalMode:    Bool =  false { didSet { updatePoints() }}

    override public class var layerClass: AnyClass { return CAGradientLayer.self }

    var gradientLayer: CAGradientLayer { return layer as! CAGradientLayer }

    func updatePoints() {
        if horizontalMode {
            gradientLayer.startPoint = diagonalMode ? .init(x: 1, y: 0) : .init(x: 0, y: 0.5)
            gradientLayer.endPoint   = diagonalMode ? .init(x: 0, y: 1) : .init(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonalMode ? .init(x: 0, y: 0) : .init(x: 0.5, y: 0)
            gradientLayer.endPoint   = diagonalMode ? .init(x: 1, y: 1) : .init(x: 0.5, y: 1)
        }
    }
    func updateLocations() {
        gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
    }
    func updateColors() {
        gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
    }
    override public func layoutSubviews() {
        super.layoutSubviews()
        updatePoints()
        updateLocations()
        updateColors()
    }
}

