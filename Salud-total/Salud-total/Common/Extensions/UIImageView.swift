//
//  UIImageView.swift
//  miClaro
//
//  Created by IMAC on 11/14/18.
//  Copyright © 2018 wigilabs. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import Lottie
import UIKit

extension UIImageView{
    
    @objc func loadImage(_ url:String){
        
        self.image = nil
        
        for subview in self.subviews {
            subview.removeFromSuperview()
        }
        
        let animationView = AnimationView(name: "material_loader")
        self.addSubview(animationView)
        
        let with = ((self.frame.size.width > self.frame.size.height) ? self.frame.size.height : self.frame.size.width) - 2
        let heigth = with * 300 / 400
        
        animationView.frame.size.width=with
        animationView.frame.size.height=heigth
        animationView.frame.origin.x = (self.frame.size.width / 2) - (animationView.frame.size.width / 2)
        animationView.frame.origin.y = (self.frame.size.height / 2) - (animationView.frame.size.height / 2)
        animationView.loopMode = .loop
        animationView.play()
        
        if url != "" {
            let urlRequest = URLRequest(url: URL(string: url)!)
            Alamofire.request(urlRequest).responseImage { response in
                //debugPrint(response)
                
                if let image = response.result.value {
                    animationView.removeFromSuperview()
                    self.image = image
                    print(image.size)
                }
            }
        }
        
    }
    
    
    @objc func loadImageFixed(_ url:String){
        
        self.image = nil
        
        for subview in self.subviews {
            subview.removeFromSuperview()
        }
        
        let animationView = AnimationView(name: "material_loader")
        self.addSubview(animationView)
        
        let with = ((self.frame.size.width > self.frame.size.height) ? self.frame.size.height : self.frame.size.width) - 2
        let heigth = with * 300 / 400
        
        animationView.frame.size.width=with
        animationView.frame.size.height=heigth
        animationView.frame.origin.x = (self.frame.size.width / 2) - (animationView.frame.size.width / 2)
        animationView.frame.origin.y = (self.frame.size.height / 2) - (animationView.frame.size.height / 2)
        animationView.loopMode = .loop
        animationView.play()
        
        if url != "" {
            let urlRequest = URLRequest(url: URL(string: url)!)
            Alamofire.request(urlRequest).responseImage { response in
                //debugPrint(response)
                
                if let image = response.result.value {
                    animationView.removeFromSuperview()
                    let imageFixed=image.fixedOrientation()
                    self.image = imageFixed
                    
                }
            }
        }
        
    }
    func convertBase64ToImage(_ imageString: String) {
        
        let imageData = Data(base64Encoded: imageString, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
        self.image = UIImage(data: imageData)!
       
    }
}
