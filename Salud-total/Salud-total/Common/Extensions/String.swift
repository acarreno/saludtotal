//
//  String.swift
//  Salud-total
//
//  Created by iMac on 11/14/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    func isValidEmail() -> Bool {
        let emailStr=self
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    func convertToList() -> [[String: Any]]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any]]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    func toPriceNumber() -> String{
        
        var text = self
        text = text.replacingOccurrences(of: ",", with: ".")
        
        let formater = NumberFormatter()
        formater.groupingSeparator = "."
        formater.decimalSeparator = ","
        formater.usesGroupingSeparator = true
        formater.usesSignificantDigits = false
        formater.maximumFractionDigits = 0
        formater.numberStyle = .decimal
        return "$"+formater.string(from: Double( (text != "") ? text : "0" )! as NSNumber )!
        
    }
}
