//
//  InitViewModel.swift
//  Salud-total
//
//  Created by iMac on 11/5/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit
import ObjectMapper
class InitViewModel:UIViewController{
    
    struct Cells {
        let button = "button"
        let banner = "banner"
    }
    
    struct Ids {
        let banner="banner"
        let register="register"
        let login="login"
    }
    
    
    typealias data = (title:String,imag:UIImage,subtitle:String)
    
    typealias CellModel =  (cell:String,id:String,height:CGFloat,obj:data)
    
    let cells=Cells()
    let ids=Ids()
    var container=[CellModel]()
    var heightView:Double=0
    var banner=""
    var urlNews=""
    
    func login(callback:@escaping (Bool,String)->()){
        let url="\(URlGet().login)?PerfilUsuario=AFI&TipoEmpleadorId=N&EmpleadorId=800149453&TipoDocumentoId=C&DocumentoId=999&Clave=saludtotal&aplicativo=APP"
        servGet(url: url) { (finish, res) in
            debugPrint(res)
            if finish{
                if let response=LoginModel(JSON: res as! [String:Any]){
                    if response.Valido{
                        //UserDefaults.standard.set(response.Token, forKey: "token")
                        
                        self.getToken(token:response.Token){(finish,res)in
                            if finish{
                                
                                callback(true,res)
                            }else{
                                callback(false,response.Token)
                            }
                        }
                    }else{
                        callback(false,response.Error)
                    }
                    
                }
                
            }
            else{
                callback(false,res as!String)
            }
            
            
        }
    }
    func getBanner(token:String,callback:@escaping (Bool,String)->()){
        servGet(url:  URlGet().ObteneImagen, isHeaders: true) { (finish, res) in
            if finish{
                if let response = MultimediaModel(JSON: (res as! [String:Any])){
                    callback(true,response.Imagen)
                    self.banner = response.Imagen
                    self.urlNews = response.Url_Noticias
                    UserDefaults.standard.set(self.urlNews, forKey: "newsUrl")
                }
            }else{
                callback(false,"Error interno")
            }
           //callback(true,res)
        }
        
    }
    
    func getToken(token:String,callback:@escaping (Bool,String)->()){
        let url = "\(URlGet().GetTokenApps)?Token=\(token)&Origen=APP"
        servGet(url: url) { (finish, res) in
            if finish{
                if let response=LoginModel(JSON: res as! [String:Any]){
                    if response.Valido{
                        //UserDefaults.standard.set(response.Token, forKey: "token")
                        UserDefaults.standard.set(response.Token, forKey: "tokenAutorization")
                        callback(true,response.Token)
                        self.getCities(){res in
                            self.alert(msj: res, callback: {
                                self.navigationController?.popViewController(animated: true)
                            })
                        }
                    }else{
                        
                        callback(false,response.Error)
                    }
                    
                }
                
            }
            else{
                callback(false,res as!String)
            }
            debugPrint(res)
        }
    }
    func getCities(callback:@escaping (String)->()) {
        let url=URlGet().TraerCiudadesPac
        servGet(url:url, isHeaders: true) { (finish, res) in
            if finish{
                if res is [[String:Any]]{
                    let list=res as! [[String:Any]]
                    let cities=Mapper<City>().mapArray(JSONObject: (list) )!
                    //UserDefaults.standard.set(res, forKey: "listCities")
                    UserDefaults.standard.setValue(cities.toJSONString(), forKey: "listCities")
                }else{
                   callback("Error en el servicio de ciudades")
                }
                
                debugPrint(res)
                
            }
        }
    }
    func inflateTable(){
        
        container.append(
            (
                cell:cells.banner,
                id:ids.banner,
                height:0,
                obj:(
                    title:"",
                    imag:UIImage(named:"bannerAppst")!,
                    subtitle:""
                )
            )
        )
        container.append(
            (
                cell:cells.button,
                id:ids.login,
                height:90,
                obj:(
                     title:"INICIA SESIÓN",
                     imag:UIImage(named:"stHomeInciarsesion")!,
                     subtitle:"Si ya cuentas con usuario y contraseña."
                )
            )
        )
        container.append(
            (
                cell:cells.button,
                id:ids.register,
                height:90,
                obj:(
                    title:"REGÍSTRATE",
                    imag:UIImage(named:"stHomeRegistrate")!,
                    subtitle:"Si eres nuevo afiliado o aún no cuentas con usuario y contraseña."
                )
            )
        )
        
        
    }
    
}
