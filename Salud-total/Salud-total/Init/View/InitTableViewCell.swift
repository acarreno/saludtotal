//
//  InitTableViewCell.swift
//  Salud-total
//
//  Created by iMac on 11/5/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class InitTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var contentButton: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
