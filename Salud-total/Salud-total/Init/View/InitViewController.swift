//
//  InitViewController.swift
//  SaludTotal
//
//  Created by iMac on 10/31/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class InitViewController: UIViewController {

    @IBOutlet weak var table: UITableView!
    
    let vm=InitViewModel()
    
    override func viewWillAppear(_ animated: Bool){
        self.navigationController?.isNavigationBarHidden=true
        
        hideBack()
    }
    override func viewDidAppear(_ animated: Bool) {
        UserDefaults.standard.removeObject(forKey: "tokenAutorization")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        table.separatorStyle = .none
        
        //important
        //self.navigationController?.pushViewController(AutorizationsTapViewController.instantiate(), animated: true)
        
        vm.inflateTable()
        vm.heightView=Double(view.frame.height)
        showLoading()
        vm.login { (finish, res) in
            if finish{
                self.vm.getBanner(token: res, callback: { (finish, res) in
                    self.hiddenLoading()
                    if finish{
                        
                        self.table.reloadData()
                    }else{
                        self.alert( msj: res, callback: {
                            print("close")
                        })
                    }
                    
                })
                debugPrint(res)
            }else{
                self.alert( msj: res, callback: {
                    print("close")
                })
            }
        }
        // Do any additional setup after loading the view.
    }
    
   
    @IBAction func tapAffi(_ sender: Any) {
        goToAffiliate()
    }
    
    @IBAction func tapApps(_ sender: Any) {
        goToApps()
    }
    
    @IBAction func TapNews(_ sender: Any) {
        goToNews()
    }
    @IBAction func tapDirectory(_ sender: Any) {
        goToDirectory()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension InitViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item=vm.container[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: item.cell, for: indexPath) as! InitTableViewCell
        
        switch item.cell {
        
        case vm.cells.banner:
            if vm.banner != ""{
                cell.imgBanner.convertBase64ToImage(vm.banner)
            }
            
            
        case vm.cells.button:
            
            cell.lblTitle.text=item.obj.title
            cell.lblSubTitle.text=item.obj.subtitle
            cell.imgIcon.image=item.obj.imag
            cell.contentButton.viewShadow()
            
        default:
        
            break
        
        }
        
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm.container.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item=vm.container[indexPath.row]
        switch item.id {
        case vm.ids.login:
            goToLogin()
        case vm.ids.register:
            goToRegister()
        default:
            break
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item=vm.container[indexPath.row]
        if item.cell==vm.cells.banner{
            vm.container[indexPath.row].height=CGFloat(Int(view.frame.height*0.49))
        }
        return vm.container[indexPath.row].height
    }
    
}
