//
//  routing.swift
//  Salud-total
//
//  Created by iMac on 11/5/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

extension InitViewController{
    static func instantiate() -> InitViewController {
        let storyboard = UIStoryboard(name: "Init", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! InitViewController
        return vc
    }
    static func navigation() -> UINavigationController {
        let storyboard = UIStoryboard(name: "Init", bundle: Bundle.main)
        let navigationController:UINavigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        return navigationController
    }
    func goToLogin(){
        self.navigationController?.pushViewController(LoginViewController.instantiate(), animated: true) //InitViewController.self.navigation().pushViewController(LoginViewController.instantiate(), animated: true)
    }
    func goToRegister(){
        self.navigationController?.pushViewController(RegisterViewController.instantiate(), animated: true) //InitViewController.self.navigation().pushViewController(LoginViewController.instantiate(), animated: true)
    }
    func goToAffiliate(){
        self.navigationController?.pushViewController(AffiliateViewController.instantiate(), animated: true)
        
    }
    func goToApps(){
        self.navigationController?.pushViewController(AppsViewController.instantiate(), animated: true)
    }
    func goToDirectory(){
        self.navigationController?.pushViewController(DirectoryFilterViewController.instantiate(), animated: true)
    }
    func goToNews() {
        let vc=WebViewViewController.instantiate()
        vc.vm.title="NOTICIAS"
        vc.vm.url=vm.urlNews
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
