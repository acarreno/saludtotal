//
//  BannerModel.swift
//  Salud-total
//
//  Created by iMac on 11/29/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper

class MultimediaModel : Mappable {
    
    var Imagen=""
    var Version = 1
    var Producto = 1
    var Url_Noticias = ""
    var Url_Chat = ""
    var Error = ""
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        Imagen <- map["Imagen"]
        Version <- map["Version"]
        Producto <- map["Producto"]
        Url_Noticias <- map["Url_Noticias"]
        Url_Chat <- map["Url_Chat"]
        Error <- map["Error"]
    }
    
    
}
