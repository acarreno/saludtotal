//
//  AffiliateDataTabViewModel.swift
//  Salud-total
//
//  Created by iMac on 12/9/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

class AffiliateDataTabViewModel:UIViewController  {
    
    var beneficiary=BeneficiaryModel(JSON: ["":""])!
    var manager=EditInformationAffiliateManager()
    var listTowns=[TownModel]()
    
    
    
    func initModel(susses:@escaping ()->(),failed:@escaping (String)->()){
        
        beneficiary=retBeneficiary()
        manager.getTowns(susses: { (res) in
            self.listTowns=res
            susses()
        }) { (res) in
            failed(res)
        }
    }
    
}
