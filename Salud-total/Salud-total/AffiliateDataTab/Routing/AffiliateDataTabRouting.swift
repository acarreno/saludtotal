//
//  AffiliateDataTab.swift
//  Salud-total
//
//  Created by iMac on 12/9/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//


import Foundation
import UIKit

extension AffiliateDataTabViewController{
    static func instantiate() -> AffiliateDataTabViewController {
        let storyboard = UIStoryboard(name: "AffiliateDataTab", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! AffiliateDataTabViewController
        return vc
    }
    
}
