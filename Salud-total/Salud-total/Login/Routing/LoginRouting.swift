//
//  LoginRouting.swift
//  Salud-total
//
//  Created by iMac on 11/6/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

extension LoginViewController{
    
    static func instantiate() -> LoginViewController {
        
        let storyboard = UIStoryboard(name: "Login", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! LoginViewController
        return vc
    
    }
    
    func goToHome(){
        
        self.navigationController?.pushViewController(HomeViewController.instantiate(), animated: true)
    }
    
    func gotoForgotMyPassword(){
        self.navigationController?.pushViewController(ForgotMyPasswordViewController.instantiate(), animated: true)
    }
}
