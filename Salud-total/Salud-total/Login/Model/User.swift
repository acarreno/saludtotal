//
//  User.swift
//  Salud-total
//
//  Created by Andrey Carreño on 11/18/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import ObjectMapper

class UserModel: Mappable {
    var DocumentoId = ""
    var type=""
    var codeDocument=""
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        DocumentoId <- map["DocumentoId"]
        type <- map["type"]
        codeDocument <- map["codeDocument"]
    }
}
