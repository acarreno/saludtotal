//
//  TypeDocumentModel.swift
//  Salud-total
//
//  Created by Andrey Carreño on 11/17/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation


struct TypeDocumentModel {
    var name = ""
    var id = ""
    var code = ""
}
