//
//  LoginViewModel.swift
//  Salud-total
//
//  Created by iMac on 11/7/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

class LoginViewModel:UIViewController {
    
    
    var typeSelect=""
    var documentCode = ""
    var pickerData=[TypeDocumentModel]()
    var isShowPass=false
    
    func getLogin(document:String,pass:String,callback:@escaping (Bool,String)->()){
        let url="\(URlGet().login)?PerfilUsuario=AFI&TipoEmpleadorId=N&EmpleadorId=\(document)&TipoDocumentoId=\(typeSelect)&DocumentoId=999&Clave=\(pass)&aplicativo=APP"
        servGet(url: url) { (finish, res) in
            debugPrint(res)
            if finish{
                if let response=LoginModel(JSON: res as! [String:Any]){
                    if response.Valido{
                        
                        let user=["DocumentoId":document,"type":self.typeSelect,"codeDocument":self.documentCode]
                        UserDefaults.standard.set(user, forKey: "user")
                        UserDefaults.standard.set(response.Token, forKey: "token")
                        
                        callback(true,"")
                    }else{
                        callback(false,response.Error)
                        
                        
                    }
                    
                }
                
            }
            else{
                callback(false,res as!String)
            }
           
        
        }
    }
    
    func inflatePicker(){
        pickerData.append(TypeDocumentModel(
            name:"Seleccionar",
            id:"",
            code:""
        ))

        pickerData.append(TypeDocumentModel(
            name:"CEDULA DE CIUDADANIA",
            id:"C",
            code:"1"
        ))
        pickerData.append(TypeDocumentModel(
            name:"CARNET DIPLOMATICO",
            id:"CD",
            code:"2"
        ))
        pickerData.append(TypeDocumentModel(
            name:"CERTIFICADO NACIDO VIVO",
            id:"CN",
            code:"3"
        ))
        pickerData.append(TypeDocumentModel(
            name:"CEDULA DE EXTRANJERIA",
            id:"E",
            code:"4"
        ))
        pickerData.append(TypeDocumentModel(
            name:"PERSONERIA JURIDICA",
            id:"J",
            code:"5"
        ))
        pickerData.append(TypeDocumentModel(
            name:"MENOR DE EDAD",
            id:"M",
            code:"6"
        ))
        pickerData.append(TypeDocumentModel(
            name:"MENOR SIN IDENTIFICACION",
            id:"MS",
            code:"7"
        ))
        pickerData.append(TypeDocumentModel(
            name:"NIT",
            id:"N",
            code:"8"
        ))
        pickerData.append(TypeDocumentModel(
            name:"NUMERO UNICO DE IDENTIFICACION PERSONAL",
            id:"NU",
            code:"9"
        ))
        pickerData.append(TypeDocumentModel(
            name:"PASAPORTE",
            id:"P",
            code:"10"
        ))
        pickerData.append(TypeDocumentModel(
            name:"Permiso Especial de Permanencia",
            id:"PE",
            code:"11"
        ))
        pickerData.append(TypeDocumentModel(
            name:"REGISTRO CIVIL",
            id:"R",
            code:"12"
        ))
        pickerData.append(TypeDocumentModel(
            name:"SALVOCONDUCTO",
            id:"SC",
            code:"13"
        ))
        pickerData.append(TypeDocumentModel(
            name:"TARJETA DE IDENTIDAD",
            id:"T",
            code:"14"
        ))
    }
    
}
