//
//  LoginViewController.swift
//  Salud-total
//
//  Created by iMac on 11/6/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit
import FSnapChatLoading

class LoginViewController: UIViewController {

    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var viewContent: SuperView!
    @IBOutlet weak var txtIdentification: UITextField!
    @IBOutlet weak var txtPass: UITextField!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var btnShowPass: UIButton!
    
    let vm=LoginViewModel()
    let loadingView = FSnapChatLoadingView()
    
    override func viewWillAppear(_ animated: Bool) {
       self.navigationController?.navigationBar.isHidden=false
        hideBack()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /*if validLogin(){
            goToHome()
        }*/
        //vm.getSession()
       
        sessionManager(loading: true) {
            
            self.goToHome()
        }
        picker.isHidden=true
        addHeader()
        vm.inflatePicker()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func initSess(_ sender: Any) {
        
        if (validForm()){
            showLoading()
            vm.getLogin(document: txtIdentification.text!,pass: txtPass.text!) { (finish, res) in
                self.hiddenLoading()
                
                if finish{
                    self.goToHome()
                }else{
                    self.alert(msj: res, callback: {
                        print("")
                    })
                }
            }
        }
        
        
    }
    
    func validForm()->Bool{
        var ret=true
        
        if txtIdentification.text==""{
            alert( msj: "Ingresa tu número de identificación") {
                print("alert ret")
            }
            ret=false
        }
        if txtPass.text==""{
            alert( msj: "Ingresa tu contraseña") {
                print("alert ret")
            }
            ret=false
        }
        if vm.typeSelect == ""{
            alert( msj: "Ingresa tu contraseña") {
                print("alert ret")
            }
            ret=false
        }
        return ret
    }
    
    @IBAction func selectTypeID(_ sender: Any) {
        picker.isHidden=false
    }
    
    @IBAction func showPass(_ sender: Any) {
        vm.isShowPass = !vm.isShowPass

        btnShowPass.setImage(UIImage(named:"stGeneralVercontraseA"), for: .normal)
        txtPass.isSecureTextEntry=true
        if vm.isShowPass{
            btnShowPass.setImage(UIImage(named:"stGeneralOcultarcontraseA"), for: .normal)
            txtPass.isSecureTextEntry=false
        }
        
    }
    
    @IBAction func fogotMyPassword(_ sender: Any) {
        gotoForgotMyPassword()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LoginViewController:UIPickerViewDelegate,UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return vm.pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return vm.pickerData[row].name
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let item=vm.pickerData[row]
        lblType.textColor=UIColor(red: 186/255, green: 186/255, blue: 186/255, alpha: 1)
        lblType.text="Selecciona el tipo de identificación"
        if row>0{
            pickerView.isHidden=true
            lblType.textColor=UIColor(red: 0, green: 0, blue: 0, alpha: 1)
            lblType.text=item.name
            vm.typeSelect=item.id
            vm.documentCode=item.code
        }
    }
}
extension LoginViewController:UINavigationControllerDelegate{
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        let item = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        viewController.navigationItem.backBarButtonItem = item
    }
}
