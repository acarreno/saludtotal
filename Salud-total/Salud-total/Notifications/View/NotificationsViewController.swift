//
//  NotificationsViewController.swift
//  Salud-total
//
//  Created by Andrey Carreño on 11/15/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController {

    @IBOutlet weak var table: UITableView!
    
    let vm=NotificationsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addHeader()
        //vm.inflateTable()
        table.separatorStyle = .none
        vm.getTokenNoti(){(finish,res)in
            self.vm.getNotificatios(){(finish,res)in
                if finish{

                    self.table.reloadData()

                }else{
                    self.alert( msj: res, callback: {
                        self.navigationController?.popViewController(animated: true)
                    })
                   
                }
            }
            
        }
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension NotificationsViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item=vm.container[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: item.cell, for: indexPath) as! NotificationsTableViewCell
        if item.cell == vm.cells.noti{
            cell.contentNoti.addLine(position: .LINE_POSITION_BOTTOM, color: UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.09), width:cell.contentNoti.frame.width )
        }
        if item.cell == vm.cells.alltext{
            cell.textView.text=item.obj! as! String
            vm.container[indexPath.row].height = Int(cell.textView.contentSize.height+20)
        }
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm.container.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //let item=vm.container[indexPath.row]
        /*if item.cell == vm.cells.noti{
            goToDetail()
        }*/
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item=vm.container[indexPath.row]
        let height=item.height
        
        return CGFloat(height)
    }
}
