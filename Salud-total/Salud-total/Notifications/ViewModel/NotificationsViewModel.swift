//
//  NotificationsViewModel.swift
//  Salud-total
//
//  Created by Andrey Carreño on 11/15/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

class NotificationsViewModel:UIViewController {
    struct Cells {
        let noti = "noti"
        let noNoti = "noNoti"
        let alltext = "alltext"
    }
    struct Sizes {
        let noti = 104
        let noNoti = 327
    }
    
    typealias CellModel =  (cell:String,height:Int,obj:Any?)
    
    let cells=Cells()
    let sizes=Sizes()
    var container=[CellModel]()
    
    func getNotificatios(callback:@escaping (Bool,String)->()){
        let url="\(URlGet().notificacion)?token=\(retToken())&aplicativo=APP"

        //let url="\(URlGet().notificacion)?token=582af425-6781-428b-99ec-6218d5cb6039&aplicativo=APP"
        servGet(url:url) { (finish, res) in
            self.container=[]
            if let response = (res as? [String]){
                if response[0] != "No se encontro el usuario"{
                    if response.count>0{
                        if response[0]=="Usuario no tiene notificaciones"{
                            self.container.append((cell:self.cells.noNoti,height:self.sizes.noNoti,obj:nil))
                            callback(true,"")
                        }else{
                            for item in response{
                                self.container.append((cell:self.cells.alltext,height:self.sizes.noti,obj:item))
                            }
                            callback(true,"")
                        }
                    }
                }else{
                    callback(false,"No se encontro el usuario")
                }
            }
            debugPrint(res)
        }
    }
    func getTokenNoti(callback:@escaping (Bool,String)->()){
        let token=retToken()
        let url = "\(URlGet().ValidaTokenNoti)?token=\(token)&aplicativo=APP"
        servGet(url: url) { (finish, res) in
            if finish{
                if let response=LoginModel(JSON: res as! [String:Any]){
                    if response.Valido{
                        
                        UserDefaults.standard.set(response.Token, forKey: "tokenAutorization")
                        callback(true,response.Token)
                        
                    }else{
                        callback(false,response.Error)
                    }
                    
                }
                
            }
            else{
                callback(false,res as!String)
            }
            debugPrint(res)
        }
    }
    func inflateTable(){
        container.append(
            (
                cell:cells.noti,
                height:sizes.noti,
                obj:nil
            )
        )
        container.append(
            (
                cell:cells.noNoti,
                height:sizes.noNoti,
                obj:nil
            )
        )
    }
}
