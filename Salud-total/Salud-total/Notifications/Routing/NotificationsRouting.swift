 //
//  NotificationsRouting.swift
//  Salud-total
//
//  Created by Andrey Carreño on 11/15/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit
extension NotificationsViewController{
    
    static func instantiate() -> NotificationsViewController {
        
        let storyboard = UIStoryboard(name: "Notifications", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! NotificationsViewController
        return vc
        
    }
    func goToDetail(){
        let vc = NotificationsDetailViewController.instantiate()
        self.navigationController?.pushViewController(vc, animated:true)
    }
}
