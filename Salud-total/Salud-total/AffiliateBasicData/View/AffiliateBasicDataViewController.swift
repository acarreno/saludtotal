//
//  AffiliateBasicDataViewController.swift
//  Salud-total
//
//  Created by iMac on 12/9/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class AffiliateBasicDataViewController: UIViewController {

    @IBOutlet weak var table: UITableView!
    
    var vm=AffiliateBasicDataViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vm.inflateTable()
        table.separatorStyle = .none
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AffiliateBasicDataViewController: UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item=vm.container[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AffiliateBasicDataTableViewCell
        cell.icon.image=UIImage(named:item.img)!
        cell.lblProperty.text=item.text
        switch item.id {
        case vm.ids.name:
            cell.lblValue.text=vm.beneficiary.Nombres
        case vm.ids.lastName:
            cell.lblValue.text=vm.beneficiary.Apellidos
        case vm.ids.document:
            cell.lblValue.text=vm.beneficiary.BeneficiarioId
        case vm.ids.gener:
            if vm.beneficiary.Genero=="M"{
                cell.lblValue.text="Masculino"
            }else{
                cell.lblValue.text="Femenino"
            }
        case vm.ids.date:
            let isoDate = vm.beneficiary.FechaNacimiento
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            dateFormatter.locale = Locale(identifier: "es_CO")
            //dateFormatter.locale = Locale(identifier: "es") // set locale to reliable US_POSIX
            let date = dateFormatter.date(from:isoDate)!
            
            let calendar = Calendar.current
            let components = calendar.dateComponents([.year, .month, .day, .hour,], from: date)
           
            let monthName = DateFormatter().monthSymbols[components.month! - 1]

            //let finalDate = calendar.date(from:components)
            cell.lblValue.text="\(components.day!) \(monthName) \(components.year!)"
            
        default:
            break
        }
        return cell
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm.container.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 74
    }
    
}
