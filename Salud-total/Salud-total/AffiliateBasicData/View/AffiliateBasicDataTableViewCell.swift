//
//  AffiliateTableViewCell.swift
//  Salud-total
//
//  Created by iMac on 12/10/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import UIKit

class AffiliateBasicDataTableViewCell: UITableViewCell {

    @IBOutlet weak var lblProperty: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var lblValue: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
