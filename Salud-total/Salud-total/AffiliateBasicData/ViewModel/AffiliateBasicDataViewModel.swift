//
//  AffiliateBasicDataViewModel.swift
//  Salud-total
//
//  Created by iMac on 12/10/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//

import Foundation
import UIKit

class AffiliateBasicDataViewModel  {
    
    struct Cells {
        
    }
    
    struct Ids {
        let name = "name"
        let lastName = "lastName"
        let document = "document"
        let gener = "gener"
        let date = "date"
    }
    
    typealias CellModel =  (id:String,text:String,img:String)
    
    var container=[CellModel]()
    let ids=Ids()
    var beneficiary=BeneficiaryModel(JSON:["":""])!
    
    func inflateTable(){
        container.append((id:ids.name,text:"Nombre (s)",img:"stGeneralUsuario"))
        container.append((id:ids.lastName,text:"Apellido (s)",img:"stGeneralUsuario"))
        container.append((id:ids.document,text:"Documento de identidad",img:"stGeneralDocusuario"))
        container.append((id:ids.gener,text:"Género",img:"stGeneralGenerousuario"))
        container.append((id:ids.date,text:"Fecha de nacimiento",img:"stGeneralFechaAzul"))
    }
}
