//
//  AffiliateBasicDataRouting.swift
//  Salud-total
//
//  Created by iMac on 12/10/19.
//  Copyright © 2019 Wigilabs. All rights reserved.
//


import Foundation
import UIKit

extension AffiliateBasicDataViewController{
    
    static func instantiate() -> AffiliateBasicDataViewController {
        let storyboard = UIStoryboard(name: "AffiliateBasicData", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! AffiliateBasicDataViewController
        return vc
    }
    
   
    
}
